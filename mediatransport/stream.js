const fs = require('fs');
// const http = require('http');
const WebSocket = require('ws');
// const uuidv1 = require('uuid/v1');
const randomstring = require("randomstring");
// const config = require('./config');
// const FFmpeg = require('./ffmpeg');
// const GStreamer = require('./gstreamer');
// const {
//   initializeWorkers,
//   createRouter,
//   createTransport
// } = require('./mediasoup');
// const Peer = require('./peer');
const {
  getPort,
  releasePort
} = require('./port');

const PROCESS_NAME = process.env.PROCESS_NAME || 'FFmpeg';




const initMediaTransporter = (wss, callback) => {
	const peers = new Map();
	let router;

	wss.on('connection', async (socket, request) => {
  //console.log('new socket connection [ip%s]', request.headers['x-forwared-for'] || request.headers.origin);

	  try {
	    const sessionId = randomstring.generate(32).toLowerCase();
	    socket.sessionId = sessionId;
	    const peer = new Peer(sessionId);
	    peers.set(sessionId, peer);

	    const message = JSON.stringify({
	      action: 'router-rtp-capabilities',
	      routerRtpCapabilities: router.rtpCapabilities,
	      sessionId: peer.sessionId
	    });
	    // console.log('router.rtpCapabilities:', router.rtpCapabilities)
	    socket.send(message);
	  } catch (error) {
	    console.error('Failed to create new peer [error:%o]', error);
	    socket.terminate();
	    return;
	  }

	  socket.on('message', async (message) => {
	    try {
	      const jsonMessage = JSON.parse(message);
	     // console.log('socket::message [jsonMessage:%o]', jsonMessage);

	      const response = await handleJsonMessage(jsonMessage);

	      if (response) {
	        ///console.log('sending response %o', response);
	        socket.send(JSON.stringify(response));
	      }
	    } catch (error) {
	      console.error('Failed to handle socket message [error:%o]', error);
	    }
	  });

	  socket.once('close', () => {
	    // console.log('socket::close [sessionId:%s]', socket.sessionId);

	    
	  });
	});


	const handleJsonMessage = async (jsonMessage) => {
	  const { action } = jsonMessage;

	  switch (action) {
	    case 'create-transport':
	    
	    case 'connect-transport':
	     
	    case 'produce':
	      
	    case 'start-record':
	     
	    case 'stop-record':
	     
	    default: console.log('handleJsonMessage() unknown action [action:%s]', action);
	  }
	};



}




module.exports = {
	initMediaTransporter
}