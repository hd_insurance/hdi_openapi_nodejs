const envVariables = {
  env: process.env.NODE_ENV,
  app: {
    secretKey: process.env.APP_SECRET_KEY || '',
    port: process.env.SERVER_PORT,
    prefix: 'NODE_',
    login_timeout: 5,
    rate_limiter: {
      windowMs: 15 * 60 * 1000,
      max: 10000 // limit each IP to 10000 requests per windowMs
    }
  },
  oracledb: {
    master: process.env.ORC_DB_MASTER || '',
    cmedia: process.env.ORC_DB_CMEDIA || '',
    wb2b2c: process.env.ORC_DB_WB2B2C || ''
  },
  mongo:{
    db: "landing_sdk"
  },
  logsystem:{
    host: process.env.LOG_DB_HOST || 'localhost',
    port: process.env.LOG_DB_PORT || 27017,
    username: process.env.LOG_DB_USER || '',
    password: process.env.LOG_DB_PASS || '',
    scheme: process.env.LOG_DB_SCHEME || 'system_log'
  },
  redis:{
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT*1,
    password: process.env.REDIS_PASSWORD,
    db: process.env.REDIS_DB.toString(),
    cdb: process.env.REDIS_DB.toString(),
    ttl: 5
  },
  plugin3rd:{
    zalo:{
      key: process.env.ZALO_KEY,
      appid: '773002743126271816'
    }
  },
  kafka: {
    kafka_topic: 'test_topic',
    kafkaHost: process.env.KAFKA_HOST || 'localhost:2181',
    kafkaUser: process.env.KAFKA_USER || '',
    kafkaPassword: process.env.KAFKA_PASSWORD || '',
  },
  fileExt:{
        '.ico': 'image/x-icon',
        '.json': 'application/json',
        '.csv': 'text/csv',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.jpeg': 'image/jpeg',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg',
        '.mp4': 'video/mpeg-4',
        '.svg': 'image/svg+xml',
        '.pdf': 'application/pdf',
        '.doc': 'application/msword',
        '.pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        '.pem': 'application/x-x509-ca-cert',
        '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        '.xls': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      },
  file_option:{
    'UPLOAD':"UPLOAD",
    'VIEW':"VIEW",
    'UPDATE':"UPDATE",
    'DELETE':"DELETE"
  },
  stream: {
    stream_secret: "123@123x",
    rtmp_server: "localhost",
    server: "localhost:8000",
    ffmpeg: process.env.FFMPEG || "/Users/phamminhhoang/Documents/workspace/OpenapiNodejs/openlibs/ffmpeg"
  }
  
}
module.exports = envVariables;