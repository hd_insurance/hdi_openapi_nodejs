module.exports = {
	SYS_ACTION_API_GETALL: `BEGIN SYS_ACTION_API_GETALL(:cs1); END;`,
	GET_LIST_QUEUE_FUNCTION: `BEGIN GET_LIST_QUEUE_FUNCTION(:cs1); END;`,
	SYS_PARTNER_CONFIG_GETALL: `BEGIN SYS_PARTNER_CONFIG_GETALL(:cs1); END;`,
	SYS_PARTNER_GROUP_API_GETALL: `BEGIN SYS_PARTNER_GROUP_API_GETALL(:cs1); END;`,
	SYS_DOMAIN_GETALL: `BEGIN SYS_DOMAIN_GETALL(:cs1); END;`,
	SYS_USER_GETALL: `BEGIN SYS_USER_GETALL(:cs1); END;`,
	SYS_PARTNER_CONFIG_GET_FROM_DB: `BEGIN SYS_PARTNER_CONFIG_GET_BY_PARTNER_CODE(:ptncode, :cs1); END;`,
	SYS_USER_GET_BY_USRNAME: `BEGIN SYS_USER_GET_BY_USRNAME(:username, :cs1); END;`,
	SYS_API_DATABASE_GETALL: `BEGIN SYS_API_DATABASE_GETALL(:cs1); END;`,
	SYS_BEHAVIOS_CACHE_GETALL: `BEGIN SYS_BEHAVIOS_CACHE_GETALL(:cs1); END;`,
	//Queue
	GET_QUEUE_API: `BEGIN PKG_QUEUE_API.get_producing(:p_actioncode, :p_out1); END;`,
	//File query
	FILE_GET_FROM_DB: `BEGIN pkg_files.get_md_files_by_key(:p_file_key, :cur_1); END;`,
	INSERT_FILE_DB: `BEGIN pkg_files.insert_md_file(:p_file_key, :p_file_original_name, :p_file_name, :p_file_bucket, :p_root_folder, :p_create_by, :p_modified_by, :p_action_code, :p_permission, :p_sub_folder, :p_tags); END;`,
	FILE_GET_CONFIG_ORG: `BEGIN pkg_files.get_org_cfg(:p_org_code, :p_action_code, :p_option, :p_env, :cur_1); END;`,
	FILE_UPDATE_NEW: `BEGIN pkg_files.update_new_md_file(:p_file_key, :p_file_name); END;`,
	//Evoucher

	VALID: `BEGIN PKG_VOUCHER.GIFT_CHECK(:P_CHANNEL,:P_USERNAME,:P_ACTIVATION_CODE,:CUR_1); END;`,
	//LANGUAGE
	GETLANGS: `BEGIN PKG_OPENAPI.getlangcfg(:p_page, :cur_1); END;`,
	ADDLANGS: `BEGIN PKG_OPENAPI.addlangcfg(:p_page, :p_key, :p_content_vi, :p_content_en); END;`,
	//FAQ
	ADDFAQCONTENT: `BEGIN PKG_OPENAPI.addfaqcontent(:p_web_faq_id, :p_icon, :p_title, :p_content, :p_sortorder, :p_parent_id); END;`,
	EDITFAQCONTENT: `BEGIN PKG_OPENAPI.editfaqcontent(:p_faqcontent_id, :p_icon, :p_title, :p_content, :p_sortorder); END;`,
	DELETEFAQCONTENT: `BEGIN PKG_OPENAPI.deletefaqcontent(:p_faqcontent_id); END;`,
	GETFAQ: `BEGIN PKG_OPENAPI.getpagefaq(:p_faq_id, :cur_1); END;`,
	//WEB instance
	GETALLPAGEINSTANCE: `BEGIN PKG_OPENAPI.get_page_instances(:cur_1); END;`,
	//WEB PAGE instance
	GETALLWEBPAGEINSTANCE: `BEGIN PKG_OPENAPI.get_page_config(:p_wiid, :cur_1, :cur_2); END;`,

};