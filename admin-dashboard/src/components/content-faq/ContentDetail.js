import React, { Component, useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { confirmAlert } from "react-confirm-alert";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Col, Button, Form, Modal, Row } from "react-bootstrap";
import { Card, CardHeader, ListGroup, ListGroupItem } from "shards-react";
import { StageSpinner } from "react-spinners-kit";
import axios from "axios";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const ContentDetail = (props) => {
  let faq_id = props.faq_id;

  const [arrContent, setArrContent] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [parentId, setParentId] = useState(null);
  const [typeModel, setTypeModel] = useState("ADD");
  const [contentSelect, setContentSelect] = useState({});

  useEffect(() => {
    try {
      setParentId(props.data.FAQ_CONTENT_ID); // set parrent thằng cha
      setArrContent(props.data.content || []);
    } catch (err) {
      console.log(err);
    }
  }, [props.data]);

  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    setArrContent((arrContent) =>
      reorder(arrContent, result.source.index, result.destination.index)
    );
  };

  const handleEditContent = (data) => {
    setTitle(data.TITLE);
    setContent(data.CONTENT);
    setContentSelect(data);
    setTypeModel("EDIT");
    setShowModal(true);
  };

  const closeModel = () => {
    setShowModal(false);
  };

  const openModelAddContent = () => {
    setTypeModel("ADD");
    setTitle("");
    setContent("");
    setShowModal(true);
  };

  const deleteContent = async (data) => {
    // console.log('====>', data);
    let arr = arrContent.filter(
      (item) => item.FAQ_CONTENT_ID !== data.FAQ_CONTENT_ID
    );
    setArrContent(arr);
    try {
      let res = await axios.post(
        `${process.env.REACT_APP_SERVER}/api/faq-content/delete`,
        {
          p_faqcontent_id: data.FAQ_CONTENT_ID,
          p_web_faq_id: data.WEB_FAQ_ID,
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  const handleDeleteContent = (data) => {
    confirmAlert({
      title: "Bạn có muốn xoá không!",
      message: "Khi đã xoá dữ liệu sẽ không quay lại ahihi!",
      buttons: [
        {
          label: "Ok",
          onClick: () => deleteContent(data),
        },
        {
          label: "Close",
          onClick: () => closeModel(),
        },
      ],
    });
  };

  const handleSubmit = async () => {
    try {
      if (!parentId) {
        closeModel();
        setContent("");
        setTitle("");
        confirmAlert({
          title: "Bạn chưa chọn chủ đề",
          message: "Chọn Chủ đề xong bạn mới có thể thêm được content!",
          buttons: [
            {
              label: "Ok",
            },
          ],
        });
        return;
      }
      if (title === "" || content === "") {
        closeModel();
        confirmAlert({
          title: "Các trường không được để trống",
          message: "Bạn phải điền đầy đủ thông tin các trường!",
          buttons: [
            {
              label: "Ok",
            },
          ],
        });
        return;
      }
      let data = {
        p_faqcontent_id: contentSelect.FAQ_CONTENT_ID,
        p_icon: contentSelect.ICON,
        p_title: title,
        p_content: content,
        p_sortorder: contentSelect.SORT_ORDER,
      };
      let url = `${process.env.REACT_APP_SERVER}/api/faq-content/edit`;
      if (typeModel === "ADD") {
        url = `${process.env.REACT_APP_SERVER}/api/faq-content/add`;
        data = {
          p_web_faq_id: faq_id, // hardcode
          p_icon: null,
          p_title: title,
          p_content: content,
          p_sortorder: null,
          p_parent_id: parentId,
        };
      }
      const response = await axios.post(url, data);
      if (response.data.success) {
        console.log(typeModel);
        if (typeModel === "ADD") {
          props.reload();
        }
         /* xử lý để không cần api sửa trả data xịn về */
        if (typeModel === "EDIT") {
          const tempData = [...arrContent];
          const index = tempData.findIndex(
            (item) => contentSelect.FAQ_CONTENT_ID === item.FAQ_CONTENT_ID
          );
          if (index > -1) {
            const item = tempData[index];
            tempData.splice(index, 1, {
              ...item,
              TITLE: title,
              CONTENT: content,
            });
            setArrContent(tempData);
          }
        }
        /* xử lý để không cần api sửa trả data xịn về */
        closeModel();
        confirmAlert({
          title: "Hệ Thống Thông Báo",
          message: "Thành Công",
          buttons: [
            {
              label: "Ok",
            },
          ],
        });
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <Modal
        show={showModal}
        onHide={closeModel}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h6>Thêm Content</h6>
        </Modal.Header>
        <Modal.Body className="cs-body">
          <Row className="pt-3">
            <label>Title</label>
          </Row>
          <Row>
            <input
              className="form-control"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </Row>
          <Row className="pt-3">
            <label>Content</label>
          </Row>
          <Row>
            <textarea
              rows="4"
              name="content"
              value={content}
              className="form-control"
              onChange={(e) => setContent(e.target.value)}
            />
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModel}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSubmit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <Card
        small
        className="mb-4 hqk-custom-scrollbar"
        style={{ maxHeight: "520px", overflowX: "scroll" }}
      >
        <CardHeader className="border-bottom d-flex justify-content-end">
          <Button
            disabled={props.isDisable}
            onClick={() => openModelAddContent()}
          >
            Thêm Content
          </Button>
        </CardHeader>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div {...provided.droppableProps} ref={provided.innerRef}>
                <ListGroup flush>
                  <ListGroupItem className="p-3">
                    {props.isLoading ? (
                      <div
                        className="d-flex justify-content-center"
                        style={{ width: "100%" }}
                      >
                        <StageSpinner
                          size={35}
                          color="#329945"
                          loading={props.isLoading}
                        />
                      </div>
                    ) : (
                      arrContent.map((item, index) => (
                        <Draggable
                          key={item.FAQ_CONTENT_ID}
                          draggableId={`${item.FAQ_CONTENT_ID}`}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                            >
                              <Card
                                small
                                className="mb-3"
                                style={{
                                  boxShadow:
                                    "0 4px 20px rgb(149 185 193 / 20%)",
                                }}
                              >
                                <div
                                  className="row p-2"
                                  style={{ marginBottom: "-25px" }}
                                >
                                  <Col className="d-flex justify-content-end">
                                    <div
                                      className="material-icons kitin-save"
                                      onClick={() => handleEditContent(item)}
                                    >
                                      edit
                                    </div>
                                    <div
                                      className="material-icons kitin-delete"
                                      onClick={() => handleDeleteContent(item)}
                                    >
                                      delete
                                    </div>
                                  </Col>
                                </div>

                                <div
                                  className="row pt-2"
                                  style={{
                                    width: "100%",
                                    margin: "auto",
                                  }}
                                >
                                  <Col>
                                    <div className="form-group col-12 pr-0">
                                      <label
                                        htmlFor="title"
                                        className="kitin-custom-p"
                                      >
                                        Title
                                      </label>
                                      <div>{item.TITLE}</div>
                                    </div>
                                    <div className="form-group col-12 pr-0">
                                      <label
                                        htmlFor="lastName"
                                        className="kitin-custom-p"
                                      >
                                        Content
                                      </label>
                                      <div>{item.CONTENT}</div>
                                    </div>
                                  </Col>
                                </div>
                              </Card>
                            </div>
                          )}
                        </Draggable>
                      ))
                    )}
                  </ListGroupItem>
                </ListGroup>
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </Card>
    </>
  );
};

export default ContentDetail;
