import React from "react";
import "./style.css"

const LoadingForm = (props) => {
  return (
    <div className={props.isFullScreen ? "cover-full-loading" : ""}>
      <div className={props.isFullScreen ? "fm-loader-full" : "fm-loader"}>
        <div className="loader-sdk">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  );
};
export default LoadingForm;
