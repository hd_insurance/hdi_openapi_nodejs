import axios from 'axios';
export default class AuthService {
  constructor() {
    this.domain = process.env.REACT_APP_SERVER
    this.login = this.login.bind(this)
    this.register = this.register.bind(this)
    this.logout = this.logout.bind(this)
    this.getProfile = this.getProfile.bind(this)
    this.setProfile = this.setProfile.bind(this)
  }

  login(username, password, callback) { 
    var self = this
    axios.post(`${this.domain}/api/admin/sign-in`, {
        username: username,
        password: password
      })
      .then(function (response) {
        console.log('response', response)
        self.setProfile(response.data.data)
        callback(true, response.data.data)
      })
      .catch(function (error) {
        console.log(error)
        if(error.response){
          if(error.response.data){
            callback(false, error.response.data)
          }else{
            callback(false, {})
          }
        }else{
          callback(false, {})
        }
        
      });


      
  }

  register(username, password, fullname, callback) { 
    var self = this
    axios.post(`${this.domain}/api/user/register`, {
        email: username,
        password: password,
        fullname: fullname
      })
      .then(function (response) {
        console.log('response', response)
        self.setProfile(response.data.data)
        callback(true, response.data.data)
      })
      .catch(function (error) {
        console.log(error)
        if(error.response){
          if(error.response.data){
            callback(false, error.response.data)
          }else{
            callback(false, {})
          }
        }else{
          callback(false, {})
        }
        
      });


      
  }

  loggedIn(){
    // Checks if there is a saved token and it's still valid
    const profile = this.getProfile()
    console.log(profile)
    return Object.keys(profile).length!=0;
  }

  setProfile(profile){
    console.log('GETPROFILE', profile)
    localStorage.setItem('profile', JSON.stringify(profile))
  }

  getProfile(){
    const profile = localStorage.getItem('profile')
    return profile ? JSON.parse(localStorage.profile) : {}
  }

  logout(){
    localStorage.removeItem('profile');
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      var error = new Error(response.statusText)
      error.response = response
      throw error
    }
  }

}
