import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
import BlogOverview from "./views/BlogOverview";
import UserProfileLite from "./views/UserProfileLite";
import AddNewPost from "./views/AddNewPost";
import Errors from "./views/Errors";
import ComponentsOverview from "./views/ComponentsOverview";
import Tables from "./views/Tables";
import BlogPosts from "./views/BlogPosts";
import APICode from "./views/APICode";
import WebInstance from "./views/WebInstance";
import FAQContent from "./views/FAQContent";
import GetLogs from "./views/GetLogs";

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/blog-overview" />
  },
  {
    path: "/blog-overview",
    layout: DefaultLayout,
    component: BlogOverview
  },
  {
    path: "/api-code",
    layout: DefaultLayout,
    component: APICode
  },
  {
    path: "/get-logs",
    layout: DefaultLayout,
    component: GetLogs
  },
  {
    path: "/list-web-instance",
    layout: DefaultLayout,
    component: WebInstance
  },
  {
    path: "/user-profile-lite",
    layout: DefaultLayout,
    component: UserProfileLite
  },
  {
    path: "/add-new-post",
    layout: DefaultLayout,
    component: AddNewPost
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: DefaultLayout,
    component: ComponentsOverview
  },
  {
    path: "/language/:page_id",
    layout: DefaultLayout,
    component: Tables
  },
    {
    path: "/faq/:faq_id",
    layout: DefaultLayout,
    component: FAQContent
  },
  {
    path: "/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts
  }
];
