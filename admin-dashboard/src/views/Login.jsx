import React, { useState, useEffect } from 'react';
import { Redirect,NavLink } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AuthService from '../services/AuthService'
import { MetroSpinner, CircleSpinner} from "react-spinners-kit";

import cogoToast from 'cogo-toast';
const auth = new AuthService()


const Login  = (props)=> {
	const [userName, setUsername] = useState("")
	const [password, setPassword] = useState("")
 
	const login = ()=>{
		auth.login(userName, password, (state, userinfo)=>{
			
			
	        if(state){
	        	
	        	cogoToast.loading('Login success!').then(() => {
                  window.location.reload(false);
				});
	        }else{
	          	cogoToast.error('Login failed!')
	        }
	    })

	}
		
		return (
			
				    <div id="root">
				        <div>
				            <div className="icon-sidebar-nav container-fluid">
				                <div className="row">
				                    <main className="main-content col col">
				                        <div className="main-content-container h-100 px-4 container-fluid">
				                            <div className="h-100 no-gutters row">
				                                <div className="auth-form mx-auto my-auto col-md-5 col-lg-3">
				                                    <div className="card">
				                                        <div className="card-body">
				                                            <h5 className="auth-form__title text-center mb-4">Access Your Account</h5>
				                                            <form className="">
				                                                <div className="form-group">
				                                                    <label htmlFor="exampleInputEmail1">Tên đăng nhập</label>
				                                                    <input type="text" id="exampleInputEmail1" placeholder="User name" value={userName} onChange={(e)=>setUsername(e.target.value)} className="form-control"/>
				                                                </div>
				                                                <div className="form-group">
				                                                    <label htmlFor="exampleInputPassword1">Mật khẩu</label>
				                                                    <input type="password" id="exampleInputPassword1" placeholder="Password" value={password} onChange={(e)=>setPassword(e.target.value)} className="form-control"/>
				                                                </div>
				                                                <div className="form-group">
				                                                    <label className="custom-control custom-checkbox">
				                                                        <input id="dr-checkbox-s0qzPfSoL" type="checkbox" className="custom-control-input"/>
				                                                        <label id="dr-checkbox-s0qzPfSoL" className="custom-control-label" aria-hidden="true"></label><span className="custom-control-description">Remember me for 30 days.</span>
				                                                    </label>
				                                                </div>
				                                                <button type="button" className="d-table mx-auto btn btn-accent btn-pill" onClick={(e)=>login()}>Access Account</button>
				                                            </form>
				                                        </div>
				                                        <div className="card-footer">
				                                            
				                                        </div>
				                                    </div>
				                                    <div className="auth-form__meta d-flex mt-4"><a href="/demo/shards-dashboard-react/forgot-password">Forgot your password?</a><a className="ml-auto" href="/demo/shards-dashboard-react/register">Create a new account?</a>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
				                    </main>
				                </div>
				            </div>
				        </div>
				    </div>
				

			);
}
export default Login;