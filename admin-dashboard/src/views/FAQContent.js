import React, { useState, useEffect, useRef, useCallback } from "react";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Form,
  FormInput,
} from "shards-react";

import { Button, Modal } from "react-bootstrap";
import '../assets/style.css';

// import Network from "../services/Network.js"
import PageTitle from "../components/common/PageTitle";
import ContentDetail from "../components/content-faq/ContentDetail";

const Tables = (props) => {
  let faq_id = props.match.params.faq_id;

  let formData = useRef({}).current;
  const [faq, setFAQ] = useState([]);
  const [openAddTopic, setOpenAddTopic] = useState(false);
  const [editingId, setEditingId] = useState("");
  const [isLoadingContent, setIsLoadingContent] = useState(false);
  const [topicDetail, setTopicDetail] = useState({});
  const [disableBtn, setDisableBtn] = useState(false);
  const [icon, setIcon] = useState("");
  const [topic, setTopic] = useState("");

  const [dataEdit, setDataEdit] = useState({
    ICON: '',
    TITLE: '',
  });
  const [active, setActive] = useState(null);
  const [showModal, setShowModal] = useState(false);
  // const faq_id = 2

  useEffect(() => {
    initList();
  }, []);

  const initList = async () => {
    try {
      const data = await axios.get(
        `${process.env.REACT_APP_SERVER}/api/faq/${faq_id}?cache=false`
      );
      setFAQ(data.data);
      console.log(data.data);
    } catch (e) {
      console.log(e);
    }
  };

  const closeModel = () => {
    setShowModal(false);
  };

  const handleAddTopic = () => {
    setOpenAddTopic(!openAddTopic);
    setDisableBtn(!disableBtn);
    setTopicDetail({});
  };

  const handleEdit = (data) => {
    setDataEdit(data);
    setActive(data.FAQ_CONTENT_ID);
    setShowModal(true);
  };

  const handleDelete = async (record) => {
    let arrTemp = faq.filter(
      (item) => item.FAQ_CONTENT_ID !== record.FAQ_CONTENT_ID
    );
    setFAQ(arrTemp);
    try {
      let res = await axios.post(
        `${process.env.REACT_APP_SERVER}/api/faq-content/delete`,
        {
          p_faqcontent_id: record.FAQ_CONTENT_ID,
          p_web_faq_id: record.WEB_FAQ_ID,
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  const handleDeleteTopic = (row) => {
    setActive(row.FAQ_CONTENT_ID);
    confirmAlert({
      title: "Bạn có muốn xoá không!",
      message: "Bạn có muốn xoá không",
      buttons: [
        {
          label: "Ok",
          onClick: () => handleDelete(row),
        },
        {
          label: "Close",
        },
      ],
    });
  };

  const handleSave = async () => {
    console.log('hoang khanh', dataEdit);
    const tempData = [...faq];
    const index = tempData.findIndex(
      (item) => dataEdit.FAQ_CONTENT_ID === item.FAQ_CONTENT_ID
    );
    if (index > -1) {
      const item = tempData[index];
      tempData.splice(index, 1, {
        ...item,
        ...dataEdit
      });
      setDataEdit({
        ICON: '',
        TITLE: '',
      });
      setFAQ(tempData);
      const objFaq = {
        p_faqcontent_id: dataEdit.FAQ_CONTENT_ID,
        p_icon: dataEdit.ICON,
        p_title: dataEdit.TITLE,
        p_content: dataEdit.CONTENT,
        p_sortorder: dataEdit.SORT_ORDER,
      };
      try {
        const response = await axios.post(
          `${process.env.REACT_APP_SERVER}/api/faq-content/edit`,
          objFaq
        );
        if (response.data.success) {
          closeModel(true);
        }
      } catch (err) {
        console.log(err);
        closeModel(true);
      }
    }
  };

  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    if (form.checkValidity() === false) {
      return;
    }
    try {
      let objFaq = {
        p_web_faq_id: faq_id,
        p_icon: icon,
        p_title: topic,
        p_content: null,
        p_sortorder: null,
        p_parent_id: null,
      };
      // console.log('====> data đẩy lên', objFaq);
      const response = await axios.post(
        `${process.env.REACT_APP_SERVER}/api/faq-content/add`,
        objFaq
      );
      // console.log('response nè ===>', response);
      // console.log('response.data ===>', response.data);
      if (response.data.success) {
        let objTemp = {
          ICON: objFaq.p_icon,
          TITLE: objFaq.p_title,
        };
        let arrTemp = faq;
        arrTemp.push(objTemp);
        setFAQ(arrTemp);
        setIcon("");
        setTopic("");
        setOpenAddTopic(false);
        console.log("Ok");
      }
    } catch (err) {
      console.log(err);
    }
  };

  const reloadData = () => {
    initList();
    setTopicDetail([]);
    setActive(null);
  };

  const handleChange = (e) => {
    const { value } = e.target;
    if(e.target.name === 'iconEdit') {
      setDataEdit(prevState => ({
        ...prevState,
        ICON: value
      }));
      return;
    }
    setDataEdit(prevState => ({
      ...prevState,
      TITLE: value
    }));
  }
  

  const handleActive = (data) => {
    setActive(data.FAQ_CONTENT_ID);
    setIsLoadingContent(true); // set loading
    setOpenAddTopic(false); // đóng add topic
    setDisableBtn(false); // tắt disable btn
    setTopicDetail(data);
    setTimeout(() => {
      setIsLoadingContent(false);
    }, 700);
  }

  return (
    <Container fluid className="main-content-container px-4">
      {/* Page Header */}
      <Modal
        show={showModal}
        onHide={closeModel}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h6>Update</h6>
        </Modal.Header>
        <Modal.Body className="cs-body">
          <Row className="pt-3">
            <label>Icon</label>
          </Row>
          <Row>
            <input
              name="iconEdit"
              className="form-control"
              value={dataEdit.ICON}
              onChange={handleChange}
            />
          </Row>
          <Row className="pt-3">
            <label>Topic</label>
          </Row>
          <Row>
            <textarea
              rows="4"
              name="topicEdit"
              value={dataEdit.TITLE}
              className="form-control"
              onChange={handleChange}
            />
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModel}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSave}>
            Save Change
          </Button>
        </Modal.Footer>
      </Modal>
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="4"
          title="Cấu Hình FQA"
          subtitle="FAQ manager"
          className="text-sm-left"
        />
      </Row>

      <Row>
        <Col lg="6">
          <Card small>
            <CardHeader className="border-bottom">
              <div className="row">
                <div className="col-md-7">
                  <h6 className="m-0">Danh sách Topic FAQ</h6>
                </div>
                <div className="col-md-5 ta-right">
                  <Button
                    size="sm"
                    theme="secondary"
                    className=" mr-1"
                    onClick={handleAddTopic}
                  >
                    Thêm topic
                  </Button>
                </div>
              </div>
            </CardHeader>
            <ListGroup flush>
              <ListGroupItem className="p-0">
                <div className="row m-0 contain_title_table">
                  <div className="col-4">Icon</div>
                  <div className="col-5">Title</div>
                  <div className="col-3">Action</div>
                </div>
                {faq.map((item, index) => {
                  return (
                    <div key={index} className={`row m-0 row_content ${active === item.FAQ_CONTENT_ID ? 'hightlight_row' : ''}`}>
                      <div className="col-4 col_icon" onClick={() => handleActive(item)}>{item.ICON}</div>
                      <div className="col-5 col_title" onClick={() => handleActive(item)}>{item.TITLE}</div>
                      <div className="col-3 contain_action">
                        <div className="btn btn-warning btn-sm mr-2" onClick={() => handleEdit(item)}>Edit</div>
                        <div className="btn btn-danger btn-sm" onClick={() => handleDeleteTopic(item)}>Delete</div>
                      </div>
                    </div>
                  );
                })}

                <Form className="mt-3 mb-3" onSubmit={handleSubmit}>
                  {openAddTopic ? (
                    <>
                      <Row style={{ with: "100%", margin: "auto" }}>
                        <Col md={6}>
                          <FormInput
                            name="icon"
                            placeholder="enter icon"
                            value={icon}
                            onChange={(e) => setIcon(e.target.value)}
                            required
                          />
                        </Col>
                        <Col md={6}>
                          <FormInput
                            name="topic"
                            value={topic}
                            onChange={(e) => setTopic(e.target.value)}
                            placeholder="enter topic"
                            required
                          />
                        </Col>
                      </Row>
                      <Row
                        style={{ with: "100%", margin: "auto" }}
                        className="d-flex justify-content-center mt-3"
                      >
                        <Button
                          theme="success"
                          className=" btn-success"
                          type="submit"
                        >
                          Save
                        </Button>
                      </Row>
                    </>
                  ) : null}
                </Form>
              </ListGroupItem>
            </ListGroup>
          </Card>
        </Col>
        <Col lg="6">
          <ContentDetail
            isDisable={disableBtn}
            isLoading={isLoadingContent}
            data={topicDetail}
            reload={reloadData}
            faq_id={faq_id}
          />
        </Col>
      </Row>

      {/* Default Dark Table */}
    </Container>
  );
};

export default Tables;
