import React, { useState, useEffect } from "react";
import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import { useParams } from "react-router-dom";
import DataTable from "react-data-table-component";

import {
  Container,
  FormInput,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormCheckbox,
} from "shards-react";

import { Button, Tabs, Tab, Modal } from "react-bootstrap";

import NormalButtons from "../components/components-overview/NormalButtons";

import { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";

// import Network from "../services/Network.js"
import PageTitle from "../components/common/PageTitle";
import axios from "axios";

const columns = [
  {
    name: "ACTION CODE",
    selector: "API_CODE",
    sortable: true,
  },
  {
    name: "STORE",
    selector: "PRO_CODE",
    sortable: true,
  },
  {
    name: "TYPE",
    selector: "ACTION_TYPE",
    sortable: true,
  },
];

const Tables = (props) => {
  const [apiList, setAPIList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [totalRows, setTotalRows] = useState(0);
  const [perPage, setPerPage] = useState(10);

  useEffect(() => {
    initList();
  }, []);

  const initList = async () => {
    try {
      const data = await axios.get(
        `${process.env.REACT_APP_SERVER}/api/admin/list/action-code`
      );
      setAPIList(data.data.data[0]);
      console.log(data.data.data[0])
      setLoading(false);
    } catch (e) {}
  };

  const handlePageChange = (page) => {
    setLoading(false);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setLoading(false);
  };

  return (
    <Container fluid className="main-content-container px-4">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="4"
          title="Cấu hình API"
          subtitle="API manager"
          className="text-sm-left"
        />
      </Row>

      {/* Default Light Table */}
      <Row>
        <Col>
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <div className="row">
                <div className="col-md-6">
                  <h6 className="m-0">API CODE </h6>
                </div>

                <div className="col-md-6 ta-right">
                  <Button size="sm" theme="secondary" className=" mr-1">
                    Thêm nội dung
                  </Button>
                </div>
              </div>
            </CardHeader>
            <CardBody className="p-0 pb-3">
              <DataTable
                title="API Code"
                columns={columns}
                data={apiList}
                progressPending={loading}
                pagination
                paginationServer
                paginationTotalRows={totalRows}
                selectableRows
                onChangeRowsPerPage={handlePerRowsChange}
                onChangePage={handlePageChange}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>

      {/* Default Dark Table */}
    </Container>
  );
};

export default Tables;
