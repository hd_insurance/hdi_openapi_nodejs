import React, { useState, useEffect } from "react";
import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import { useParams } from "react-router-dom";

import {
  Container,
  FormInput,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormCheckbox,
} from "shards-react";

import { Button, Tabs, Tab, Modal } from "react-bootstrap";

import NormalButtons from "../components/components-overview/NormalButtons";

import { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";

// import Network from "../services/Network.js"
import PageTitle from "../components/common/PageTitle";
import axios from "axios";

const Tables = (props) => {
  let page_id = props.match.params.page_id;

  const [showModalEditor, setShowModalEditor] = useState(false);
  const [isEditor, setIsEditor] = useState(false);

  const [langs, setLanges] = useState({});

  const [langEdit, setLangeEdit] = useState(null);

  const [ckey, setCKEY] = useState(null);
  const [cVi, setCVI] = useState("");
  const [cEn, setCEN] = useState("");

  const [editorStateVi, setEditorStateVi] = useState(
    EditorState.createWithContent(
      ContentState.createFromBlockArray(htmlToDraft(cVi).contentBlocks)
    )
  );
  const [editorStateEn, setEditorStateEn] = useState(
    EditorState.createWithContent(
      ContentState.createFromBlockArray(htmlToDraft(cEn).contentBlocks)
    )
  );

  useEffect(() => {
    initLanguage();
  }, []);

  const onClickEdit = (idEdit) => {
    const langitem = langs[idEdit];
    setLangeEdit(idEdit);
    setCKEY(idEdit);
    setCVI(langitem[0]);
    setCEN(langitem[1]);

    setEditorStateVi(
      EditorState.createWithContent(
        ContentState.createFromBlockArray(
          htmlToDraft(langitem[0]).contentBlocks
        )
      )
    );
    setEditorStateEn(
      EditorState.createWithContent(
        ContentState.createFromBlockArray(
          htmlToDraft(langitem[1]).contentBlocks
        )
      )
    );

    setShowModalEditor(true);
  };

  const initLanguage = async () => {
    try {
      const data = await axios.get(
        `${process.env.REACT_APP_SERVER}/api/lang/${page_id}`
      );
      setLanges(data.data.data);
    } catch (e) {}
  };

  const showModalLanguage = () => {
    setLangeEdit(null);
    setCKEY("");
    setCVI("");
    setCEN("");
    setShowModalEditor(true);
  };

  const addLanguage = async () => {
    try {
      if (cVi) {
        const langobj = {
          key: ckey,
          p_content_vi: cVi,
          p_content_en: cEn,
        };
        const response = await axios.post(
          `${process.env.REACT_APP_SERVER}/api/lang/${page_id}`,
          langobj
        );

        initLanguage();

        if (response.data.key) {
          setLangeEdit(null);
          setCKEY("");
          setCVI("");
          setCEN("");
        }

        closeEditor();
        confirmAlert({
          title: "Thành công",
          message: `ID: #${response.data.key}`,
          buttons: [
            {
              label: "Copy",
              onClick: () => {
                navigator.clipboard.writeText(response.data.key).then(
                  function () {
                    console.log("Async: Copying to clipboard was successful!");
                  },
                  function (err) {
                    console.error("Async: Could not copy text: ", err);
                  }
                );
              },
            },
            {
              label: "Close",
              onClick: () => {},
            },
          ],
        });
      }
    } catch (e) {}
  };

  const onChangeCodeMode = (e) => {
    if (!isEditor) {
      setEditorStateVi(
        EditorState.createWithContent(
          ContentState.createFromBlockArray(htmlToDraft(cVi).contentBlocks)
        )
      );
      setEditorStateEn(
        EditorState.createWithContent(
          ContentState.createFromBlockArray(htmlToDraft(cEn).contentBlocks)
        )
      );
    } else {
      const contentVi = draftToHtml(
        convertToRaw(editorStateVi.getCurrentContent())
      );
      const contentEn = draftToHtml(
        convertToRaw(editorStateEn.getCurrentContent())
      );
      setCVI(contentVi);
      setCEN(contentEn);
    }
    setIsEditor(!isEditor);
  };

  const closeEditor = () => {
    setShowModalEditor(false);
  };
  const onChangeVi = (editorState) => {
    setEditorStateVi(editorState);
  };
  const onChangeEn = (editorState) => {
    setEditorStateEn(editorState);
  };
  return (
    <Container fluid className="main-content-container px-4">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="4"
          title="Tuỳ chỉnh ngôn ngữ"
          subtitle="NGÔN NGỮ"
          className="text-sm-left"
        />
      </Row>

      <Modal show={showModalEditor} onHide={closeEditor} size="lg">
        <Modal.Header closeButton>
          <div className="editor-heading-md">
            <input
              className="form-control editorsx"
              placeholder={"Translate key"}
              value={ckey}
              onChange={(e) => {
                setCKEY(e.target.value);
              }}
            />
            <FormCheckbox
              toggle
              small
              checked={isEditor}
              onChange={(e) => onChangeCodeMode(e)}
            >
              Editor mode
            </FormCheckbox>
          </div>
        </Modal.Header>
        <Modal.Body className="cs-body">
          <Tabs defaultActiveKey="langvi" id="uncontrolled-tab-example">
            <Tab eventKey="langvi" title="Tiếng Việt">
              {isEditor ? (
                <Editor
                  editorState={editorStateVi}
                  toolbarClassName="toolbarClassName"
                  wrapperClassName="wrapperClassName"
                  editorClassName="editorClassName"
                  onEditorStateChange={onChangeVi}
                />
              ) : (
                <textarea
                  class="form-control lg-editor"
                  onChange={(e) => setCVI(e.target.value)}
                  value={cVi}
                />
              )}
            </Tab>
            <Tab eventKey="langen" title="Tiếng Anh">
              {isEditor ? (
                <Editor
                  editorState={editorStateEn}
                  toolbarClassName="toolbarClassName"
                  wrapperClassName="wrapperClassName"
                  editorClassName="editorClassName"
                  onEditorStateChange={onChangeEn}
                />
              ) : (
                <textarea
                  class="form-control lg-editor"
                  onChange={(e) => setCEN(e.target.value)}
                  value={cEn}
                />
              )}
            </Tab>
          </Tabs>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeEditor}>
            Close
          </Button>
          <Button variant="primary" onClick={() => addLanguage()}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Default Light Table */}
      <Row>
        <Col>
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <div className="row">
                <div className="col-md-6">
                  <h6 className="m-0">LANGUAGE CONTENT </h6>
                </div>

                <div className="col-md-6 ta-right">
                  <Button
                    size="sm"
                    theme="secondary"
                    className=" mr-1"
                    onClick={() => showModalLanguage()}
                  >
                    Thêm nội dung
                  </Button>
                </div>
              </div>
            </CardHeader>
            <CardBody className="p-0 pb-3">
              <table className="table mb-0">
                <thead className="thead-light">
                  <tr>
                    <th scope="col" className="border-0">
                      #
                    </th>
                    <th scope="col" className="border-0 ex2">
                      KEY
                    </th>
                    <th scope="col" className="border-0">
                      VIETNAM
                    </th>
                    <th scope="col" className="border-0">
                      ENGLISH
                    </th>
                    <th scope="col" className="border-0">
                      ACTION
                    </th>
                  </tr>
                </thead>

                <tbody>
                  {Object.keys(langs).map((item, index) => {
                    const langitem = langs[item];
                    return (
                      <tr>
                        <td>{index + 1}</td>
                        <td>{item}</td>
                        <td className="tdx2">{langitem[0]}</td>
                        <td className="tdx2">{langitem[1]}</td>
                        <td className="ta-r">
                          <Button
                            size="sm"
                            theme="warning"
                            className=" btn-warning"
                            onClick={(e) => onClickEdit(item)}
                          >
                            Sửa
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </CardBody>
          </Card>
        </Col>
      </Row>

      {/* Default Dark Table */}
    </Container>
  );
};

export default Tables;
