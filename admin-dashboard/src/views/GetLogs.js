import React, { useState, useEffect } from "react";
import { confirmAlert } from "react-confirm-alert"; // Import
import { useParams } from "react-router-dom";
import DataTable from "react-data-table-component";
import ReactJson from "react-json-view";
import Loading from "../components/lib/loading";
import {
  Container,
  FormInput,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormCheckbox,
} from "shards-react";

import { Button, Tabs, Tab, Modal } from "react-bootstrap";

import PageTitle from "../components/common/PageTitle";
import axios from "axios";


const customCellType = (row)=>{
  var type = null
  if(row.level == "t"){
    type = "TRADE"
  }else if(row.level == "re"){
    type = "REQUEST_ERROR"
  }else if(row.level == "e"){
    type = "SERVER_ERROR"
  }else{
     type = "INFO"
  }
  return <div className="allowRowEventsStatus"><i class="fas fa-exchange-alt"></i><div className="type-debug">{type}</div></div>
}
const customCellRE = (row)=>{
  var type = null
  if(row.type == "s"){
    type = "SYSTEM"
  }else if(row.type == "r"){
    type = "REQUEST"
  }else{
    type = "UNDIFF"
  }
  return <div className="allowRowEventsType"><i class="fas fa-dot-circle"></i><div className="type-debug">{type}</div></div>
}

const columns = [
  {
    name: "TYPE",
    selector: "req_id",
    cell: customCellType,
    sortable: true,
  },
  {
    name: "TYPE",
    selector: "req_id",
    cell: customCellRE,
    sortable: true,
  },
  {
    name: "TITLE",
    selector: "title",
    sortable: true,
  },
  {
    name: "TIME",
    selector: "time",
    sortable: true,
  },
  {
    name: "ENV",
    selector: "env",
    sortable: true,
  },
];

const Tables = (props) => {
  const [loading, setLoading] = useState(true);
  const [totalRows, setTotalRows] = useState(0);
  const [perPage, setPerPage] = useState(40);

  const [listLog, setListLog] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [curentLog, setCurentLog] = useState({});

  useEffect(() => {
    initList();
  }, []);

  const initList = async () => {
    try {
      setLoading(true);
      const data = await axios.get(`${process.env.REACT_APP_SERVER}/getlogs`);
      if (data) {
        setListLog(data.data);
        setTotalRows(data.data.length)
        console.log(data.data);
        setLoading(false);
      }
    } catch (e) {
      setLoading(false);
      console.log(e);
    }
  };

  const handlePageChange = (page) => {
    setLoading(false);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setLoading(false);
  };

  const handleRowClick = (data) => {
    setLoading(true);
    setTimeout(() => {
      setCurentLog(data);
      setShowModal(true);
      setLoading(false);
    }, 500);

    // console.log(data);
  };

  return (
    <>
      <Modal
        size="lg"
        show={showModal}
        onHide={() => setShowModal(false)}
        aria-labelledby="example-modal-sizes-title-lg"
        scrollable={true}
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg">
            Detail Logs
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <ReactJson
              displayDataTypes={false}
              src={curentLog}
              theme="rjv-default"
            />
          </div>
        </Modal.Body>
      </Modal>

      <Container fluid className="main-content-container px-4">
        {/* Page Header */}

        <Row noGutters className="page-header py-4">
          <PageTitle
            sm="4"
            title="Logs API"
            subtitle="Log manager"
            className="text-sm-left"
          />
        </Row>

        <Row>
          <Col>
            <Card small className="mb-4">
              {loading ? <Loading /> : null}
              {/* <Loading /> */}
              <CardHeader className="border-bottom">
                <div className="row">
                  <div className="col-md-6">
                    <h6 className="m-0">Logs API </h6>
                  </div>
                </div>
              </CardHeader>
              <CardBody className="p-0 pb-3">
                <DataTable
                  title="API Code"
                  columns={columns}
                  data={listLog}
                  //   progressPending={loading}
                  //   progressComponent={<Loading />}
                  pagination={true}
                
                  paginationTotalRows={totalRows}
                  selectableRows
                  onChangeRowsPerPage={handlePerRowsChange}
                  onChangePage={handlePageChange}
                  onRowClicked={handleRowClick}
                  paginationPerPage={40}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Tables;
