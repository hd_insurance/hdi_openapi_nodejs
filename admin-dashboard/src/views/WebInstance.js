import React, { useState, useEffect } from 'react';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { useParams } from "react-router-dom";
import DataTable from 'react-data-table-component';

import { Container, FormInput, Row, Col, Card, CardHeader, CardBody, FormCheckbox } from "shards-react";

import { Button, Dropdown, DropdownButton } from 'react-bootstrap';

import NormalButtons from "../components/components-overview/NormalButtons";

import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';



// import Network from "../services/Network.js"
import PageTitle from "../components/common/PageTitle";
import axios from 'axios';


const columns = [
  {
    name: 'Landing',
    selector: 'NAME',
    sortable: true,
  },
  {
    name: 'Domain',
    selector: 'DOMAIN',
    sortable: true,
  },
  {
    name: 'ORG Key',
    selector: 'ORG_KEY',
    sortable: true,
  },
  {
    name: 'ID',
    selector: 'WI_ID',
    sortable: true,
  },
  {
    name: 'Action',
    button: true,
    cell: (row) => <Dropdown>
  <Dropdown.Toggle className="btn btn-outline-secondary btn-sm" id="dropdown-basic">
    Menu
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item href={`/language/${row.WI_ID}`}>Ngôn ngữ</Dropdown.Item>
   
  </Dropdown.Menu>
</Dropdown>

,
  }
];



const Tables = (props) => {
 

  const [apiList, setAPIList] = useState([])
  const [loading, setLoading] = useState(true);
  const [totalRows, setTotalRows] = useState(0);
  const [perPage, setPerPage] = useState(10);




  useEffect(() => {
    initList()
  }, []);

  const initList = async ()=>{
    try{
      const data = await axios.get(`${process.env.REACT_APP_SERVER}/api/pages-instance`)
      setAPIList(data.data.data)
      setLoading(false)
    }catch(e){

    }
  }


  const handlePageChange = page => {
     setLoading(false)
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setLoading(false)

  };



  return (
  <Container fluid className="main-content-container px-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Cấu hình Landing" subtitle="Web Instance" className="text-sm-left" />
    </Row>




    {/* Default Light Table */}
    <Row>
      <Col>
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <div className="row">
              <div className="col-md-6"><h6 className="m-0">Landing Instance</h6></div>

              <div className="col-md-6 ta-right">
                <Button size="sm" theme="secondary" className=" mr-1">
                 Thêm nội dung
                </Button>
              </div>
            </div>


          </CardHeader>
          <CardBody className="p-0 pb-3">



          
          <DataTable
            title="Page"
            columns={columns}
            data={apiList}
            progressPending={loading}
            paginationTotalRows={totalRows}
            onChangeRowsPerPage={handlePerRowsChange}
            onChangePage={handlePageChange}
          />









          </CardBody>
        </Card>
      </Col>
    </Row>

    {/* Default Dark Table */}

  </Container>
);
}

export default Tables;
