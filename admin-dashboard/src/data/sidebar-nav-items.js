export default function() {
  return [
    {
      title: "Blog Dashboard",
      to: "/blog-overview",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: ""
    },
    {
      title: "Landing Instances",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "/list-web-instance",
    },
    {
      title: "API Code",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/api-code",
    },
    // {
    //   title: "Forms & Components",
    //   htmlBefore: '<i class="material-icons">view_module</i>',
    //   to: "/components-overview",
    // },
    // {
    //   title: "Tables",
    //   htmlBefore: '<i class="material-icons">table_chart</i>',
    //   to: "/tables",
    // },
    // {
    //   title: "User Profile",
    //   htmlBefore: '<i class="material-icons">person</i>',
    //   to: "/user-profile-lite",
    // },
    {
      title: "FAQ",
        htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/faq/2",
    },
    {
      title: "Get Logs",
      htmlBefore: '<i class="material-icons">error</i>',
      to: "/get-logs",
    }
  ];
}
