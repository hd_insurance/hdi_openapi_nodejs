const config = require('../config');
const constant = require('../common/constant');

function sendError(code, errorStr, err) {
  console.log("ERROR == ", code, errorStr, err)
  return {
    code: code || 500,
    data: error(errorStr, err)
  }
}

function sendSuccess(response, signature=null) {
  return {
    success: true,
    error: null,
    error_message:null,
    data: response,
    signature: signature
  }
}

function error(errorStr, err, data=null, reqid=null) {
  return {
    success: false,
    error: errorStr || 'INTERNAL_SERVER_ERROR',
    error_message: err ? err.toString() : undefined,
    reqid: reqid,
    data: data,
    signature:null
  }
}

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}


module.exports = {
  error,
  sendError,
  sendSuccess
}
