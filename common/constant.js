const oracledb = require('oracledb');

const errorMessage = {
	PTN_NOT_FOUND: 'PARTNER_NOT_FOUND',
	SECRET_INCORRECT: 'SECRET_INCORRECT',
  ENV_INCORRECT: 'ENV_INCORRECT',
	API_ACCESS_DENIED: 'This request access is denied'
}

const errorcode = {
  INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
  UNAUTHORIZED: 'UNAUTHORIZED',
  ACCESS_DENIED: 'ACCESS_DENIED',
  INVALID_DATA: 'INVALID_DATA',
  MISSING_PARAMETER: 'MISSING_PARAMETER',
  BAD_REQUEST: 'BAD_REQUEST',
  AUTHENTICATION_FAILED: 'AUTHENTICATION_FAILED'
}

const dbmode = {
	ORACLE: 'ORACLE',
	SERVERCACHE: 'SERVERCACHE',
}

const log_type = {
  SYSTEM: 's',
  REQUEST: 'r'
}

const log_level = {
  TRACE: 't',
  REQ_ERROR: 're',
  DEBUG: 'd',
  INFO: 'i',
  WARN: 'w',
  ERROR: 'e',
  FATAL: 'f',
}
const log_priority = {
  HIGH: 'h',
  MEDIUM: 'm',
  LOW: 'l'
}

module.exports = {
  errorMessage:errorMessage,
  errorcode:errorcode,
  dbmode: dbmode,
  log_type:log_type,
  log_level:log_level,
  log_priority:log_priority,
  CUSOR: { type: oracledb.CURSOR, dir: oracledb.BIND_OUT }
}
