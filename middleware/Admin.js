const util = require('../common/util');
const constant = require('../common/constant');
var jwt = require('jsonwebtoken');

this.checkAuthentication = async (req, res, next)=>{
	try{
			if(req.headers.token){
				var decoded = jwt.verify(req.headers.token, 'hdi_log_services');
				return next();
			}else{
				return res.sendStatus(401);
			}
		  
	}catch(e){
		return res.sendStatus(401);
	}
 
}



module.exports = {
  checkAuthentication: this.checkAuthentication
}