var randomstring = require("randomstring");
const redisClient = require('../Services/Connector/Redis.js');
const app_services = require('../Services');
this.attach = async (req, res, next)=>{
	try{

		
		// const node_tracking = await redisClient.getClient().get("NODE_TRACKING")
		// if(node_tracking){
			
		// }else{
		// 	redisClient.createConnection((isConnected)=>{
		// 		if(isConnected){
		// 			app_services.app_initialization()
		// 		}else{

		// 		}
				
		// 	})
		// }
		
		req.id = randomstring.generate({
				length: 12,
				charset: 'alphabetic'
			  });
		req.device = {
			name: req.Device ? req.Device.Device_name : null,
			browser: req.Device ? req.Device.browser : null,
			agent: req.Device ? req.Device.IpPrivate : null,
			ip: req.Device ? req.Device.IpPublic : null,
			code: req.Device ? req.Device.DeviceCode : null,
			env: req.Device ? req.Device.DeviceEnvironment : null,
		}
		req.user = {
			user_id:null,
			partner:null
		}
		req.request = {
			header: JSON.stringify(req.headers),
			body: req.body,
			response: null
		}


		
	return next()
	}catch(e){
		return next()
	}
	
}

module.exports = {
  attach: this.attach
}
