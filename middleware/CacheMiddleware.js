var cache = require('memory-cache');
const RedisConnector = require('../Services/Connector/Redis.js');
const constant = require('../common/constant');
const Logging = require('../Services/Logging');
const initQuery = require('../config/InitQuery.js');
var newCache = new cache.Cache();
var crypto = require('crypto');
const dbConnector = require('../Services/Connector');
const init = require('../Services/Init.js');
const config = require('../config');

this.attach = async (req, res, next)=>{
  try{
    const req_data = req.body
    const partner_config = req.partner_config
    if(!req.body.Action){
      return next()
    }
    if(req.body.Action.ActionCode){
      const action_code = req.body.Action.ActionCode
      const api_info = await this.getAPIConfig(action_code)
      if(api_info.AUTOCACHE == 1){
        const cache_conf = await this.get_cache_cfg(api_info.API_CODE)
        if(!cache_conf)  return next()
        const cache_key = this.createCacheKey(req, api_info.API_CODE, req.environment, partner_config.CLIENT_ID, req.body.Device.IpPrivate, req.body.Device.IpPublic, (cache_conf.CLIENTTRACKING == 1), req.body.Data)
        const cache_res = await this.getResponse(cache_key)
        if(cache_res){
          return res.send(cache_res)
        }
        await this.checkBehaviorCache(req, res, api_info, req.environment, partner_config, cache_conf, cache_key)
      }
    }
    return next()
  }catch(err){
    console.log(err)
    return next()
  }
}

this.checkBehaviorCache = (req, res, api_info, env, partner_config, cache_conf, cache_key)=>{
  return new Promise(async (resolved, rejected)=>{
      if(api_info.BEHAVIOSCACHE == 1){
        const time_count = cache.get(cache_key)
        if(time_count){
          if(time_count == (cache_conf.BEHAVIORSAPPEAR)){
             cache.del(cache_key)
             await this.storeCache(cache_key, res)
          }else{
            cache.put(cache_key, (time_count+1), (cache_conf.PERIODVALIDITY*1000), function(key, value) {
            });
          }
        }else{
            cache.put(cache_key, 1, (cache_conf.PERIODVALIDITY*1000), function(key, value) {
            });
        }
      }else{
        await this.storeCache(cache_key, res)
      }
      return resolved()
  })
}

this.createCacheKey = (req, api_code, env, client_id, IpPrivate, IpPublic, isClientTrack, data)=>{
  let cacheKey = '';
  if(isClientTrack){
     cacheKey = `${client_id}${IpPrivate}${IpPublic}`
    return `API_CACHE_${env}${api_code}`+crypto.createHash('md5').update(cacheKey).digest("hex")
  }else{
    if(typeof data == 'object'){
       cacheKey = JSON.stringify(data)
    }else{
       cacheKey = ''
    }
    return `API_CACHE_${env}${api_code}`+crypto.createHash('md5').update(cacheKey).digest("hex")
  }
}

this.storeCache = (key, res)=>{
  return new Promise(async (resolved, rejected)=>{
    try{
      var oldWrite = res.write,
        oldEnd = res.end;
        var chunks = [];
        res.write = function (chunk) {
          chunks.push(Buffer.from(chunk));
          oldWrite.apply(res, arguments);
        };
        res.end = async function (chunk) {
          if (chunk)
            chunks.push(Buffer.from(chunk));
          var body = Buffer.concat(chunks).toString('utf8');
          await saveCache(key, body, 1) 
          oldEnd.apply(res, arguments);
        };
        resolved(true)
    }catch(e){
      resolved(false)
    }
  })
}

this.getAPIConfig = (api_code)=>{
  return new Promise(async (resolved, rejected)=>{
    const client = RedisConnector.getClient()
    const api_info = await client.get(api_code);
    if(api_info){
        return resolved(JSON.parse(api_info))
    }else{
        return resolved({})
    }

  })
}

this.get_cache_cfg = (api_code)=>{
  return new Promise(async (resolved, rejected)=>{
    try{
      const client = RedisConnector.getClient()
      
      const cache_cfg = await client.get(`BHVCACHE:${api_code}`); 
      if(cache_cfg){
          return resolved(JSON.parse(cache_cfg))
      }else{
          const all_behavior_cache = await dbConnector.execute(initQuery.SYS_BEHAVIOS_CACHE_GETALL, null, config.oracledb.master)
          if(all_behavior_cache[0]){
            await init.cacheBehaviorCacheSetting(all_behavior_cache[0])
          }
      }
      return resolved(false)

    }catch(error){
      console.log(error)
      return resolved(false)
    }
  })
}


this.getResponse = (key)=>{
  return new Promise(async (resolved, rejected)=>{
    const client = RedisConnector.getClient()
    const response = await client.get(key);
      if(response){
        return resolved(JSON.parse(response))
      }else{
        return resolved(null)
      }
  })
}

saveCache = (key, content, expired)=>{
  return new Promise(async (resolved, rejected)=>{
    const client = RedisConnector.getClient()
    if(expired>0){
       await client.set(key, content, 'EX', 60 * 60 * 24*expired);
    }else{
      await client.set(key, content);
    }
    return resolved()
  })
}


module.exports = {
  attach: this.attach
}

