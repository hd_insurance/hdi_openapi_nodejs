const caching = require('../controller/Caching.js');
const util = require('../common/util');
const constant = require('../common/constant');
const cyptoSign = require('../Services/SignCrypto');
const Logging = require('../Services/Logging');

this.checkAuthentication = async (req, res, next)=>{
	try{
		  const req_data = req.body
		  const environment = await this.checkSystemDomain(req_data, req, res)
		  const req_environment = req.body.Device.environment
		  if(req_environment!=environment){
		  	if(req.request==null){
		  		req.request = {}
		  	}
		  	req.request.response = util.error(constant.errorcode.UNAUTHORIZED, constant.errorMessage.ENV_INCORRECT)
		  	return res.send(util.error(constant.errorcode.UNAUTHORIZED, constant.errorMessage.ENV_INCORRECT));
		  }
		  const partner_config = await caching.getPartnerConfig(req_data.Action.ParentCode, environment.toUpperCase())
		  if(!partner_config){
		  	return res.send(util.error(constant.errorcode.UNAUTHORIZED, constant.errorMessage.PTN_NOT_FOUND));
		  }
		  if(req_data.Action.ActionCode != "HDI_API_LOGIN"){
			  const accept_api_access = await this.checkPartnerAPIAccess(partner_config.PARTNER_CODE, req_data.Action.ActionCode)
			  if(!partner_config){
			  	req.request.response = util.error(constant.errorcode.UNAUTHORIZED, constant.errorMessage.PTN_NOT_FOUND)
			  	return res.send(util.error(constant.errorcode.UNAUTHORIZED, constant.errorMessage.PTN_NOT_FOUND));
			  }
			  if(!accept_api_access){
			  	if(req.request==null){
			  		req.request = {}
			  	}
			  	console.log("accept_api_access ", accept_api_access)
			  	req.request.response = util.error(constant.errorcode.ACCESS_DENIED, constant.errorMessage.API_ACCESS_DENIED)
				return res.send(util.error(constant.errorcode.ACCESS_DENIED, constant.errorMessage.API_ACCESS_DENIED));
			  }
		  }
		  if(partner_config.SECRET!=req_data.Action.Secret){
		  	if(req.request==null){
			  		req.request = {}
			  }
		  	req.request.response = util.error(constant.errorcode.UNAUTHORIZED, constant.errorMessage.SECRET_INCORRECT)
		  	return res.send(util.error(constant.errorcode.UNAUTHORIZED, constant.errorMessage.SECRET_INCORRECT));
		  }
		  req.environment = environment
		 
		  req.partner_config = partner_config
		  if(partner_config.ISSIGNATURE_REQUEST){
		  	const verify = await this.checkSignature(req_data.Signature, partner_config)
		  }
		  return next();
	}catch(e){
		console.log(e)
		return res.send(util.error(constant.errorcode.INTERNAL_SERVER_ERROR, constant.errorMessage.INTERNAL_SERVER_ERROR));
	}

}

this.checkSystemDomain = (req_data, req, res)=>{
	return new Promise(async (resolved, rejected)=>{
		try{
 			const environment = await caching.getSystemDomain(req.get('host'))
 			return resolved(environment)
		}catch(e){
			return resolved(null)
		}
	})
}

this.checkPartnerAPIAccess = (ptn_code, api_code)=>{
	return new Promise(async (resolved, rejected)=>{
		try{
			const isAccepted = await caching.getPartnerAPIAccess(ptn_code, api_code)
			return resolved(isAccepted?true:false)
		}catch(e){
			console.log("checkPartnerAPIAccess ", false)
			return resolved(false)
		}
	})
}

this.checkSignature = async (data, partner_config)=>{
	try{
		if(partner_config.ISSIGNATURE_REQUEST === '1'){
			if(partner_config.PATH_CER){
				const encrypted_text = cyptoSign.createTextSignature(partner_config.CLIENT_ID, partner_config.PARTNER_CODE, data)
				return true
			}else{
				const encrypted_text = cyptoSign.createTextSignature(partner_config.CLIENT_ID, partner_config.PARTNER_CODE, data)
				return encrypted_text == data.Signature
			}
		}
		return true
	}catch(e){
		console.log(e)
	}
	
}


module.exports = {
  checkAuthentication: this.checkAuthentication,
  checkSignature: this.checkSignature,
  checkPartnerAPIAccess: this.checkPartnerAPIAccess
}