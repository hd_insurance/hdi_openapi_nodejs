require('dotenv').config()
require('./controller/admin/passport');
// const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger.json');
// const mediaTransporter = require('./mediatransport');
const passport = require('passport')
const config = require('./config');
const express = require('express');
const routes = require('./routes');
const bodyParser = require('body-parser');
// const WebSocket = require('ws');
const cors = require('cors');
const app_services = require('./Services');
const kafkaQueue = require('./Services/Connector/Kafka.js');
const RedisConnector = require('./Services/Connector/Redis.js');
const socketController = require('./controller/SocketController.js');
const syncDbController = require('./controller/SyncDatabase.js');
var _ = require('lodash');
const app = express();
var server = require("http").Server(app);
// const wss = new WebSocket.Server({ server })

var io = require("socket.io")(server, {
  cors: {
    origin: '*',
  }
});
const limiter = app_services.RateLimiter(config.app.rate_limiter);
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(express.static('public'))
app.set('views', __dirname + '/resources/layout');
app.set('view engine', 'ejs');
app.use(limiter);
app.use(passport.initialize())
app.use(cors());
app.options("*", cors());

// kafkaQueue.init()

// RedisConnector.createConnection((isConnected)=>{
// 	if(isConnected){
// 		app_services.app_initialization()
// 	}else{
// 		console.log("Redis Error")
// 	}
	
// })


RedisConnector.createConnection((isConnected)=>{
	if(isConnected){
		console.log("app_initialization")
		app_services.app_initialization()
	}else{
		console.log("app_initialization is error ")
	}
})



socketController.initSocket(io)

// syncDbController.initSync()
// mediaTransporter.initMediaTransporter(wss, ()=>{
// 	console.log("Khoi tao media transporter")
// })



app.use('/recache', (req, res, next)=>{
		app_services.app_initialization()
		res.send("Init cache ok")
});
app.use(routes);

server.listen(config.app.port, () => {
	console.info(`HYPERSERVER START AT PORT ${config.app.port}`)
});


