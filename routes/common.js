const Router = require('express').Router();
const authController = require('../controller/Authentication.js');
const commonController = require('../controller/Common.js');
const cacheMiddleware = require('../middleware/CacheMiddleware.js')
const authMiddleware = require('../middleware/Authentication.js')
const loggingMiddleware = require('../middleware/Logging.js')
const { celebrate, Joi, errors } = require('celebrate');


Router.get('/api/lang/:page', commonController.getLanguage)

Router.post('/api/lang/:page', commonController.addLanguage)

Router.get('/api/faq/:faqid', loggingMiddleware.attach, commonController.getPageFAQ)
Router.post('/api/faq-content/add', commonController.addFAQContent)
Router.post('/api/faq-content/edit', commonController.editFAQContent)
Router.post('/api/faq-content/delete', commonController.deleteFAQContent)

Router.get('/api/pages-instance', commonController.getPageInstances)

Router.get('/api/pages-config/:wi_id', commonController.getPageConfig)


Router.use(errors());
module.exports = Router; 