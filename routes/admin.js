const Router = require('express').Router();
const authController = require('../controller/Authentication.js');
const adminController = require('../controller/admin/AdminController.js');
const landingController = require('../controller/admin/AdminLanding.js');
const landingEditorController = require('../controller/admin/LandingEditorController.js');
const passport = require('passport')
const cacheMiddleware = require('../middleware/CacheMiddleware.js')
const authMiddleware = require('../middleware/Admin.js')
const loggingMiddleware = require('../middleware/Logging.js')
const { celebrate, Joi, errors } = require('celebrate');




Router.post('/api/admin/sign-in', (req, res, next) => {
	passport.authenticate('local', function (err, user, info) {
		if(user){
			return res.status(200).send({login:true, data:user})
		}
		return res.status(401).send({login:false, data:{info}})
	})(req, res, next);
})
 Router.get('/api/admin/faq-config', authMiddleware.checkAuthentication, landingController.getFAQList)
 Router.get('/api/admin/faq-config/:faq_id', authMiddleware.checkAuthentication, landingController.getFAQDetail)
 Router.post('/api/admin/faq-config/:faq_id', authMiddleware.checkAuthentication, landingController.updateFAQDetail)
 Router.post('/api/admin/remove-faq', authMiddleware.checkAuthentication, landingController.removeFAQ)
 Router.post('/api/admin/add-faq', authMiddleware.checkAuthentication, landingController.addNewFAQ)
 Router.post('/api/admin/duplicate-faq', authMiddleware.checkAuthentication, landingController.cloneFAQ)
 Router.get('/api/get-layout-editor/:layout_id', landingEditorController.getLayoutEditor)
 Router.get('/api/get-landing-list', authMiddleware.checkAuthentication, landingController.getLandingList)
 Router.get('/api/get-landing-page/list/:wi_id', authMiddleware.checkAuthentication, landingController.getPageList)
 Router.post('/api/landing-page/update-page/:page_id', authMiddleware.checkAuthentication, landingController.updatePageInfo)
 Router.get('/api/landing-layout/list', authMiddleware.checkAuthentication, landingController.getListLayout)
 Router.get('/api/landing-layout/:layout_id', landingController.getLayoutJson)
 Router.post('/api/landing-layout/update/:layout_id', authMiddleware.checkAuthentication, landingController.updateLayoutJson)
 Router.get('/api/landing-layout/editor-history/:layout_id', authMiddleware.checkAuthentication, landingController.getLayotEditHistory)
 Router.post('/api/landing-layout/editor-history/revert', authMiddleware.checkAuthentication, landingController.revertLayout)
 Router.get('/api/admin/page-info/:page_id', landingController.getPageInfo)



// Router.get('/api/admin/list/action-code', adminController.getListActionCode)
// Router.get('/api/admin/action-code/params/:owner/:package/:store_code', adminController.getAPIParam)
// Router.get('/api/admin/action-code/params', adminController.getAllAPIParam)

// Router.get('/api/testqueue', adminController.testQueue)
// Router.get('/api/admin/list/landing', adminController.getLandingList)
// Router.get('/api/admin/list/languagegroup', adminController.getListLanguageGroup)
// Router.get('/api/admin/language/:groupId', adminController.getLanguageByGroupId)
// Router.post('/api/admin/languageTrans', adminController.insertLanguage)
// Router.post('/api/admin/languageBlock', adminController.insertBlockLanguage)

// //landing
// Router.get('/api/admin/list-page/:instance_id', landingController.getListPageByInstance)
// Router.get('/api/admin/page-layout/:page_id', landingController.getPageLayoutSection)



Router.use(errors());
module.exports = Router;