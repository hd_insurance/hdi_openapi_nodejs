const Router = require('express').Router();
const mainRoutes = require('./main');
const fileRoutes = require('./file');
const webrtcRoutes = require('./webrtc');
const commonRoutes = require('./common');
const adminRoutes = require('./admin');
const queueRoutes = require('./queue');
const logRoutes = require('./logs');
const testRoutes = require('./test');

Router.use(mainRoutes);
Router.use(fileRoutes);
Router.use(webrtcRoutes);
Router.use(commonRoutes);
Router.use(logRoutes);
Router.use(adminRoutes);
Router.use(queueRoutes);
Router.use(testRoutes);
Router.all('/', function(req, res, next){
	res.render('views/index.ejs');
})
Router.all('*', function(req, res, next){
	res.render('views/page-404.ejs');
})



module.exports = Router;