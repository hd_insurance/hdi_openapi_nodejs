const Router = require('express').Router();
const authController = require('../controller/Authentication.js');
const webRtcController = require('../controller/WebRTC.js');
const cacheMiddleware = require('../middleware/CacheMiddleware.js')
const authMiddleware = require('../middleware/Authentication.js')
const loggingMiddleware = require('../middleware/Logging.js')
const { celebrate, Joi, errors } = require('celebrate');


	

Router.get('/api/media/remove-temp-cache',  webRtcController.removeTempFile)

Router.post('/api/media/transshipment/emit/:transId',  webRtcController.submitSocket)

Router.get('/api/media/transshipment/:trans_id', loggingMiddleware.attach, webRtcController.checkTransshipmentInfo)
Router.get('/api/media/videothumbnail/:videofileid', loggingMiddleware.attach, webRtcController.getThumbVideo)

Router.get('/api/media/cache/videothumbnail/:trans_id/:videofileid', webRtcController.getThumbCacheVideo)
Router.get('/api/media/cache/video/:trans_id/:videofileid', webRtcController.getCacheVideo)
Router.get('/api/media/cache/photothumbnail/:trans_id/:videofileid', webRtcController.getThumbCachePhoto)

Router.post('/api/media-transshipment/upload-video/:stream_token/:media_id', loggingMiddleware.attach, webRtcController.uploadVideoBlob)
Router.post('/api/media-transshipment/upload/:stream_token', loggingMiddleware.attach, webRtcController.uploadBase64)
Router.post('/api/media-transshipment/upload-other/:stream_token', loggingMiddleware.attach, webRtcController.uploadBase64Array)





Router.get('/api/wrtc/:trans_id/delete-other-media/:media_id',  webRtcController.deleteOtherMedia)

Router.post('/api/wrtc/:trans_id/commit-video/:media_id',  webRtcController.commitVideo)



Router.use(errors());
module.exports = Router; 