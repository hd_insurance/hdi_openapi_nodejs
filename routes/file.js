const Router = require('express').Router();
const fileController = require('../controller/File');
const authController = require('../controller/FileAuthentication');
const { celebrate, Joi, errors } = require('celebrate');
const config = require('../config');

Router.get('/f/:file_key', authController.checkAuthenticationView, fileController.getFile)

Router.post('/upload', authController.checkAuthenticationUpload, fileController.upFile)

Router.get('/removef/:file_key', authController.checkAuthenticationView, fileController.removeFile) //QuynhTM xóa file

Router.post('/upload/initialization', authController.checkAuthenticationUpload, fileController.initialization)


Router.use(errors());
module.exports = Router;