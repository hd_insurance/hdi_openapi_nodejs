const Router = require('express').Router();
const QueueController = require('../controller/QueueController.js');
const { celebrate, Joi, errors } = require('celebrate');
const config = require('../config');

Router.get('/api/queue', QueueController.getQueueAPICode)



Router.use(errors());
module.exports = Router;