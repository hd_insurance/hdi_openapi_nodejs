const Router = require('express').Router();
const TestController = require('../controller/Test.js');
const { celebrate, Joi, errors } = require('celebrate');
const authMiddleware = require('../middleware/Authentication.js')
const config = require('../config');




var ts1 = 	
	{
	  body: Joi.object().keys({
	    Device: Joi.object().keys({
		    DeviceId: Joi.string().required().allow('').max(100),
		    DeviceCode: Joi.string().required().allow('').max(100),
		    Device_name: Joi.string().required().allow('').max(100),
		    IpPrivate: Joi.string().required().allow('').max(100),
		    IpPublic: Joi.string().required().allow('').max(100),
		    X: Joi.string().allow('').max(100),
		    Y: Joi.string().allow('').max(100),
		    province: Joi.string().allow('').max(100),
		    district: Joi.string().allow('').max(100),
		    wards: Joi.string().allow('').max(100),
		    address: Joi.string().allow('').max(100),
		    environment: Joi.string().required().valid(
			  'DEV', 'TEST', 'LIVE'
			), 
		    browser: Joi.string().allow('').max(100),
		    DeviceEnvironment: Joi.string().uppercase()
		}),
		Action: Joi.object().keys({
		  ParentCode: Joi.string().allow('').allow(null),
		  UserName: Joi.string().allow('').allow(null),
		  Secret: Joi.string().allow('').allow(null),
		  ActionCode: Joi.string().required(),
		}).required(),
		Data: Joi.object().allow(null),
		Signature: Joi.string().allow('').allow(null)
	  })
	}





Router.get('/api/hoangchimto', TestController.testHoang)

// /api/get-queue-producing/TEST_PUB_QUEUE


Router.use(errors());
module.exports = Router;