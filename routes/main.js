const Router = require('express').Router();
const mainController = require('../controller/Main.js');
const queueController = require('../controller/QueueController.js');
const authController = require('../controller/Authentication.js');
const webRtcController = require('../controller/WebRTC.js');
const LGController = require('../controller/lg');
const cacheMiddleware = require('../middleware/CacheMiddleware.js')
const authMiddleware = require('../middleware/Authentication.js')
const loggingMiddleware = require('../middleware/Logging.js')
const APICache = require('../Services/APICache.js');
const { celebrate, Joi, errors } = require('celebrate');

var ts1 = 	
	{
	  body: Joi.object().keys({
	    Device: Joi.object().keys({
		    DeviceId: Joi.string().required().allow('').max(100),
		    DeviceCode: Joi.string().required().allow('').max(100),
		    Device_name: Joi.string().required().allow('').max(100),
		    IpPrivate: Joi.string().required().allow('').max(100),
		    IpPublic: Joi.string().required().allow('').max(100),
		    X: Joi.string().allow('').max(100),
		    Y: Joi.string().allow('').max(100),
		    province: Joi.string().allow('').max(100),
		    district: Joi.string().allow('').max(100),
		    wards: Joi.string().allow('').max(100),
		    address: Joi.string().allow('').max(100),
		    environment: Joi.string().required().valid(
			  'DEV', 'TEST', 'LIVE'
			),
		    browser: Joi.string().allow('').max(100),
		    DeviceEnvironment: Joi.string().uppercase()
		}),
		Action: Joi.object().keys({
		  ParentCode: Joi.string().allow('').allow(null),
		  UserName: Joi.string().allow('').allow(null),
		  Secret: Joi.string().allow('').allow(null),
		  ActionCode: Joi.string().required(),
		}).required(),
		Data: Joi.object().allow(null),
		Signature: Joi.string().allow('').allow(null)
	  })
	}
var ts_queue = 	
	{
	  body: Joi.object().keys({
	  	Device: Joi.object().keys({
		    environment: Joi.string().required().valid(
			  'DEV', 'TEST', 'LIVE'
			)
		}),
		Action: Joi.object().keys({
		  ParentCode: Joi.string().allow('').allow(null),
		  UserName: Joi.string().allow('').allow(null),
		  Secret: Joi.string().allow('').allow(null),
		  ActionCode: Joi.string().required(),
		}).required(),
		Data: Joi.array().items(Joi.object({
			key: Joi.string().required(),
			value: Joi.string().required()
		}).required()),
		Signature: Joi.string().allow('').allow(null)
	  })
	}

var ts_auth = 	
	{
	  body: Joi.object().keys({
	    Device: Joi.object().keys({
		    DeviceId: Joi.string().required().allow('').max(100),
		    DeviceCode: Joi.string().required().allow('').max(100),
		    Device_name: Joi.string().required().allow('').max(100),
		    IpPrivate: Joi.string().required().allow('').max(100),
		    IpPublic: Joi.string().required().allow('').max(100),
		    X: Joi.string().allow(''),
		    Y: Joi.string().allow(''),
		    province: Joi.string().allow(''),
		    district: Joi.string().allow(''),
		    wards: Joi.string().allow(''),
		    address: Joi.string().allow('').max(100),
		    environment: Joi.string().required().allow('').max(100),
		    browser: Joi.string().required().allow('').max(100),
		    DeviceEnvironment: Joi.string().lowercase().required().valid(
			  'web', 'app'
			)
		}),
		Action: Joi.object().keys({
		  ParentCode: Joi.string().allow('').allow(null),
		  UserName: Joi.string().allow('').allow(null),
		  Secret: Joi.string().allow('').allow(null),
		  ActionCode: Joi.string().required(),
		}).required(),
		Data: Joi.object().keys({
		  UserName: Joi.string().required(),
		  Password: Joi.string().allow('').required()
		}).required(),
		Signature: Joi.string().allow('').allow(null)
	  })
	}

Router.post('/api/media-transshipment/initialization', celebrate(ts1), authMiddleware.checkAuthentication, webRtcController.createQR)

Router.post('/api/media-transshipment/commit', celebrate(ts1), authMiddleware.checkAuthentication, webRtcController.commitMedia)

Router.post('/open-api/pub-queue', celebrate(ts_queue), loggingMiddleware.attach, authMiddleware.checkAuthentication, queueController.publish)

Router.post('/open-api', celebrate(ts1), loggingMiddleware.attach, authMiddleware.checkAuthentication, cacheMiddleware.attach, mainController.main)
Router.post('/api/login', celebrate(ts_auth), loggingMiddleware.attach, authMiddleware.checkAuthentication, authController.login)




Router.use(errors());
module.exports = Router;  