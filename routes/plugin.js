const Router = require('express').Router();
const authController = require('../controller/Authentication.js');
const pluginController = require('../controller/Plugin.js');
const cacheMiddleware = require('../middleware/CacheMiddleware.js')
const authMiddleware = require('../middleware/Authentication.js')
const loggingMiddleware = require('../middleware/Logging.js')
const { celebrate, Joi, errors } = require('celebrate');


Router.get('/api/plugin/zalo/link', loggingMiddleware.attach, pluginController.zaloLink)
Router.get('/api/plugin/zalo/callback', loggingMiddleware.attach, pluginController.zaloLinkCallback)
Router.get('/api/plugin/zalo/test', loggingMiddleware.attach, pluginController.getListFriendUser)


Router.use(errors());
module.exports = Router; 