const Router = require('express').Router();
const logController = require('../controller/Logs');
const config = require('../config');
const authMiddleware = require('../middleware/Admin.js')

Router.get('/logs/app-list', authMiddleware.checkAuthentication, logController.getListApp)
Router.post('/logs', logController.addLog2)
Router.get('/getlogs', authMiddleware.checkAuthentication, logController.getAllLogs)
Router.get('/getlog/:req_id', authMiddleware.checkAuthentication, logController.getLogReq)
Router.get('/clear-logs', authMiddleware.checkAuthentication, logController.clearAllLogs)
Router.get('/get-logs-chart', logController.getChartData)

module.exports = Router;