const config = require("../config");
const util = require("../common/util");
const moment = require("moment");
const MongoConnector = require("../Services/Connector/MongoDB.js");
const {ObjectId} = require('mongodb');
const logdb = new MongoConnector();
logdb.createConnection();



async function getAllLogs(req, res) {
    var page = 0;
    var perpage = 10;
    if(req.query.perpage){
      perpage = req.query.perpage*1
    }
    if(req.query.page){
      page = perpage*req.query.page
    }
    var logsData = {};
    try {
     
      const collection = logdb.getClient().collection("event_logs");
      const count = await collection.count()
      logsData.total = count
      let aggregate_opts = [
            {
              $lookup:{
                    from: "app",
                    localField : "app",
                    foreignField : "_id",
                    as : "app_info"
                }
            },
            { $sort: { time: -1 } },
            { $skip : page },
            { $limit : perpage },
            { $project: { content:0 } }
      ]

      if(req.query.app){
        if(req.query.app!='undefined'){
          let app_list_psr = req.query.app.split(',')
          app_list_psr = app_list_psr.map((item, index)=>{
            item = ObjectId(item)
            return item
          })
          aggregate_opts.unshift({ $match: { 'app':{'$in' : app_list_psr} } })
        }
      }
      const logs_list = await collection.aggregate(aggregate_opts).toArray()
      logsData.list = logs_list
      res.send(logsData);
    } catch (e) {
      res.send({});
    }
}

async function getLogReq(req, res) {
    try {
      const collection = logdb.getClient().collection("event_logs");
      const aggregate_opts = [
            { $match: { group_id: req.params.req_id } },
            {$lookup:{
                    from: "app",
                    localField : "app",
                    foreignField : "_id",
                    as : "app_info"
                }
            },
            { $sort: { time: 1 } },
            { $limit : 55 }
      ]

      const logs_list = await collection.aggregate(aggregate_opts).toArray()
      res.send(logs_list);
    } catch (e) {
      res.send([]);
    }
}

async function getChartData(req, res) {
    try {
      let listvalue_trace = []
      let listvalue_error = []
      let listvalue_warning = []
      let listlabel = []
      const collection = logdb.getClient().collection("event_logs");
      //chart 1
      for(var i = 20; i>=0; i--){
        let date = new Date();
        date.setDate(date.getDate() - i);
        let d = date.toISOString().split('T')[0]
        listlabel.push(d)
        listvalue_trace.push(0)
        listvalue_error.push(0)
        listvalue_warning.push(0)
      }
      const aggregate_opts_trace = [
            { $match: { severity: "info", time : {"$gt" : new Date(Date.now() - 20*24*60*60 * 1000) } } },
            {
                $project:
                {
                    month: { $dateToString: { format: "%Y-%m-%d", date: "$time" } },
                }
            },
            {
                $group: {
                    _id: { time: "$month"},
                    numlog: { $sum: 1 }
                }
            },

            {
                $addFields: {
                    time: "$_id.time"
                }
            },
            {
                $project: {
                    _id: false
                }
            },
            { $sort: { time: 1} }

      ]
      const aggregate_opts_error = [
            { $match: { severity: "error", time : {"$gt" : new Date(Date.now() - 20*24*60*60 * 1000) } } },
            {
                $project:
                {
                    month: { $dateToString: { format: "%Y-%m-%d", date: "$time" } },
                }
            },
            {
                $group: {
                    _id: { time: "$month"},
                    numlog: { $sum: 1 }
                }
            },

            {
                $addFields: {
                    time: "$_id.time"
                }
            },
            {
                $project: {
                    _id: false
                }
            },
            { $sort: { time: -1} }

      ]

      const aggregate_opts_warning = [
            { $match: { severity: "warning", time : {"$gt" : new Date(Date.now() - 20*24*60*60 * 1000) } } },
            {
                $project:
                {
                    month: { $dateToString: { format: "%Y-%m-%d", date: "$time" } },
                }
            },
            {
                $group: {
                    _id: { time: "$month"},
                    numlog: { $sum: 1 }
                }
            },

            {
                $addFields: {
                    time: "$_id.time"
                }
            },
            {
                $project: {
                    _id: false
                }
            },
            { $sort: { time: -1} }

      ]

      const logs_list_trace = await collection.aggregate(aggregate_opts_trace).toArray()
      const logs_list_error = await collection.aggregate(aggregate_opts_error).toArray()
      const logs_list_warning = await collection.aggregate(aggregate_opts_warning).toArray()
      const count_1 = await collection.count({severity: "error"})
      const count_3 = await collection.count({severity: "warning"})
      const count_2 = await collection.count({severity: "info"})

      logs_list_trace.forEach((item, index)=>{

          const ind = listlabel.indexOf(item.time)
          listvalue_trace[ind] = (item.numlog)
      })
      logs_list_error.forEach((item, index)=>{
          const ind = listlabel.indexOf(item.time)
          listvalue_error[ind] = (item.numlog)
      })
      logs_list_warning.forEach((item, index)=>{
          const ind = listlabel.indexOf(item.time)
          listvalue_warning[ind] = (item.numlog)
      })
     
      res.send({
        total_error: count_1,
        total_trace: count_2,
        total_warning: count_3,
        label: listlabel,
        trace: listvalue_trace,
        error: listvalue_error,
        warning: listvalue_warning
      });
    } catch (e) {
      console.log(e)
      res.send([]);
    }
}


function getListApp(req, res) {
  return new Promise(async (resolved, rejected) => {
    const collection = logdb.getClient().collection("app");
    try {
      const _result = await collection.find({}).toArray();
      res.send({success:true, apps:_result});
    } catch (e) {
      rres.send({success:false, apps: []});
    }
  });
}



function clearAllLogs(req, res) {
  let default_days = 7;
  if(req.query.day){
    default_days = req.query.day*1
  }
  return new Promise(async (resolved, rejected) => {
    const collection = logdb.getClient().collection("req_logs");
    var logsList = [];
    try {
      const remove_result = await collection.deleteMany( { time : {"$lt" : new Date(Date.now() - default_days*24*60*60 * 1000) } })
      res.send({success:true, count: remove_result.result.n});
    } catch (e) {
      res.send({success: false});
    }
  });
}


// const addLog = async (req, res)=>{
//   try{
//     const collection = logdb.getClient().collection("req_logs");
//     const log_item = { 
//       request_id: req.body.req_id,
//       time: new Date(),
//       source: req.body.source||"none",
//       app: ObjectId(req.body.app),
//       title: req.body.title,
//       evt_type: req.body.evt_type,
//       log_type: req.body.log_type,
//       content: req.body.content
//     }
//     const insert_result = await collection.insertOne(log_item)
//     res.sendStatus(200)
//   }catch(e){
//     res.sendStatus(200)
//   }
// }


const addLog2 = async (req, res)=>{
  try{
    const collection = logdb.getClient().collection("event_logs");
    const log_item = { 
      group_id: req.body.group_id,
      time: new Date(),
      source: req.body.source||"none",
      app: ObjectId(req.body.app),
      title: req.body.title,
      severity: req.body.severity,
      type: req.body.type,
      content: req.body.content
    }
    const insert_result = await collection.insertOne(log_item)
    res.send({log_id: insert_result.insertedId})
  }catch(e){
    res.send({log_id: null})
  }
}



module.exports = {
  // addLog,
  addLog2,
  getAllLogs,
  clearAllLogs,
  getLogReq,
  getListApp,
  getChartData
};
