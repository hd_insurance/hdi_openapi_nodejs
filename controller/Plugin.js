const config = require('../config');
const constant = require('../common/constant');
const RedisConnector = require('../Services/Connector/RedisConnector.js');
const Logging = require('../Services/Logging');
const util = require('../common/util');
const axios = require('axios');


const zaloLink = (req, res)=>{
	const redirect_uri = 'https://dev-hyperservices.hdinsurance.com.vn/api/plugin/zalo/callback'
	const idForm = "12ABF56GH"
	res.redirect(`https://oauth.zaloapp.com/v3/permission?app_id=${config.plugin3rd.zalo.appid}&redirect_uri=${redirect_uri}&state=${idForm}`)
}

const zaloLinkCallback = async (req, res)=>{
	try{
		const redis = new RedisConnector(config.redis.cdb)
		const client = redis.getClient()
		const data = req.query
		if(data.code && data.state && data.uid){
			const keyname = `ZALO_USR_${data.uid}`
			const keyname_form = `ZALO_FORM_${data.state}`
			const listuser = []
			listuser.push(data.uid.toString())
			//set value + token to redis
			await client.set(keyname, data.code);
			await client.set(keyname_form, JSON.stringify(listuser));
		}
		redis.quit()
		res.send("OK")

	}catch(e){
		console.log(e)
		const response = util.error(constant.errorcode.BAD_REQUEST, "ZALO CALLBACK ERROR", e, req.id)
		req.request.response = response
		return res.send(response);
	}
}

const getListFriendUser = async (req, res)=>{
	try{
		const message = "Hoang dep trai"
		const sendtoID = ""
		const userAccessToken = ""
		// const response = await axios.get(`https://graph.zalo.me/v2.0/me/friends?access_token=<User_Access_Token>&from=0&limit=5&fields=id,name,gender,picture`);
		res.send("OK")



	}catch(e){
		console.log(e)
		const response = util.error(constant.errorcode.BAD_REQUEST, "ZALO ERROR", e, req.id)
		req.request.response = response
		return res.send(response);
	}
}


module.exports = {
  zaloLink,
  zaloLinkCallback,
  getListFriendUser
}