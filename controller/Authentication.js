const caching = require('./Caching.js');
const util = require('../common/util');
const constant = require('../common/constant');
const config = require('../config/index.js');
const cyptoSign = require('../Services/SignCrypto');
const oracleDB = require('../Services/Connector/OracleDB.js');
const RedisConnector = require('../Services/Connector/Redis.js');
const Logging = require('../Services/Logging');
const initQuery = require('../config/InitQuery.js');
const app_init = require('../Services/Init.js');
const oracledb = require('oracledb');
var randomstring = require("randomstring");

this.login = async (req, res, next)=>{
	try{
		let isLoginSuccess = true
		const usr = req.body.Data.UserName
		const psw = req.body.Data.Password
		const prc = req.body.Action.ParentCode
		const userinfo = await this.getUser(usr, req.environment)
		if(userinfo){
			const vrf_pwd = this.verifyPassword(userinfo.PASSWORD, psw)
			if(!vrf_pwd){
				isLoginSuccess = false
			}
			if(!isLoginSuccess){
				return res.send(util.error(401, constant.errorcode.AUTHENTICATION_FAILED, constant.errorcode.AUTHENTICATION_FAILED));
			}
			const token = randomstring.generate(32);
			const login_response = {
					    Success: true,
					    Error: "",
					    ErrorMessage: "",
					    Token: token.toUpperCase(),
					    Expries: config.app.login_timeout*60,
					    RefeshToken: "",
					    Signature: null
					}
			
			// console.log("SAVE KEY = ", `${usr}${req.body.Action.Secret}${req.body.Device.DeviceEnvironment.toUpperCase()}`)
			await RedisConnector.getClient().set(`${usr}${req.body.Action.Secret}${req.body.Device.DeviceEnvironment.toUpperCase()}`, `"${token.toUpperCase()}"`, 'EX', config.app.login_timeout * 60)
			return res.send(login_response)
		}else{
			return res.send(util.error(401, constant.errorcode.AUTHENTICATION_FAILED, constant.errorcode.AUTHENTICATION_FAILED));
		}
	}catch(error){
		console.log(error)
		if(error == constant.errorcode.UNAUTHORIZED){
			return res.send(util.error(401, constant.errorcode.AUTHENTICATION_FAILED, constant.errorMessage.AUTHENTICATION_FAILED));
		}else{
			return res.send(util.error(503, constant.errorcode.BAD_REQUEST, constant.errorcode.BAD_REQUEST));
		}
		
	}
}

this.getUser = (username, environment)=>{
	return new Promise(async (resolved, rejected)=>{
		const keyname = `${config.app.prefix}_USR_${username}_${environment.toUpperCase()}`
		const dbset = await RedisConnector.getClient().hgetall(keyname);
		
		if(dbset==null || dbset==false){
		  const params = {username: username.toUpperCase(), cs1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }}
		  const get_partner_db = await oracleDB.execute(initQuery.SYS_USER_GET_BY_USRNAME, params, config.oracledb.master)
		  	if(get_partner_db[0].length > 0){
		  		app_init.cacheUser(get_partner_db[0])
		  		return resolved(get_partner_db[0][0])
		  	}else{
		  		return resolved(null)
		  	}
		  }
		return resolved(dbset)

	})
}

this.verifyPassword = (p1, p2)=>{
	return p1==p2
}
this.gentoken = ()=>{
	
}
module.exports = {
  login: this.login,
  getUser: this.getUser
}