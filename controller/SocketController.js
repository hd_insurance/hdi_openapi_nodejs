const config = require('../config');
const constant = require('../common/constant');
const RedisConnector = require('../Services/Connector/Redis.js');
const Logging = require('../Services/Logging');
const util = require('../common/util');
const axios = require('axios');

var skio = null


const initSocket = (io)=>{
	skio = io
	io.on("connection", async function(socket)
	{
		const id = socket.handshake.query.id
		const channel = socket.handshake.query.channel
		if(channel == "transshipment"){
			const client = RedisConnector.getClient()
		    const rd_file_key = `TSM_CONNECT_${id}`
		    await client.set(rd_file_key, socket.id.toString(), 'EX', 30 * 60 * 60 * 24);
		}
	});
}
const emit = (socketid, key, data)=>{
	if(skio){
		skio.to(socketid).emit(key, data);
	}
}
module.exports = {
 initSocket,
 emit
}