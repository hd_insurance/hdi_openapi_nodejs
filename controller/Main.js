const DBExecute = require('../Services/DBExecute.js');
const constant = require('../common/constant');
const util = require('../common/util');
const cyptoSign = require('../Services/SignCrypto');
const Logging = require('../Services/Logging');
async function main(req, res, next) {
	try{
		try{
			Logging.push(req.id, "info", "request", {header: JSON.stringify(req.headers), body: req.body}, `OpenAPI Request / ${req.body.Action.ActionCode}`)
		}catch(e){
			console.log("Log error ", e)
		}
		
		const response = await DBExecute.executeFunction(req)
		const partner_config = req.partner_config
		let signature = null
		if(req.partner_config.ISSIGNATURE_RESPONSE){
			signature = cyptoSign.createTextSignature(partner_config.CLIENT_ID, partner_config.PARTNER_CODE, JSON.stringify(response))
		}
		req.request.response = response

		res.send(util.sendSuccess(response, signature))
	}catch(e){
		if(e === constant.errorcode.MISSING_PARAMETER || e === constant.errorcode.INVALID_DATA ){
			const response = util.error(constant.errorcode.BAD_REQUEST, e, null, req.id)
			req.request.response = response
			Logging.push(req.id, "error", "request", {response:response}, `OpenAPI Response / ${req.body.Action.ActionCode}`)
			return res.send(response);
		}else{
			if(e.message){
				const response = util.error(constant.errorcode.BAD_REQUEST, e.message, e, req.id)
				req.request.response = response
				Logging.push(req.id, "error", "request", {response: response}, `OpenAPI Response / ${req.body.Action.ActionCode}`)

				return res.send(response);
			}else{
				const response = util.error(constant.errorcode.BAD_REQUEST, e, null, req.id)
				req.request.response = response
				Logging.push(req.id, "error", "request", {response: response}, `OpenAPI Response / ${req.body.Action.ActionCode}`)
				return res.send(response);
			}
			
		}
	}
}
async function testSend(req, res, next){
	res.send('OK')
}

module.exports = {
  main,
  testSend
}
