const config = require('../config');
const util = require('../common/util');
const oracledb = require('oracledb');
const mime = require('mime-types')
var static = require('node-static');
const forEach = require('async-foreach').forEach;
const redisDb = require('../Services/Connector/Redis.js');
const initQuery = require('../config/InitQuery.js');
var randomstring = require("randomstring");
const url = require('url');
const uid = require('uid').uid;
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const oracleDB = require('../Services/Connector/OracleDB.js');
const Logging = require('../Services/Logging');

async function initialization(req, res) {
	try{
		  var count = req.body.count?req.body.count*1:1
		  var array_id = Array(count).fill(0)
		  var file_ext = req.body.file_ext || "pdf"
		  var originalname = req.body.original_name+"."+file_ext.trim()
		  var permission = 0
		  var tags = req.body.tags || null
		  var subpath = ""

		  if(req.body.subpath){
	    		const isValid = isValidExistingDirectory(req.body.subpath)
	    		if(isValid){
	    			subpath = req.body.subpath.substring(1)
	    		}else{
	    			return res.send(util.sendError(400, "SUB_PATH_INVALID", 'Subpath is invalid format'))
	    		}
		   }
		  forEach(array_id, async function(item, index){
		  		// var done = this.async()
		  		const file_key = uid(32)
		  		let filename = file_key+"."+file_ext.trim()
		  		array_id[index] = {
		  			file_key: file_key,
		  			filename: filename,
		  			original_name: originalname
		  		}
		  		const insert_result = await insertFileInfo(file_key, originalname, filename, req.partner_cfg.BUCKET.toLowerCase(), req.partner_cfg.PATH_ROOT.toLowerCase(), req.ParentCode, req.ParentCode, req.header('ActionCode'), permission, subpath, tags)
		  		// done()
		  }, function(){
		  	res.send(util.sendSuccess({
		  		full_path: req.partner_cfg.PATH_ROOT.toLowerCase() + req.partner_cfg.BUCKET.toLowerCase() + subpath,
		  		files: array_id
		  	}))
		  })
	}catch(e){
		console.log(e)
		return res.send(util.sendError())
	}
}

async function getFile(req, res) {
	try{
		  const request_id = randomstring.generate({length: 12})
		  const ext = path.parse(req.file_info.FILE_NAME).ext;
		  let fullpath = req.file_info.ROOT_FOLDER+req.file_info.FILE_BUCKETS+req.file_info.FILE_NAME
		  if(req.file_info.SUB_FOLDER){
		  	fullpath = req.file_info.ROOT_FOLDER+req.file_info.FILE_BUCKETS+req.file_info.SUB_FOLDER+"/"+req.file_info.FILE_NAME
		  }
		  fs.exists(fullpath, function (exist) {
		    if(!exist) {
		    	Logging.push(request_id, "warning", "request", {fullpath:fullpath }, `Get file - not exists / Error Get file`)
		      return res.send(util.sendError(`Can't find file with name ${req.file_info.ORIGINAL_NAME}`,'FILE_NOT_FOUND'))
		    }
		    // if is a directory search for index file matching the extention
		    if (fs.statSync(fullpath).isDirectory()){
		    	return res.send(util.sendError('ACCESS DENIED'))
		    }
		    if(req.query.download){
		      	res.download(fullpath, req.file_info.ORIGINAL_NAME);
		    }else{
		    	let fpath = req.file_info.ROOT_FOLDER+req.file_info.FILE_BUCKETS
		    	if(req.file_info.SUB_FOLDER){
				fpath = req.file_info.ROOT_FOLDER+req.file_info.FILE_BUCKETS+req.file_info.SUB_FOLDER
			}
		    	var fileServer = new static.Server(fpath, { cache: 600 });
	      	fileServer.serveFile(req.file_info.FILE_NAME, 200, {}, req, res);
		    }
		  });

	}catch(e){
		console.log(e)
		Logging.push(request_id, "warning", "request", {error: e.toString(), fpath: fpath, fullpath:fullpath }, `Get file: FILE_INVALID / Error Get file`)
		return res.send(util.sendError())
	}
}

//Quynhtm dev
async function removeFile(req, res) {
	try{
		const request_id = randomstring.generate({length: 12})
		const ext = path.parse(req.file_info.FILE_NAME).ext;
		let fullpath = req.file_info.ROOT_FOLDER+req.file_info.FILE_BUCKETS+req.file_info.FILE_NAME
		if(req.file_info.SUB_FOLDER){
			fullpath = req.file_info.ROOT_FOLDER+req.file_info.FILE_BUCKETS+req.file_info.SUB_FOLDER+"/"+req.file_info.FILE_NAME
		}
		fs.exists(fullpath, function (exist) {
			if(!exist) {
				Logging.push(request_id, "warning", "request", {fullpath:fullpath }, `Remove file - not exists / Error Remove file`)
				return res.send(util.sendError(`Can't find file with name ${req.file_info.ORIGINAL_NAME}`,'FILE_NOT_FOUND'))
			}
			// if is a directory search for index file matching the extention
			if (fs.statSync(fullpath).isDirectory()){
				return res.send(util.sendError('ACCESS DENIED'))
			}

			if (fs.existsSync(fullpath)) {
				var readStream = fs.createReadStream(fullpath);
				readStream.on('close', function () {
					fs.unlink(fullpath, callback);
				});
				return true;
			}else{
				return false;
			}
		});
	}catch(e){
		console.log(e)
		Logging.push(request_id, "warning", "request", {error: e.toString(), fpath: fpath, fullpath:fullpath }, `Remove file: FILE_INVALID / Error Remove file`)
		return res.send(util.sendError())
	}
}


function upFile(req, res){
	try{
		const request_id = randomstring.generate({length: 12})
		
		//Create bucket
		let check_bucket = ''
		let full_path_folder = ''
		let bucket_path = req.partner_cfg.PATH_ROOT+req.partner_cfg.BUCKET.toLowerCase();
		let subpath = null
		if(req.partner_cfg.BUCKET){
			let bucket = bucket_path
			full_path_folder = bucket_path
			if(req.query.subpath){
		    		const isValid = isValidExistingDirectory(req.query.subpath)
		    		if(isValid){
		    			subpath = req.query.subpath
		    			subpath = subpath.substring(1)
		    			bucket = bucket + subpath
		    			full_path_folder += subpath
		    		}else{
		    			return res.send(util.sendError(400, "SUB_PATH_INVALID", 'Subpath is invalid format'))
		    		}
		    	}
			const bk_arr = bucket.split("/");

			if(bk_arr.length > 0){
				forEach(bk_arr, function(item, index, arr) {
				  if(item){
				  	check_bucket = check_bucket+'/'+item
				  	if (!fs.existsSync(check_bucket)){
					    fs.mkdirSync(check_bucket);
					}
				  }
				});
			}
		}

		const fileExtValid = async (req, file, cb) => {
			const enb_file_ext = req.partner_cfg.FILE_EXTENSIONS
			if(enb_file_ext=="*"){
				return cb(null, true);
			}
			if(enb_file_ext==null){
				cb(null, false);
		        return cb(new Error(`No file format allowed!`));
			}
			enb_file_ext_arr = enb_file_ext.split(",")
			const all_ext = await getFileMime(enb_file_ext_arr)
		    if (all_ext.includes(file.mimetype)) {
		      cb(null, true);
		    } else {
		      cb(null, false);
		      return cb(new Error(`Only ${enb_file_ext.toString()} format allowed!`));
		    }
		  }

		const storage = multer.diskStorage({
		    destination: function(req, file, cb) {
		        cb(null, full_path_folder);
		    },

		    // By default, multer removes file extensions so let's add them back
		    filename: function(req, file, cb) {
		        cb(null, randomstring.generate() + path.extname(file.originalname));
		    }
		});

		let upload = multer({ storage: storage, fileFilter: fileExtValid, limits: { fileSize: (req.partner_cfg.MAX_SIZE*1000000) }}).array('files', 10);
		
		upload(req, res, async function(err) {


	        // req.file contains information of uploaded file
	        // req.body contains information of text fields, if there were any
	        if (req.fileValidationError) {
	            return res.send(req.fileValidationError);
	        }
	        else if (!req.files) {
	        		Logging.push(request_id, "warning", "request", {error:"Thieu file upload"}, `Upload file: Thieu file upload`)
	            return res.send(util.sendError(400, "FILE_INVALID", 'Please select an image to upload'))
	        }
	        else if (err instanceof multer.MulterError) {
	            return res.send(util.sendError(400, null, err))
	        }
	        else if (err) {
	        	  Logging.push(request_id, "warning", "request", {error: err.toString()}, `Upload file: FILE_INVALID / Error upfile`)
	            return res.send(util.sendError(400, "FILE_INVALID", err.toString()))
	        }
	        const files = req.files;
	        let out_array = []
	        let permission = 0;
	        if(req.query.permission){
	        	permission = req.query.permission
	        }

	        const arr_id = req.query.id?req.query.id.split("/"):[]

	        const conf = {
	        	bucket: req.partner_cfg.BUCKET.toLowerCase(), 
	        	pathroot: req.partner_cfg.PATH_ROOT.toLowerCase(), 
	        	parent_code: req.ParentCode,
	        	actioncode: req.header('ActionCode'),
	        	tags: req.query.tags,
	        	subpath: subpath,
	        	permission:permission
	        }
	        let index = 0
	        for (const item of files) {
				    const obj_file = await processItem(item, index, req.query.id, arr_id, conf);
				    out_array.push(obj_file)
				    index ++;
				  }
				  
        	Logging.push(request_id, "info", "request", {header: JSON.stringify(req.headers), response: out_array}, `Upload file: ${out_array.length} files`)
        	res.send(util.sendSuccess(out_array))
	    });
	}catch(e){
		console.log(e)
		return res.send(util.sendError())
	}
}

function processItem(item, index, query_id, arr_id, conf){
	return new Promise(async (resolve, reject) => {
    
    let fk = null
		const outobj = {
			file_key: null,
			file_org_name: item.originalname,
			success:false
		}

		if(query_id){
			if(arr_id[index]){
				fk = arr_id[index]
				//check file key is exist in db
				const params = {p_file_key: fk, cur_1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }}
	    	const get_file_info = await oracleDB.execute(initQuery.FILE_GET_FROM_DB, params, config.oracledb.cmedia)
	    if(get_file_info[0][0]){
	      	const full_file_path = get_file_info[0][0].ROOT_FOLDER + get_file_info[0][0].FILE_BUCKETS + get_file_info[0][0].FILE_NAME;
	      	const old_file_stored_path = get_file_info[0][0].ROOT_FOLDER + get_file_info[0][0].FILE_BUCKETS + ".stored_"+fk
	     	if (!fs.existsSync(old_file_stored_path)){
			    fs.mkdirSync(old_file_stored_path);
				}
			//move old file to stored folder
				const des_file = old_file_stored_path+"/"+get_file_info[0][0].FILE_NAME;
			//Del cache file info in redis
				//const client = redisDb.getClient()
				//const rd_file_key = `${config.app.prefix}file_${fk}`
				//await client.del(rd_file_key);

				//client.quit()
				const moveresult = await moveFile(full_file_path, des_file)
				if(moveresult){
					const update_result = await updateNewFile(fk, item.filename)
	          if(update_result){
	            	outobj.success = true
	            	outobj.file_key = fk
								// out_array.push(outobj)
						    return resolve(outobj);
				    }else{
	          	console.log("Update file failed")
	          	Logging.push(request_id, "error", "system", {message:"Can't update new file path database",outobj:outobj}, `File update is error!`)

				    }
	      }else{
	        	console.log("move file is error")
	        	Logging.push(request_id, "error", "system", {message:"Can't move file to backup folder",outobj:outobj}, `File move to backup is error!`)
	        	outobj.file_key = fk
	        	outobj.success = false
						return resolve(outobj);
	      }
	    }else{
	    	outobj.file_key = fk
	    }
			}else{
				fk = `${uid(32)}.${mime.extension(item.mimetype)}`
				outobj.file_key = fk
			}
		}else{
			fk = `${uid(32)}.${mime.extension(item.mimetype)}`
			outobj.file_key = fk
		}

		// bucket: req.partner_cfg.BUCKET.toLowerCase(), 
	  // 	pathroot: req.partner_cfg.PATH_ROOT.toLowerCase(), 
	  // 	parent_code: req.ParentCode,
	  // 	actioncode: req.header('ActionCode'),
	  // 	tags: req.query.tags

		const insert_result = await insertFileInfo(fk, item.originalname, item.filename, conf.bucket, conf.pathroot,  conf.parent_code, conf.parent_code, conf.actioncode, conf.permission, conf.subpath, conf.tags)
		if(!insert_result){
			fs.unlink(item.path, (err) => {
		 	if (err) {
		     console.error("UPLOAD ERROR ",err)
			   Logging.push(request_id, "error", "system", {message:"Can't delete error file", source_at:"File.processItem"}, `File remove error!`)
			  }
			})
			outobj.success = false
			return resolve(outobj);
		}else{
			outobj.success = true
			return resolve(outobj);
		}

    //END PROMISE
  });

	



}

function gFefault(req, res){
	return res.send(util.sendError('ACCESS DENIED'))
}

function isValidExistingDirectory(path) {
     if (path == null || path.trim() == '') return false;
     const reg_str = '^/|(/[a-zA-Z0-9_-]+)+$'
     const regex = new RegExp(reg_str);
     return regex.test(path);
}

function insertFileInfo(fkey, original_name, file_name, file_bucket, file_root, create_by, modified_by, action_code, permission, subfolder=null, tags=null){
	return new Promise(async (resolved, rejected)=>{
		try{
			const params = {
				p_file_key: fkey, 
				p_file_original_name: original_name,
				p_file_name: file_name,
				p_file_bucket: file_bucket,
				p_root_folder: file_root,
				p_create_by: create_by || "system",
				p_modified_by: modified_by || "system",
				p_action_code: action_code,
				p_permission: permission || 0,
				p_sub_folder: subfolder,
				p_tags: tags
			}
			await oracleDB.execute(initQuery.INSERT_FILE_DB, params, config.oracledb.cmedia)
			return resolved(true)
		}catch(e){
			console.log(e)
			return resolved(false)
		}
	})
}

function updateNewFile(fkey, file_name){
	return new Promise(async (resolved, rejected)=>{
		try{
			const params = {
				p_file_key: fkey, 
				p_file_name: file_name
			}
			const insert_result = await oracleDB.execute(initQuery.FILE_UPDATE_NEW, params, config.oracledb.cmedia)
			return resolved(true)
		}catch(e){
			console.log("updateNewFile > ",e)
			return resolved(false)
		}
	})
}


function getFileMime(arr_file_ext){
	return new Promise((resolved, rejected)=>{
		var arr = []
		forEach(arr_file_ext, function(item, index){
			var done = this.async()
			if(config.fileExt["."+item.toLowerCase()]){
				arr.push(config.fileExt["."+item.toLowerCase()])
			}
			done()
		}, function(){
			return resolved(arr);
		});
	})
}

function moveFile(oldPath, newPath){
	return new Promise((resolved, rejected)=>{
		if (fs.existsSync(oldPath)) {
			 fs.rename(oldPath, newPath, function (err) {
				if (err) {
					if (err.code === 'EXDEV') {
						copy();
					} else {
						console.log("err moveFile", err)
					  return resolved(false)
					}
					return;
				}
				return resolved(true)
			});
		}else{
			return resolved(true)
		}
		

	    function copy() {
	        var readStream = fs.createReadStream(oldPath);
	        var writeStream = fs.createWriteStream(newPath);

	        readStream.on('error', callback);
	        writeStream.on('error', callback);

	        readStream.on('close', function () {
	            fs.unlink(oldPath, callback);
	        });

	        readStream.pipe(writeStream);
	    }
	})
   
}

module.exports = {
  initialization,
  getFile,
  removeFile,
  gFefault,
  moveFile,
  updateNewFile,
  insertFileInfo,
  upFile:upFile
}
