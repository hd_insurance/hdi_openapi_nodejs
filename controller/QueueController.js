const oracledb = require('oracledb');
const oracleDB = require('../Services/Connector/OracleDB.js');
const constant = require('../common/constant');
const initQuery = require('../config/InitQuery.js');
const config = require('../config');
const init = require('../Services/Init.js');
const urlparser = require('urlparser');
const RedisConnector = require('../Services/Connector/Redis.js');
const util = require('../common/util');
const { Kafka } = require('kafkajs')


const kafka = new Kafka({
  clientId: 'openapi-localapp',
  brokers: [config.kafka.kafkaHost]
})

const publish = async (req, res, next)=>{
	try{
		const action_code = req.body.Action.ActionCode
		const params = {
		 	p_actioncode: "TEST_PUB_QUEUE",
		 	p_out1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }
		 }
		 const get_queue_producing_info = await oracleDB.execute(initQuery.GET_QUEUE_API, params, config.oracledb.master)
		 if(get_queue_producing_info[0][0]){
		 	const procedure = get_queue_producing_info[0][0]
		 	let list_topic = []
		 	get_queue_producing_info[0].forEach((item, index)=>{
		 		  let topic = {
				    topic: item.TOPIC_CODE,
				    messages: req.body.Data,
				  }
				  list_topic.push(topic)
		 	})

		 	const producer = kafka.producer()
			await producer.connect()
		 	let object_send = {
		 		topicMessages: list_topic
		 	}
		 	if(procedure.ACKS){
		 		object_send.acks = procedure.ACKS * 1
		 	}
		 	if(procedure.TIME_OUT){
		 		object_send.timeout = procedure.TIME_OUT * 1
		 	}
		 	const sendResult = await producer.sendBatch(object_send)
		 	await producer.disconnect()
		 	const result_response = {
		 		result: sendResult,
		 		topic_data: list_topic
		 	}
		 	return res.send(util.sendSuccess(result_response, null))
		 }else{
		 	return res.send(util.error(constant.errorcode.BAD_REQUEST, "Can't find this queue config to publish", null, req.id))
		 }
	}catch(e){
		console.log("PUB QUEUE ERROR ", e)
		res.send(util.error(constant.errorcode.BAD_REQUEST, e, null, req.id))
	}
}
const getQueueAPICode = async (req, res, next)=>{
	try{
		 const producer = kafka.producer()
		 await producer.connect()

		 const message = {
		 		key: "key_a",
		 		value: "hoang hello"
		 }

		 const params = {
		 	p_actioncode: "TEST_PUB_QUEUE",
		 	p_out1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }
		 }
		 const get_queue_producing_info = await oracleDB.execute(initQuery.GET_QUEUE_API, params, config.oracledb.master)
		 if(get_queue_producing_info[0][0]){
		 	const procedure = get_queue_producing_info[0][0]

		 	get_queue_producing_info[0].forEach((item, index)=>{
		 			console.log(item)
		 	})




		 }else{
		 	return res.send({error: true})
		 }
		 return res.send({error: true})
	}catch(e){
		console.log(e)
	}
	res.send({a:1})
}






module.exports = {
  getQueueAPICode,
  publish
}
