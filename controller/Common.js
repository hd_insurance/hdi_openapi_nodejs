const config = require("../config");
const constant = require("../common/constant");
const RedisConnector = require("../Services/Connector/Redis.js");
const Logging = require("../Services/Logging");
const util = require("../common/util");
const orcdb = require("../Services/Connector/OracleDB.js");
const initQuery = require("../config/InitQuery.js");
var randomstring = require("randomstring");
const axios = require("axios");
var forEach = require("async-foreach").forEach;

const oracledb = require("oracledb");

const getLanguage = async (req, res) => {
  try {
    const p_page = req.params.page.toString().toUpperCase();
    var version = "1";
    if (req.query.version) {
      version = req.query.version;
    }
    const client = RedisConnector.getClient();
    const cache_key = `LNG_${p_page}_${version}`;
    const data = await client.get(cache_key);
    if (data) {
      res.send({ success: true, data: JSON.parse(data) });
    } else {
      const dbset = await client.keys(`LNG_${p_page}_*`);
      if (dbset.length > 0) {
        await client.del(dbset);
      }
      const all_database = await orcdb.execute(
        initQuery.GETLANGS,
        { p_page: p_page, cur_1: constant.CUSOR },
        config.oracledb.cmedia
      );
      let object_lang_out = {};
      if (all_database[0]) {
        forEach(
          all_database[0],
          function (item, index) {
            const keylang = item.TRANS_KEY;
            object_lang_out[keylang] = [
              item.CONTENT_VI,
              item.CONTENT_EN ? item.CONTENT_EN : item.CONTENT_VI,
            ];
          },
          async function () {
            await client.set(cache_key, JSON.stringify(object_lang_out));
            res.send({ success: true, data: object_lang_out });
          }
        );
      } else {
        res.send({ success: false, data: null });
      }
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: null });
  }
};

const addLanguage = async (req, res) => {
  const p_page = req.params.page.toString().toUpperCase();
  var p_key = null;
  if (req.body.key) {
    p_key = req.body.key.toString();
  } else {
    p_key = randomstring.generate(8).toLowerCase();
  }
  const p_content_vi = req.body.p_content_vi.toString();
  const p_content_en = req.body.p_content_en
    ? req.body.p_content_en.toString()
    : null;

  try {
    const all_database = await orcdb.execute(
      initQuery.ADDLANGS,
      {
        p_page: p_page,
        p_key: p_key,
        p_content_vi: p_content_vi,
        p_content_en: p_content_en,
      },
      config.oracledb.cmedia
    );
    res.send({
      success: true,
      key: p_key,
      p_content_vi: p_content_vi,
      p_content_en: p_content_en,
    });
  } catch (e) {
    console.log(e);
    res.send({ success: false, key: null, content: null });
  }
};

const getPageFAQ = async (req, res) => {
  try {
    var version = "1";
    var cache = true;
    if (req.query.version) {
      version = req.query.version;
    }
    if (req.query.cache) {
      cache = false;
    }
    const p_faq = req.params.faqid;

    const client = RedisConnector.getClient();
    const cache_key = `PFAQ_${p_faq}_${version}`;
    const data = await client.get(cache_key);

    if (data && cache) {
      res.send(JSON.parse(data));
    } else {
      const dbset = await client.keys(`PFAQ_${p_faq}_*`);
      if (dbset.length > 0) {
        await client.del(dbset);
      }

      const all_database = await orcdb.execute(
        initQuery.GETFAQ,
        { p_faq_id: p_faq, cur_1: constant.CUSOR },
        config.oracledb.cmedia
      );
      let object_lang_out = {};
      var faq_out = [];

      if (all_database[0]) {
        var faq_parent = all_database[0].filter((item) => {
          return item.PARENT_ID == null;
        });

        forEach(
          faq_parent,
          function (item, index) {
            item.content = all_database[0].filter((childitem) => {
              return childitem.PARENT_ID == item.FAQ_CONTENT_ID;
            });

            faq_out.push(item);
          },
          async function () {
            await client.set(cache_key, JSON.stringify(faq_out));
            res.send(faq_out);
          }
        );
      } else {
        res.send({ success: false, data: null });
      }
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: null });
  }
};

const addFAQContent = async (req, res) => {
  const data = req.body;
  try {
    const all_database = await orcdb.execute(
      initQuery.ADDFAQCONTENT,
      {
        p_web_faq_id: data.p_web_faq_id,
        p_icon: data.p_icon,
        p_title: data.p_title,
        p_content: data.p_content,
        p_sortorder: data.p_sortorder,
        p_parent_id: data.p_parent_id,
      },
      config.oracledb.cmedia
    );
    res.send({ success: true });
  } catch (e) {
    console.log(e);
    res.send({ success: false });
  }
};

const editFAQContent = async (req, res) => {
  const data = req.body;

  try {
    const all_database = await orcdb.execute(
      initQuery.EDITFAQCONTENT,
      {
        p_faqcontent_id: data.p_faqcontent_id,
        p_icon: data.p_icon,
        p_title: data.p_title,
        p_content: data.p_content,
        p_sortorder: data.p_sortorder,
      },
      config.oracledb.cmedia
    );
    res.send({ success: true });
  } catch (e) {
    console.log(e);
    res.send({ success: false, key: null, content: null });
  }
};

const deleteFAQContent = async (req, res) => {
  const data = req.body;
  try {
    const all_database = await orcdb.execute(
      initQuery.DELETEFAQCONTENT,
      { p_faqcontent_id: data.p_faqcontent_id },
      config.oracledb.cmedia
    );
    res.send({ success: true });
  } catch (e) {
    console.log(e);
    res.send({ success: false, key: null, content: null });
  }
};

const getPageInstances = async (req, res) => {
  try {
    const all_database = await orcdb.execute(
      initQuery.GETALLPAGEINSTANCE,
      { cur_1: constant.CUSOR },
      config.oracledb.cmedia
    );
    if (all_database[0]) {
      res.send({ success: false, data: all_database[0] });
    } else {
      res.send({ success: false, data: null });
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: null });
  }
};

const getPageConfig = async (req, res) => {
  try {
    var version = "1";
    if (req.query.version) {
      version = req.query.version;
    }
    const p_wiid = req.params.wi_id.toString().toUpperCase();
    const client = RedisConnector.getClient();
    const cache_key = `PCFG_${p_wiid}_${version}`;
    const data = await client.get(cache_key);

    if (data) {
      res.send({ success: true, data: JSON.parse(data) });
    } else {
      const dbset = await client.keys(`PCFG_${p_wiid}_*`);
      if (dbset.length > 0) {
        await client.del(dbset);
      }
      const all_database = await orcdb.execute(
        initQuery.GETALLWEBPAGEINSTANCE,
        { p_wiid: p_wiid, cur_1: constant.CUSOR, cur_2: constant.CUSOR },
        config.oracledb.cmedia
      );
      const obj_out = {
        webconfig: all_database[1][0],
        pages: all_database[0],
      };
      await client.set(cache_key, JSON.stringify(obj_out));
      res.send({ success: true, data: obj_out });
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: null });
  }
};

const testORC = async (req, res) => {
  try {
    const connection = await oracledb.getConnection({
      user: "B_CONTRACTS",
      password: "Contracts@#123!",
      connectString:
        "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=103.237.145.141)(PORT=1521))(CONNECT_DATA=(SERVER = DEDICATED)(SERVICE_NAME = ORADEV)))",
    });
    const query =
      "BEGIN pkg_insur_info.SEARCH_INFO_INSUR(:P_PROJECT,:P_USERNAME,:P_ORG_CODE,:P_PRODCODE,:P_O_BUSINESS,:CUR_1,:CUR_2,:CUR_3,:CUR_4,:CUR_5,:CUR_6); if(:CUR_1 IS NULL) then OPEN :CUR_1 FOR SELECT * FROM dual WHERE 1 = 0; end if; if(:CUR_2 IS NULL) then OPEN :CUR_2 FOR SELECT * FROM dual WHERE 1 = 0; end if; if(:CUR_3 IS NULL) then OPEN :CUR_3 FOR SELECT * FROM dual WHERE 1 = 0; end if; if(:CUR_4 IS NULL) then OPEN :CUR_4 FOR SELECT * FROM dual WHERE 1 = 0; end if; if(:CUR_5 IS NULL) then OPEN :CUR_5 FOR SELECT * FROM dual WHERE 1 = 0; end if; if(:CUR_6 IS NULL) then OPEN :CUR_6 FOR SELECT * FROM dual WHERE 1 = 0; end if; END;";
    const in_params = {
      P_PROJECT: "",
      P_USERNAME: "",
      P_ORG_CODE: "VIETJET_VN",
      P_PRODCODE: "BAY_AT",
      P_O_BUSINESS: `{"a12":"Khách hàng 3"}`,
      CUR_1: { type: 2021, dir: 3003 },
      CUR_2: { type: 2021, dir: 3003 },
      CUR_3: { type: 2021, dir: 3003 },
      CUR_4: { type: 2021, dir: 3003 },
      CUR_5: { type: 2021, dir: 3003 },
      CUR_6: { type: 2021, dir: 3003 },
    };
    const result = await connection.execute(query, in_params);
    console.log(result);
    res.send(200);
  } catch (e) {
    res.send("LOI");
  }
};

module.exports = {
  getPageInstances,
  getPageConfig,
  getLanguage,
  addLanguage,
  getPageFAQ,
  addFAQContent,
  editFAQContent,
  deleteFAQContent,
};
