const oracledb = require('oracledb');
const oracleDB = require('../Services/Connector/OracleDB.js');
const initQuery = require('../config/InitQuery.js');
const config = require('../config');
const init = require('../Services/Init.js');
const urlparser = require('urlparser');
const RedisConnector = require('../Services/Connector/Redis.js');
const Logging = require('../Services/Logging');


this.getPartnerConfig = (partner_id, environment)=>{
	return new Promise(async (resolved, rejected)=>{
		
		const client = RedisConnector.getClient()
		const dbset = await client.hgetall(`${config.app.prefix}${partner_id}_${environment.toUpperCase()}`); 
		if(dbset==null || dbset==false){
		  	const params = {ptncode: partner_id, cs1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }}
		  	const get_partner_db = await oracleDB.execute(initQuery.SYS_PARTNER_CONFIG_GET_FROM_DB, params, config.oracledb.master)
		  	if(get_partner_db[0].length > 0){
		  		await init.cachePartnerConfig(get_partner_db[0])
		  		const ptn_config = await module.exports.getPartnerConfig(partner_id, environment)
		  		return resolved(ptn_config)
		  	}else{
		  		return resolved(null)
		  	}
		  }
		  return resolved(dbset)

	})
}
this.getSystemDomain = (url)=>{
	return new Promise(async (resolved, rejected)=>{
		
		const client = RedisConnector.getClient()
		const u = urlparser.parse(url)
		const environment = await client.get(`${config.app.prefix}API_DOMAIN_${u.host.hostname}:${u.host.port}`);
		if(environment){
		  	 return resolved(environment)
		}else{
		  	const params = {cs1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }}
		  	const get_system_domain = await oracleDB.execute(initQuery.SYS_DOMAIN_GETALL, params, config.oracledb.master)
		  	await init.cacheDomain(get_system_domain[0])
		  	const environment_out = await module.exports.getSystemDomain(url)
		  	return resolved(environment_out)
		}

	})
}


this.getPartnerAPIAccess = (ptn_code, api_code)=>{
	return new Promise(async (resolved, rejected)=>{
		const client = RedisConnector.getClient()
		const environment = await client.get(`${config.app.prefix}API_GR_${ptn_code}_${api_code}`);
		if(environment == null){
			 Logging.push(null, "error", "request", {error: "GET redis is return null"}, `GetPartnerAPIAccess ERRORl`)
		}
		return resolved(environment) 
	})
}






module.exports = {
  getSystemDomain: this.getSystemDomain,
  getPartnerConfig: this.getPartnerConfig,
  getPartnerAPIAccess: this.getPartnerAPIAccess
}
