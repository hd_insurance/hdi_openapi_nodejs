const config = require('../config');
const constant = require('../common/constant');
const redisDb = require('../Services/Connector/Redis.js');
const oracledb = require('oracledb');
const util = require('../common/util');
const initQuery = require('../config/InitQuery.js');
const _ = require('lodash')
const oracleDB = require('../Services/Connector/OracleDB.js');
const Logging = require('../Services/Logging');


function checkClientKey(req, res, next) {
  //logic here
  return next();
}

function isValidToken(token, tokenType) {
  //code logic here
  return true
}

async function checkAuthenticationView(req, res, next) {
  try{  
    const file_info = await getFileInfo(req.params.file_key)
    if(file_info==null){
      console.log("FILE WITH KEY: ", req.params.file_key, "not found in server")
      Logging.push(req.params.file_key, "warning", "request", {file_key: req.params.file_key}, `Get file - not exists / Error Get file`)
      return res.send(util.sendError(404, 'FILE_NOT_FOUND', 'FILE NOT FOUND'))
    }


    if(file_info.PERMISSION!=0){
      const isValid = await checkTokenExisted(req, file_info.PERMISSION)
      if(!isValid){
        return res.send(util.sendError(403, constant.errorcode.UNAUTHORIZED, 'Authentication Error'))
      }
      const partner_cfg = await getPartnerConfigInfo(req.ParentCode, req.ActionCode, req.environment, config.file_option.VIEW)

    }
    req.file_info = file_info
    return next();
  }catch(e){
    console.log(e)
    return res.send(util.sendError())
  }
}

async function checkAuthenticationUpload(req, res, next){
  try{
    if(validHeaderParams(req)){
      const token = req.UserName.toUpperCase()+req.Secret.toUpperCase()+req.DeviceEnvironment.toUpperCase()
      const partner_api_cfg = await getPartnerAPIConfigInfo(req.ParentCode.toUpperCase(), req.environment.toUpperCase())
      if(partner_api_cfg==null){
        console.log("ODAY1111")
        Logging.push(null, "error", "request", {error: "Upload file: UNAUTHORIZED | Get redis is null", token:token}, `Upload file: UNAUTHORIZED -  Get Partner config is null`)
        return res.send(util.sendError(403, constant.errorcode.UNAUTHORIZED, constant.errorcode.UNAUTHORIZED))
      }
      if(partner_api_cfg.ISTOKEN == '0'){
          const partner_cfg = await getPartnerConfigInfo(req.ParentCode, req.ActionCode, req.environment, config.file_option.UPLOAD)
          if(partner_cfg){
            req.partner_cfg = partner_cfg
            return next();
          }else{
            Logging.push(null, "error", "request", {error: "Upload file: UNAUTHORIZED | Get redis is null"}, `Upload file: UNAUTHORIZED -  Pertner not found`)
            console.log("ODAY112221 ", partner_cfg)
            return res.send(util.sendError(403, constant.errorcode.UNAUTHORIZED, constant.errorcode.UNAUTHORIZED))
          }
      }else{
        const tkvl = await checkTokenValid(token)
        if(tkvl!=null){
          const partner_cfg = await getPartnerConfigInfo(req.ParentCode, req.ActionCode, req.environment, config.file_option.UPLOAD)
          if(partner_cfg){
            req.partner_cfg = partner_cfg
            return next();
          }else{
            console.log("ODAY133311")
            return res.send(util.sendError(403, constant.errorcode.UNAUTHORIZED,constant.errorcode.UNAUTHORIZED))
          }
        }else{
          console.log("ODAY14222224411")
          Logging.push(null, "error", "request", {error: "Upload file: UNAUTHORIZED | Get redis is null"}, `Upload file: UNAUTHORIZED -  Get redis is null`)
          return res.send(util.sendError(403, constant.errorcode.UNAUTHORIZED,constant.errorcode.UNAUTHORIZED))
        }
      }
    }else{
      console.log("ODAY144411")
      Logging.push(null, "error", "request", {error: "Upload file: ERROR | checkAuthenticationUpload", err_message: "Invalid header parameters"}, `Upload file: ERROR | checkAuthenticationUpload`)
      return res.send(util.sendError(403, constant.errorcode.UNAUTHORIZED,constant.errorcode.UNAUTHORIZED))
    }
    
  }catch(e){
    console.log(e)
    console.log("ODAY555111")
    Logging.push(null, "error", "request", {error: "Upload file: ERROR | checkAuthenticationUpload", err_message: e.toString()}, `Upload file: ERROR | checkAuthenticationUpload`)
    return res.send(util.sendError(403, constant.errorcode.UNAUTHORIZED,constant.errorcode.UNAUTHORIZED))
  }
}

function validHeaderParams(req){
    if(req.header('environment')
       && req.header('DeviceEnvironment')
       && req.header('ParentCode')
       && req.header('UserName')
       && req.header('Secret')
       && req.header('ActionCode')){
        req.environment = req.header('environment')
        req.DeviceEnvironment = req.header('DeviceEnvironment')
        req.ParentCode = req.header('ParentCode')
        req.Secret = req.header('Secret')
        req.UserName = req.header('UserName')
        req.ActionCode = req.header('ActionCode')
        return true
    }else{
      return false
    }
}
function checkTokenExisted(req, permission){
  return new Promise(async (resolved, rejected)=>{
    if(req.header('token')){
      const tkvl = await checkTokenValid(req.header('token'))


      return resolved(tkvl!=null)
    }else if(req.query.token){
      const tkvl = await checkTokenValid(req.query.token)
      return resolved(tkvl!=null)
    }else{
      return resolved(false);
    }
  })
}
function checkTokenValid(token_key){
  return new Promise(async (resolved, rejected)=>{
    const client = redisDb.getClient()
    const values = await client.get(token_key);
     // console.log("token_key >> ", token_key, values)
    return resolved(values)
  })
}
function getRedisOrgCfg(cfg_key){
  return new Promise(async (resolved, rejected)=>{
    const client = redisDb.getClient()
    const values = await client.get(cfg_key);
    return resolved(values)
  })
}

function getRedisHm(_key, hm){
  return new Promise(async (resolved, rejected)=>{
    const client = redisDb.getClient()
    const values = await client.hmget(_key, hm);
    return resolved(values)
  })
}

function getRedisFile(file_key){
  return new Promise(async (resolved, rejected)=>{
    const client = redisDb.getClient()
    const values = await client.get(file_key);
    return resolved(values)
  })
}

function getFileInfo(fkey){
  return new Promise(async (resolved, rejected)=>{
    try{
      const client = redisDb.getClient()
      const rd_file_key = `${config.app.prefix}file_${fkey}`
      const redisfile = await getRedisFile(rd_file_key)
      if(redisfile){
        return resolved(JSON.parse(redisfile))
      }
      const params = {p_file_key: fkey, cur_1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }}
      const get_file_info = await oracleDB.execute(initQuery.FILE_GET_FROM_DB, params, config.oracledb.cmedia)
      if(get_file_info[0][0]){
         await client.set(rd_file_key, JSON.stringify(get_file_info[0][0]), 'EX', config.redis.ttl * 60 * 60 * 24) 
         return resolved(get_file_info[0][0])
      }else{
        Logging.push(fkey, "error", "request", {error: "GET file: NULL | Get redis & db is null", file_key: fkey}, `File not found -  Get redis is null`)
        return resolved(null)
      }
      return resolved(get_file_info[0][0])
    }catch(e){
      Logging.push(fkey, "error", "system", {error: "GET file: NULL | Get redis & db is null", error_message: e.toString(), file_key: fkey}, `File is error ${fkey}`)
      return resolved(null)
    }
  })
}

async function getPartnerConfigInfo(org_code, api_code, environment, action){
  return new Promise(async (resolved, rejected)=>{
    try{
      const client = redisDb.getClient()
      const redis_key = config.app.prefix+org_code+api_code+environment+action

      
      // console.log("REDIS_K ", redis_key)

      const org_redis_cfg = await getRedisOrgCfg(redis_key)

       // console.log("REDIS_org_redis_cfg ", org_redis_cfg)
      if(org_redis_cfg==null){
         Logging.push(null, "error", "request", {error: "Upload file: UNAUTHORIZED | Get Partner is null"}, `Upload file: UNAUTHORIZED | Get redis is null`)
        const params = {p_org_code: org_code, p_action_code:api_code, p_option: action, p_env:environment.toUpperCase(), cur_1:{ type: oracledb.CURSOR, dir: oracledb.BIND_OUT }}
        const get_org_config = await oracleDB.execute(initQuery.FILE_GET_CONFIG_ORG, params, config.oracledb.cmedia)

        if(get_org_config[0][0]){
          
          await client.set(redis_key, JSON.stringify(get_org_config[0][0]), 'EX', config.redis.ttl * 60 * 60 * 24) 
          return resolved(get_org_config[0][0])
        }else{
          Logging.push(null, "error", "request", {error: "Upload file: UNAUTHORIZED | Get Partner is null"}, `Upload file: UNAUTHORIZED | Get Partner is null`)
          return resolved(null)
        }

      }else{
        return resolved(JSON.parse(org_redis_cfg))
      }
      
    }catch(e){
      console.log(e)
      Logging.push(null, "error", "system", {error: e.toString()}, `Upload file: UNAUTHORIZED | getPartnerConfigInfo`)
      return resolved(null)
    }
  })
}

async function getPartnerAPIConfigInfo(org_code, environment){
  return new Promise(async (resolved, rejected)=>{
    try{
      const redis_key = config.app.prefix+org_code+"_"+environment
      const org_redis_cfg = await getRedisHm(redis_key, ["ISTOKEN", "ISWHITELIST"])
      if(org_redis_cfg!=null){
        var parnet_cfg = {ISTOKEN: org_redis_cfg[0]||"1", ISWHITELIST: org_redis_cfg[1]||"1"}
        return resolved(parnet_cfg)
      }else{
        return resolved(null)
      }
    }catch(e){
      console.log(e)
      return resolved(null)
    }
  })
}


function cacheMiddleware(){
	//return cache(`${config.api_cache.time} minutes`)
}

module.exports = {
  checkClientKey,
  isValidToken,
  checkAuthenticationView,
  checkAuthenticationUpload,
  cacheMiddleware,
  getPartnerConfigInfo,
  getFileInfo
}