const config = require('../config');
const constant = require('../common/constant');
const path = require('path');
const fs = require('fs')
const File = require('./File.js');
const forEach = require('async-foreach').forEach;
const { rootPath } = require('get-root-path')
const RedisConnector = require('../Services/Connector/Redis.js');
const SocketController = require("./SocketController");
const Logging = require('../Services/Logging');
const findRemoveSync = require('../Services/findnremove');
const FileAuth = require('./FileAuthentication.js');
const ThumbnailGenerator = require('video-thumbnail-generator').default;
var static = require('node-static');
const util = require('../common/util');
const axios = require('axios');
var QRCode = require('qrcode')
var randomstring = require("randomstring");
var base64ToImage = require('base64-to-image');

const multer = require('multer');


const createQR = async (req, res)=>{
      try {


        const partner_file_config = await FileAuth.getPartnerConfigInfo(req.partner_config.PARTNER_CODE, req.body.Action.ActionCode, req.environment, "STREAM")
        if(!partner_file_config){
          return res.send(util.sendError())
        }
        const fpath = partner_file_config.PATH_ROOT + partner_file_config.BUCKET
        const id_transshipment = randomstring.generate(9).toLowerCase()

        const temp_cache_folder = rootPath+"/public/media_cache/"+id_transshipment
        if (!fs.existsSync(temp_cache_folder)){
            fs.mkdirSync(temp_cache_folder);
           
        }

        const layout = req.body.Data.fileconfig
        const client = RedisConnector.getClient()
          
        const auth = {
          au1: req.body.Device.environment,
          au2: req.body.Device.DeviceEnvironment,
          au3: req.body.Action.ParentCode,
          au4: req.body.Action.Secret,
          au5: req.body.Action.UserName,
          au6: req.body.Action.ActionCode

        }

        const data_default = {
          id: id_transshipment
        }


        const redis_obj = {
          fp: fpath,
          fr: partner_file_config.PATH_ROOT,
          bk: partner_file_config.BUCKET,
          t: partner_file_config.MAX_TIME,
          s: partner_file_config.MAX_SIZE,
          oc: req.partner_config.PARTNER_CODE,
          trsm: data_default,
          layout: layout,
          auth: auth
        }


        const rd_file_key = `TRANSSHIPMENT_${id_transshipment}`
        await client.set(rd_file_key, JSON.stringify(redis_obj), 'EX', 100 * 60);
        // const url_transshipment = `https://media.hdinsurance.com.vn/media-transshipment/${id_transshipment}`
          res.send({
            transshipmentId: id_transshipment
          })

    } catch (error) {
      console.error("CREATE QR ",error);
      res.sendStatus(500);
    }
}

const checkTransshipmentInfo = async (req, res)=>{
  try{
      const rd_file_key = `TRANSSHIPMENT_${req.params.trans_id}`
      const client = RedisConnector.getClient()
      const data_sample = await client.get(rd_file_key);
      if(data_sample){
        const info = JSON.parse(data_sample)
        res.send(info);
      }else{
        res.sendStatus(500);
      }
  }catch(e){
    console.log(e)
    res.sendStatus(500);
  }
}


const getThumbVideo = async (req, res)=>{
  try{
    const videofileid = req.params.videofileid
    const file_info = await FileAuth.getFileInfo(videofileid)
    
    if(file_info){
        const file_bucket = file_info.ROOT_FOLDER+file_info.FILE_BUCKETS
        const full_file_path = file_bucket+file_info.FILE_NAME
        const thumbp = file_bucket+'vid_temp_thumbvideo-'+videofileid+".png"


        var fileServer = new static.Server(file_bucket, { cache: 600 });

        if (fs.existsSync(thumbp)) {
          const gifname = 'vid_temp_thumbvideo-'+videofileid+".png"
          fileServer.serveFile(gifname, 200, {}, req, res);
        }else{
          const tg = new ThumbnailGenerator({
            sourcePath: full_file_path,
            thumbnailPath: file_bucket.slice(0, -1),
            tmpDir: file_bucket.slice(0, -1)
          });
          tg.generate({
            filename: 'vid_temp_thumbvideo-'+videofileid,
            size: '300x200',
            timemarks: [ '00:00:01.000' ]
          }).then(function(thumb_path){
              fileServer.serveFile(thumb_path, 200, {}, req, res);
          });
        }
    }


  }catch(e){
    console.log(e)
    res.sendStatus(500);
  }
}


const getThumbCacheVideo = async (req, res)=>{
  try{
    const videofileid = req.params.videofileid
    const videotransid = req.params.trans_id


    const file_bucket = `${rootPath}/public/media_cache/${videotransid}`
    const thumbp = file_bucket+'/vid_temp_thumbvideo-'+videofileid+".png"
    const full_file_path = file_bucket+"/video-"+videofileid+".webm"


    var fileServer = new static.Server(file_bucket, { cache: 600 });
    if (fs.existsSync(thumbp)) {
      const gifname = 'vid_temp_thumbvideo-'+videofileid+".png"
      fileServer.serveFile(gifname, 200, {}, req, res);
    }else{
      const tg = new ThumbnailGenerator({
        sourcePath: full_file_path,
        thumbnailPath: file_bucket,
        tmpDir: file_bucket
      });
      const obtion = {
        filename: 'vid_temp_thumbvideo-'+videofileid,
        timemarks: [ '00:00:01.000' ],
        size: '300x200'
      }
      tg.generate(obtion).then(function(thumb_path){
          fileServer.serveFile(thumb_path[0], 200, {}, req, res);
      });
    }


  }catch(e){
    console.log(e)
    res.sendStatus(404);
  }
}

const getCacheVideo = async (req, res)=>{
  try{
    const videofileid = req.params.videofileid
    const videotransid = req.params.trans_id

    const file_bucket = `${rootPath}/public/media_cache/${videotransid}`
    const thumbp = file_bucket+'/'+videofileid+".mp4"
    var fileServer = new static.Server(file_bucket, { cache: 600 });

    if (fs.existsSync(thumbp)) {
      const vidname = videofileid+".mp4"
      fileServer.serveFile(vidname, 200, {}, req, res);
    }else{
      return res.sendStatus(404);
    }
  }catch(e){
    console.log(e)
    return res.sendStatus(500);
  }
}



const getThumbCachePhoto = (req, res)=>{
  try{
    const videofileid = req.params.videofileid
    const videotransid = req.params.trans_id

    const file_bucket = `${rootPath}/public/media_cache/${videotransid}`
    const thumbp = file_bucket+'/normal_cache_'+videofileid+".png"

    var fileServer = new static.Server(file_bucket, { cache: 600 });

    if (fs.existsSync(thumbp)) {
      const gifname = 'normal_cache_'+videofileid+".png"
      fileServer.serveFile(gifname, 200, {}, req, res);
    }else{
      res.sendStatus(500);
    }


  }catch(e){
    console.log(e)
    res.sendStatus(500);
  }
}



const uploadVideoBlob = async (req, res)=>{
    try{
      const fileId = randomstring.generate(32).toLowerCase()

      // const filename = `normal_cache_${filekey}`
      const client = RedisConnector.getClient()
      const rd_file_key = `TRANSSHIPMENT_${req.params.stream_token}`
      const media_id = req.params.media_id
      const data_sample = await client.get(rd_file_key);
      if(data_sample){
        const info = JSON.parse(data_sample)
        // var optionalObj = {'fileName': filename, 'type':'png'};
          const bucket_path =  `${rootPath}/public/media_cache/${req.params.stream_token}/`
          const storage = multer.diskStorage({
              destination: function(req, file, cb) {
                  cb(null, bucket_path);
              },
              // By default, multer removes file extensions so let's add them back
              filename: function(req, file, cb) {
                  cb(null, "video-"+fileId + ".webm");
              }
          });
        let upload = multer({ storage: storage }).single('video');
        upload(req, res, async function (err) {
          if (err instanceof multer.MulterError) {
            console.log(err)
            return res.sendStatus(500);
          } else if (err) {
            console.log(err)
            return res.sendStatus(500);
          }
          const full_file_path = bucket_path+"video-"+fileId+".webm"
          var media_object = {}
          if(info.media_object){
            media_object = info.media_object
          }else{
            media_object = getMediaObject(info.layout)
          }

          if(media_object[media_id]){
              media_object[media_id].file = fileId
              media_object[media_id].type = "video"
              media_object[media_id].format = "webm"
              info.media_object = media_object
              await client.set(rd_file_key, JSON.stringify(info));
              const tg = new ThumbnailGenerator({
                sourcePath: full_file_path,
                thumbnailPath: bucket_path,
                tmpDir: bucket_path
              });
              const obtion = {
                filename: 'vid_temp_thumbvideo-'+fileId,
                timemarks: [ '00:00:01.000' ],
                size: '300x200'
              }
              tg.generate(obtion).then(function(thumb_path){
                 return res.send({fileId: fileId})
              });

          }else{
            return res.send(util.sendError())
          }

       
        })
       
      }else{
        res.sendStatus(500);
      }
    }catch(e){
      console.log(e)
      res.sendStatus(500);
    }
}

const uploadBase64 = async (req, res)=>{
    try{
      const filekey = randomstring.generate(32).toLowerCase()
      const media_id = req.body.media_id

      const filename = `normal_cache_${filekey}`
      const client = RedisConnector.getClient()
      const rd_file_key = `TRANSSHIPMENT_${req.params.stream_token}`
      const data_sample = await client.get(rd_file_key);
      if(data_sample){
        const info = JSON.parse(data_sample)
        var optionalObj = {'fileName': filename, 'type':'png'};
        if(!req.body.data){
          res.sendStatus(500);
        }
        // console.log("Upload base 64 ",  `${rootPath}/publish/media_cache/${req.params.stream_token}/`)
        base64ToImage(req.body.data, `${rootPath}/public/media_cache/${req.params.stream_token}/` , optionalObj); 
        var media_object = {}

        if(info.media_object){
          media_object = info.media_object
        }else{
          media_object = getMediaObject(info.layout)
        }

          if(media_object[media_id]){
              media_object[media_id].file = filekey
              media_object[media_id].type = "photo"
              media_object[media_id].format = "png"
              info.media_object = media_object
              await client.set(rd_file_key, JSON.stringify(info));
              return res.send({
                file_name: filename,
                file_key: filekey
              });

          }else{
            return res.send(util.sendError())
          }
        // //fkey, original_name, file_name, file_bucket, file_root, create_by, modified_by, action_code, permission
        // const insert_result = await File.insertFileInfo(filekey, filename+".png", filename+".png", info.bk.toLowerCase(), info.fr.toLowerCase(), info.auth.au6, info.auth.au6, info.auth.au6, 0)
        // if(insert_result){
        //   res.send({
        //     file_name: filename,
        //     file_key: filekey
        //   });
        // }else{
        //   res.sendStatus(500);
        // }
        
      }else{
        res.sendStatus(500);
      }
    }catch(e){
      console.log(e)
      res.sendStatus(500);
    }
}

const uploadBase64Array = async (req, res)=>{
    try{
      const filekey = randomstring.generate(32).toLowerCase()
      const media_id = req.body.media_id

      const filename = `normal_cache_${filekey}`
      const client = RedisConnector.getClient()
      const rd_file_key = `TRANSSHIPMENT_${req.params.stream_token}`
      const data_sample = await client.get(rd_file_key);
      if(data_sample){
        const info = JSON.parse(data_sample)
        var optionalObj = {'fileName': filename, 'type':'png'};
        if(!req.body.data){
          res.sendStatus(500);
        }
        // console.log("Upload base 64 ",  `${rootPath}/publish/media_cache/${req.params.stream_token}/`)
        base64ToImage(req.body.data, `${rootPath}/public/media_cache/${req.params.stream_token}/` , optionalObj); 
        var media_object = {}

        if(info.media_object){
          media_object = info.media_object
        }else{
          media_object = getMediaObject(info.layout)
        }
          if(media_object.other[media_id]){
             
          }else{
            media_object.other[media_id] = {
              type: "photo",
              format: "png",
              file: null
            }
          }

          media_object.other[media_id].file = filekey
          media_object.other[media_id].type = "photo"
          media_object.other[media_id].format = "png"
          info.media_object = media_object
          await client.set(rd_file_key, JSON.stringify(info));
          return res.send({
            file_name: filename,
            file_key: filekey
          });
      }else{
        res.sendStatus(500);
      }
    }catch(e){
      console.log(e)
      res.sendStatus(500);
    }
}

const submitSocket = async (req, res)=>{
  try{
    const transId = req.params.transId
    const client = RedisConnector.getClient()
    const rd_file_key = `TSM_CONNECT_${transId}`
    const socketId = await client.get(rd_file_key);
    if(socketId){
      SocketController.emit(socketId, "mediadata", req.body.data)
    }
    res.sendStatus(200);
  }catch(e){
    console.log(e)
    res.sendStatus(500);
  }
}



const commitVideo = async (req, res)=>{
  try{
    const { media_id, trans_id } = req.params;
    //Lay ra thong tin va save lai
    const client = RedisConnector.getClient()
    const rd_file_key = `TRANSSHIPMENT_${trans_id}`

    const data_sample = await client.get(rd_file_key);
    const info = JSON.parse(data_sample)

    var media_object = {}
    if(info.media_object){
      media_object = info.media_object
    }else{
      media_object = getMediaObject(info.layout)
    }
    const file_id = req.body.file_id
    if(media_object[media_id]){
        media_object[media_id].file = file_id
        info.media_object = media_object
        await client.set(rd_file_key, JSON.stringify(info));
      return res.send(util.sendSuccess({commit: true}))
    }else{
      return res.send(util.sendError())
    }
    
  }catch(error){
    console.log(error)
    return res.send(util.sendError())
  }
}

const commitMedia = async (req, res)=>{
  try{
    if(req.body.Data.transhipmentId){
      const trans_id = req.body.Data.transhipmentId
        const client = RedisConnector.getClient()
        const rd_file_key = `TRANSSHIPMENT_${trans_id}`
        const data_sample = await client.get(rd_file_key);
        if(data_sample){
          const info = JSON.parse(data_sample)
          const media_object_valid = getMediaObject(info.layout)
          const isValid = validMediaObjectCommit(media_object_valid, info.media_object)
          if(isValid){
            const bk_arr = info.fp.split("/");
            let check_bucket = ''
            if(bk_arr.length > 0){
              forEach(bk_arr, function(item, index, arr) {
                if(item){
                  check_bucket = check_bucket+'/'+item
                  if (!fs.existsSync(check_bucket)){
                    fs.mkdirSync(check_bucket);
                }
                }
              });
            }



            for (const [key, value] of Object.entries(info.media_object)) {
              const item = info.media_object[key]
              if(key == "other"){
                  for (const [other_key, other_value] of Object.entries(item)) {
                    const other_item = item[other_key]
                      let source = ""
                      let des = ""
                      let source_file = ""
                      let des_file= ""
                      if(other_item.type == "photo"){
                        source_file = `normal_cache_${other_item.file}.png`
                        des_file = `gd_photo_${other_item.file}.png`
                        source = `${rootPath}/public/media_cache/${trans_id}/${source_file}`
                        des = `${info.fp}${des_file}`
                        moveFile(source, des, async function(error){
                            if(error){
                              console.log("ERROR COMMIT FILE --> ", error)
                            }else{
                              const insert_result = await File.insertFileInfo(other_item.file, des_file, des_file, info.bk.toLowerCase(), info.fr.toLowerCase(), info.auth.au6, info.auth.au6, info.auth.au6, 0)
                              // console.log("INSERT RESULT = ", insert_result)
                            }
                        })
                      }
                  }
              }else{
                if(item){
                    let source = ""
                    let des = ""
                    let source_file = ""
                    let des_file= ""
                    if(item.type == "photo"){
                      source_file = `normal_cache_${item.file}.png`
                      des_file = `gd_photo_${item.file}.png`
                      source = `${rootPath}/public/media_cache/${trans_id}/${source_file}`
                      des = `${info.fp}${des_file}`
                    }else if(item.type == "video"){
                      source_file = `video-${item.file}.webm`
                      des_file = `video-${item.file}.webm`
                      source = `${rootPath}/public/media_cache/${trans_id}/${source_file}`
                      des = `${info.fp}${des_file}`
                      //if media type is video, move video thumb both
                      moveFile(`${rootPath}/public/media_cache/${trans_id}/vid_temp_thumbvideo-${item.file}.png`, `${info.fp}vid_temp_thumbvideo-${item.file}.png`, async function(error){
                      })
                    }
                    // console.log(source, des)


                    moveFile(source, des, async function(error){
                      if(error){
                        console.log("ERROR COMMIT FILE --> ", error)
                      }else{
                        const insert_result = await File.insertFileInfo(item.file, des_file, des_file, info.bk.toLowerCase(), info.fr.toLowerCase(), info.auth.au6, info.auth.au6, info.auth.au6, 0)
                        // console.log("INSERT RESULT = ", insert_result)
                      }
                        
                    })
                   
                    await client.del(rd_file_key);
                }
              }
              
            }

            res.send(util.sendSuccess({commit: true}))
          }else{
            return res.send(util.sendError(403, "TRANSSHIPMENT_IS_INVALID", "TRANSSHIPMENT IS INVALID"))
          }
        }else{
          return res.send(util.sendError(404, "TRANSSHIPMENT NOT FOUND", "TRANSSHIPMENT NOT FOUND"))
        }
    }else{
      return res.send(util.sendError(403, "REQUIRED", "transhipmentId is required!"))
    }
  }catch(error){
    console.log("Commit media is error: ",error)
    return res.send(util.sendError())
  }
}

const deleteOtherMedia = async (req, res)=>{
  try{
    const { media_id, trans_id } = req.params;
    //Lay ra thong tin va save lai
    const client = RedisConnector.getClient()
    const rd_file_key = `TRANSSHIPMENT_${trans_id}`

    const data_sample = await client.get(rd_file_key);
    const info = JSON.parse(data_sample)
    if(info.media_object){
      if(info.media_object.other){
        delete info.media_object.other[media_id]
        await client.set(rd_file_key, JSON.stringify(info));
        return res.send(util.sendSuccess({delete: true}))
      }else{
        return res.send(util.sendError())
      }
    }else{
     return res.send(util.sendError())
    }


    const file_id = req.body.file_id
    if(media_object[media_id]){
        media_object[media_id].file = file_id
        info.media_object = media_object
        await client.set(rd_file_key, JSON.stringify(info));
      return res.send(util.sendSuccess({commit: true}))
    }else{
      return res.send(util.sendError())
    }
    
  }catch(error){
    console.log(error)
    return res.send(util.sendError())
  }
}

const moveFile = (oldPath, newPath, callback) => {

    fs.rename(oldPath, newPath, function (err) {
        if (err) {
            if (err.code === 'EXDEV') {
                copy();
            } else {
                callback(err);
            }
            return;
        }
        callback();
    });

    function copy() {
        var readStream = fs.createReadStream(oldPath);
        var writeStream = fs.createWriteStream(newPath);

        readStream.on('error', callback);
        writeStream.on('error', callback);

        readStream.on('close', function () {
            fs.unlink(oldPath, callback);
        });

        readStream.pipe(writeStream);
    }
}


const validMediaObjectCommit = (root, compairelist)=>{
  var isValid = true
  if(compairelist){
    for (const [key, value] of Object.entries(root)) {
      if(compairelist[key]==null){
        isValid = false
      }
    }
  }else{
    isValid = false
  }
  
  return isValid;
}

const getMediaObject = (list)=>{
  var obj_out = {}
  list.forEach((item, index)=>{
    item.forEach((itemchild, indexchild)=>{
        obj_out[itemchild.id] = {
          type: itemchild.type,
          file: null
        }
    })
  })
  obj_out.other = {

  }
  return obj_out;
}

const removeTempFile = (req, res)=>{
  try{
    var result = findRemoveSync(`${rootPath}/public/media_cache`, {age: {seconds: 3600}, dir: "*", files: "*.*", limit: 500})
    res.send({success: true, remove: result})

  }catch(e){
    res.send({success: false, remove: 0})
  }
}


module.exports = {
  getThumbVideo,
  getThumbCacheVideo,
  getCacheVideo,
  getThumbCachePhoto,
  createQR,
  uploadBase64,
  uploadBase64Array,
  uploadVideoBlob,
  submitSocket,
  checkTransshipmentInfo,
  removeTempFile,
  commitMedia,
  deleteOtherMedia,
  commitVideo
}