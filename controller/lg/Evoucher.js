const config = require('../../config');
const util = require('../../common/util');
const constant = require('../../common/constant');
const oracledb = require('oracledb');
const oracleDB = require('../../Services/Connector/OracleDB.js');
const initQuery = require('../../config/InitQuery.js');
const DBExecute = require('../../Services/DBExecute.js');
const redisDb = require('../../Services/Connector/Redis.js');
const ttl = 2
const validation = async (req, res)=>{
	try{
		const response = await DBExecute.executeFunction(req)
		const browser_code = req.body.Device.DeviceId;


		var res_obj = {
			voucher_info: response
		}
		if(response[0][0]){
			const activation_info = response[0][0]
			const client = await redisDb.asyncRedisConnection()
			const value = await client.get(`${config.app.prefix}_VC_${activation_info.ACTIVATION_CODE}`);
			if(value!=null){
				if(value!=browser_code){
				  return res.send(util.error("VOUCHER_LOCKED", "VOUCHER_LOCKED"));
				}
			}else{
				await client.set(`${config.app.prefix}_VC_${activation_info.ACTIVATION_CODE}`, browser_code, 'EX', ttl * 60);
			}
			return res.send(util.sendSuccess(res_obj))
		}else{
			return res.send(util.error("VOUCHER_NOT_FOUND", "This voucher code is not existed"));
		}
	}catch(e){
		console.log("xxx",e)
		return res.send(util.error(constant.errorcode.BAD_REQUEST, constant.errorcode.BAD_REQUEST));
	}
}

module.exports = {
	validation: validation
}
