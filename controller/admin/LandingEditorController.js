const config = require('../../config');
const {ObjectId} = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
var forEach = require('async-foreach').forEach;
var _ = require('lodash');
const assert = require('assert');

var clientdb = null
var allcomponent = null

const langposition = {
	"vi": 0,
	"en": 1,
	"cn": 2
}


const createConnection = ()=>{
		return new Promise(async (resolve, rejected)=>{
			try{
				const url = `mongodb://${config.logsystem.host}:${config.logsystem.port}/`
				const client = await MongoClient.connect(url, { useNewUrlParser: true });
				return resolve(client)
			}catch(e){
				console.log("MongoDB Error: ", e)
				return rejected(e)
			}
		})
	}




const getLayoutEditor = async (req, res, next)=>{
try {
		let data_editor = []
		let lang = req.query.lng || "vi"
		

	 	const client = await createConnection()
	 	const database = client.db(config.mongo.db)
	 	const layout_cl = database.collection('layout');
	 	const component_cl = database.collection('component');
	 	const all_cmpnt = await component_cl.find({}).toArray()

	 	if(allcomponent){

	 	}else{
	 		allcomponent = {}
	 		forEach(all_cmpnt, (item, index)=>{
	 			allcomponent[item._type] = item
	 		}, function(){
	 			console.log("=============")
	 		})
	 	}


			const layout = await layout_cl.find({_id: ObjectId(req.params.layout_id)}).toArray()

	 		var layout_component = null
	 		var form_info = null
	 		let _path = "root"
	 		if(layout){
	 		
	 			layout_component = layout[0].component
	 			if(layout_component.internal_language){
	 				layout_component.internal_language = await parseInternalLanguage(layout_component.internal_language, lang)
	 			}
	 			await readEditorObject(layout_component, layout_cl, data_editor, _path)

	 		}
	 		var out_data = {
	 			
	 		}
	 		out_data.layout_component = layout_component


	 		res.send({status: true, data: data_editor})
	 
	 	client.close()
	} catch (e) {
	  console.log("ERROR ", e);
	  res.send([]);
	}
}




const readEditorObject = async (object, collection2, data_editor, _path)=>{
  	return Promise.all(Object.keys(object).map(async (props, index)=>{
  		let current_path;
  		if(typeof object[props] == "object"){
  			current_path = _path + "/" +props
  			
  		}

  		
  		if(typeof object[props] == "object"){
  				if(props == "define"){

  					if(allcomponent[object.type]){
  						const component_define = allcomponent[object.type].component
  						// console.log("component_define ", allcomponent[object.type].component.define, object.define)
  						
  						const aresult = await mapValueDefine(allcomponent[object.type].name, object.name, object.template_id, allcomponent[object.type].component.define, object.define)
  						data_editor.push({path:current_path, A3_RESULT: aresult} )


  						
  						if(component_define.define){
  							const resultmaped = await mapDefineEditorConfig(component_define.define, object.define, data_editor)
  							object.define = resultmaped
  						}

  						if(object.define.layout){
	  						await readEditorObject(object.define.layout, collection2, data_editor, current_path)
	  					}

  					}
  				} else {
  					if(object[props]){
  						await readEditorObject(object[props], collection2, data_editor, current_path)
  					}
  					
  				}
  				
  		}else{

  		}
  	}))

  }

  const mapValueDefine = (name, name_object, id_obj, component_define, component_value)=>{
  	return new Promise((resolve, rejected)=>{
  		let object_arr_define = {
  			component_name: name,
  			object_name: name_object,
  			id_obj: id_obj,
  			config: {}
  		}
  		forEach(Object.keys(component_define), (key_define, index)=>{
  				const cofp = component_define[key_define]
  				if(cofp.type == "text" || cofp.type == "string"){
  					object_arr_define.config[key_define] = { type: cofp.type, name: cofp.name, value: component_value[key_define]}
  				}
  		} )

  		return resolve(object_arr_define)

  	})
  }

  const mapDefineEditorConfig = (component_define, define, data_editor)=>{
  	return new Promise((resolve, rejected)=>{
  		
  			
  		  	// console.log(component_define,Object.keys(define))
		  	if(Object.keys(define)){
		  		forEach(Object.keys(define), async(keyitem, index)=>{
			  		if(component_define[keyitem]){
			  			const df_cpn = component_define[keyitem]
			  			
			  			if(typeof df_cpn === 'object'){

			  				if(df_cpn.type == "text" || df_cpn.type == "string"){
			  				if(Array.isArray(define[keyitem])){
			  					
				  					

				  					// if(define[keyitem][lpos]){
				  					// 	define[keyitem] = define[keyitem][lpos]
				  					// }else{
				  					// 	define[keyitem] = define[keyitem][0]
				  					// }
				  				}else{
				  					
				  				}
				  				
				  			}else if(df_cpn.type == "array"){

				  				if(df_cpn.item){
					  				define[keyitem].map(async (item_param, ind)=>{
					  					if(df_cpn.item.type == "object"){
						  					item_param = await parseArrayObjectParams(df_cpn.item.properties, item_param)
						  					return item_param
					  					}else{
											item_param = await parseArrayOtherParams(df_cpn.item.properties, item_param)
						  					return item_param
					  					}

					  				})
				  				}else{
				  					console.log("LAYOUT ERROR ","Missing 'item' config properties")
				  				}
				  			}else if(df_cpn.type == "object"){
				  				item_param = await parseObjectParams(df_cpn.properties, define[keyitem])
						  		return item_param
				  			}
			  			}

			  			
			  		}
			  		
			  	}, function(done){
			  		return resolve(define);
			  	})
		  	}else{
		  		return resolve(define);
		  	}
		  	
  	})


  }

  ///START PARSE BLOCK
  const parseArrayObjectParams = (param_define, param)=>{

  	return new Promise((resolve, rejected)=>{
  		forEach(Object.keys(param), (keyitem, index)=>{
  		// console.log("jxxjxx ",param[keyitem])
  		if(param_define[keyitem]){
	  			// console.log(param_define[keyitem])
	  			if(param_define[keyitem].type == "text" || param_define[keyitem].type == "string"){
	  				param[keyitem] = param[keyitem]
	  				
	  			}
	  		}
	  		
	  	}, function(){
	  		return resolve(param)
	  	})

  	})
  	
  }

   const parseObjectParams = (param_define, param)=>{

  	return new Promise((resolve, rejected)=>{
  		forEach(Object.keys(param), (keyitem, index)=>{
  		// console.log("jxxjxx ",param[keyitem])
  		if(param_define[keyitem]){
	  			// console.log(param_define[keyitem])
	  			if(param_define[keyitem].type == "text" || param_define[keyitem].type == "string"){
	  				param[keyitem] = param[keyitem]
	  				
	  				
	  			}
	  		}
	  		
	  	}, function(){
	  		return resolve(param)
	  	})

  	})
  	
  }

  const parseArrayOtherParams = (param_define, param)=>{
  	return new Promise((resolve, rejected)=>{
  		return resolve(param)
  	})
  	
  }

  const parseInternalLanguage = (internal_language, lang)=>{
  	return new Promise((resolve, rejected)=>{
  		 var lpos = langposition[lang]
	  	if(!lpos){ lpos = 0}
	  		forEach(Object.keys(internal_language), (keyitem, index)=>{
	  			internal_language[keyitem] = internal_language[keyitem][lpos] || internal_language[keyitem][0]
	  		}, function(){
	  			return resolve(internal_language)
	  		})
  	})


  }

   ///END PARSE BLOCK



  const readObject = async (object, collection2, all_component_id)=>{
  	return Promise.all(Object.keys(object).map(async (props, index)=>{
  		if(typeof object[props] == "object"){
  			if(object[props] instanceof ObjectId){
  				all_component_id.push(object[props])
  				const web_layout_component = await collection2.find({_id: object[props]}).toArray()
  				object[props] = web_layout_component[0].component

  			}else{
  				await readObject(object[props], collection2, all_component_id)
  			}
  		}
  	}))

  }
  const mapRefObject = async (input, parent_path)=>{
  	for (var [key, child] of Object.entries(input)) {
  	// 	if(key.toString() == "component"){
	 		
	 	// 	if(child instanceof ObjectId){
	 	// 		const web_layout_component = await collection2.find({_id: child}).toArray()
	 	// 		child = web_layout_component[0].component

	 	// 	}
 		// }else{
 		// 	// console.log(key)
 		// }

	 	if(typeof child == "object"){
	 		if(Array.isArray(child)){
	 			const path = parent_path+"/p/"+key
	 			mapRefArray(child, path)
	 		}else{
	 			const path = parent_path+"/p/"+key
		 		if(child.type){
		 			child["$ref"] = path
		 		}
		 		mapRefObject(child, path)
	 		}
	 	}else{
	 		
	 	}
	}

  }

  const mapRefArray = (input, parent_path)=>{
  	input.forEach((child, index)=>{
  		if(typeof child == "object"){
	 		if(Array.isArray(child)){
	 			const path = parent_path+"/"+index
	 			mapRefArray(child, path, collection2)
	 		}else{
	 			const path = parent_path+"/i/"+index
		 		if(child.type){
		 			child["$ref"] = path
		 		}
		 		mapRefObject(child, path)
	 		}
	 	}
  	})

  }


module.exports = {
	getLayoutEditor
}
