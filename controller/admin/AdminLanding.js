const config = require("../../config");
const constant = require("../../common/constant");
const RedisConnector = require("../../Services/Connector/RedisConnector.js");
const Logging = require("../../Services/Logging");
const util = require("../../common/util");
const orcdb = require("../../Services/Connector/OracleDB.js");
const initQuery = require("../../config/InitQuery.js");
var randomstring = require("randomstring");
const axios = require("axios");
var forEach = require("async-foreach").forEach;
const oracledb = require("oracledb");

const kafkaQueue = require("../../Services/Connector/Kafka.js");

const MongoConnector = require("../../Services/Connector/MongoDB.js");
const { ObjectId } = require("mongodb");

const db_name = "landing_sdk"


var allcomponent = null

const langposition = {
	"vi": 0,
	"en": 1,
	"cn": 2
}



const getFAQDetail = async (req, res) => {
  try {
   	const landingdb = new MongoConnector();
   	await landingdb.createConnection(db_name);
  	const faq_id = ObjectId(req.params.faq_id);
  	const collection = landingdb.getClient().collection("faq_config");
  	const faq_detail = await collection.find({_id: faq_id}).toArray()
    res.send({ success: true, data: faq_detail });
    landingdb.close();
  } catch (e) {
  	console.log(e)
    res.send({ success: false, data: null });
  }
};

const updateFAQDetail = async (req, res) => {
  try {
   	const landingdb = new MongoConnector();
   	await landingdb.createConnection(db_name);
  	const faq_id = ObjectId(req.params.faq_id);
  	const collection = landingdb.getClient().collection("faq_config");
  	const update_detail = await collection.update({_id: faq_id}, {$set: {data: req.body}}).toArray()
    res.send({ success: true, data: null });
    landingdb.close();
  } catch (e) {
  	console.log(e)
    res.send({ success: false, data: null });
  }
};

const getFAQList = async (req, res) => {
  try {
   	const landingdb = new MongoConnector();
   	await landingdb.createConnection(db_name);
  	const faq_id = ObjectId(req.params.faq_id);
  	const collection = landingdb.getClient().collection("faq_config");
  	let aggregate_opts = [
        { $project: { data: 0 } }
      ]

  	const faq_list = await collection.aggregate(aggregate_opts).toArray();
    res.send(faq_list);
    landingdb.close();
  } catch (e) {
  	console.log(e)
    res.send([]);
  }
};

const removeFAQ = async (req, res) => {
  try {
   	const landingdb = new MongoConnector();
   	await landingdb.createConnection(db_name);
  	const faq_id = ObjectId(req.body.faq_id);
  	const collection = landingdb.getClient().collection("faq_config");
  	const faq_detail = await collection.remove({_id: faq_id}).toArray()
    res.send({ success: true, data: [] });
    landingdb.close();
  } catch (e) {
  	console.log(e)
    res.send({ success: false, data: [] });
  }
};

const addNewFAQ = async (req, res) => {
  try {
   	const landingdb = new MongoConnector();
   	const insert_db = {
  		name:req.body.name,
  		data:[]
  	}

   	await landingdb.createConnection(db_name);
  	const collection = landingdb.getClient().collection("faq_config");
  	const faq_ins = await collection.insertOne(insert_db)
    res.send({ success: true, id: faq_ins.insertedId });
    landingdb.close();
  } catch (e) {
  	console.log(e)
    res.send({ success: false, id: null });
  }
};

const cloneFAQ = async (req, res) => {
  try {
    const landingdb = new MongoConnector();

    
    const faq_id = ObjectId(req.body.faq_id);
    await landingdb.createConnection(db_name);
    const collection = landingdb.getClient().collection("faq_config");
    const faq_detail = await collection.find({_id: faq_id}).toArray()
    const insert_db = {
      name:req.body.name,
      data: faq_detail[0].data
    }
    const faq_ins = await collection.insertOne(insert_db)
    res.send({ success: true, id: faq_ins.insertedId });
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false, id: null });
  }
};


const getLandingList = async (req, res) => {
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const faq_id = ObjectId(req.params.faq_id);
    const collection = landingdb.getClient().collection("web_instance");

    const query_string = {
        $lookup:
           {
              from: "org",
              localField: "org",
              foreignField: "_id",
              as: "org_info"
          }
     }
    const list_instance = await collection.aggregate([query_string]).toArray()
    res.send({ success: true, data: list_instance });
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false, data: null });
  }
};

const getPageList = async (req, res) => {
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const wi_id = ObjectId(req.params.wi_id);
    const collection = landingdb.getClient().collection("page");
    const collection_wi = landingdb.getClient().collection("web_instance");
    var page = 0;
    var perpage = 20;
    if(req.query.perpage){
      perpage = req.query.perpage*1
    }
    if(req.query.page){
      page = perpage*req.query.page
    }
    let aggregate_opts = [
            {$match: {web_instance_id: ObjectId(wi_id)}},
            { $sort: { _id: -1 } },
            { $skip : page },
            { $limit : perpage }
      ]
    const instance_page = await collection_wi.find({_id: ObjectId(wi_id)}).toArray()
    const list_page = await collection.aggregate(aggregate_opts).toArray()
    res.send({ success: true, wi: instance_page[0], data: list_page });
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false, wi: null, data: null });
  }
};

const updatePageInfo = async (req, res) => {
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const page_id = ObjectId(req.params.page_id);
    const collection = landingdb.getClient().collection("page");
    const update_detail = await collection.update({_id: page_id}, {$set: req.body})
    res.send({ success: true });
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false});
  }
};


const getListLayout = async (req, res) => {
    var page = 0;
    var perpage = 10;
    if(req.query.perpage){
      perpage = req.query.perpage*1
    }
    if(req.query.page){
      page = perpage*req.query.page
    }
    let logsData = {}
    try {
      const landingdb = new MongoConnector();
      await landingdb.createConnection(db_name);

      const collection = landingdb.getClient().collection("layout");
      const count = await collection.count()
      logsData.total = count
      let aggregate_opts = [
            { $sort: { _id: -1 } },
            { $skip : page },
            { $limit : perpage },
            { $project: { component:0 } }
      ]

      
      const logs_list = await collection.aggregate(aggregate_opts).toArray()
      logsData.list = logs_list
      res.send(logsData);
    } catch (e) {
      console.log(e)
      res.send({});
    }
}



const getLayoutJson = async (req, res) => {
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const page_id = ObjectId(req.params.layout_id);
    const collection = landingdb.getClient().collection("layout");

    const layout_page = await collection.find({_id: ObjectId(page_id)}).toArray()
    
    res.send({ success: true, data: layout_page[0]});
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false, wi: null, data: null });
  }
};

const updateLayoutJson = async (req, res) => {
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const page_id = ObjectId(req.params.layout_id);
    const collection = landingdb.getClient().collection("layout");
    const collection_history = landingdb.getClient().collection("layout_history");

    const layout_page = await collection.update({_id: page_id}, {$set: {component: req.body}})
    const insert_db = {
      layout_id: page_id,
      time: new Date().getTime(),
      data: req.body
    }
    const c = await collection_history.find({$query:{ layout_id: ObjectId(page_id)}, $orderby: { time : 1 }}).skip(5).toArray()
    c.map(async (item)=>{
      await collection_history.remove({_id: item._id})
    })
    const faq_ins = await collection_history.insertOne(insert_db) //insert history
    res.send({ success: true});
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false });
  }
};


const getLayotEditHistory = async (req, res)=>{
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const page_id = ObjectId(req.params.layout_id);
    const collection_history = landingdb.getClient().collection("layout_history");
    let aggregate_opts = [
            {$match: {layout_id: page_id}},
            { $sort: { _id: -1 } },
            { $project: { data:0 } }
      ]


    const c = await collection_history.aggregate(aggregate_opts).toArray()
    res.send({ success: true, data: c});
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false, data:[] });
  }
}

const revertLayout = async (req, res)=>{
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const layout_id = ObjectId(req.body.layout_id);
    const history_id = ObjectId(req.body.history_id);

    const collection_history = landingdb.getClient().collection("layout_history");
    const collection_layout = landingdb.getClient().collection("layout");

    const layout_current = await collection_layout.find({_id: ObjectId(layout_id)}).toArray()
    const layout_history = await collection_history.find({_id: ObjectId(history_id)}).toArray()

    await collection_layout.update({_id: layout_id}, {$set: {component: layout_history[0].data}})

    const c = await collection_history.find({$query:{ layout_id: ObjectId(layout_id)}, $orderby: { time : 1 }}).skip(5).toArray()
    c.map(async (item)=>{
      await collection_history.remove({_id: item._id})
    })
    const insert_db = {
      layout_id: layout_id,
      time: new Date().getTime(),
      data: layout_current[0].component
    }
    await collection_history.insertOne(insert_db) //insert history

    res.send({ success: true, data: []});
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false, data:[] });
  }
}


const getPageInfo = async (req, res)=>{
  try {
    const landingdb = new MongoConnector();
    await landingdb.createConnection(db_name);
    const page_id = ObjectId(req.params.page_id);

    const collection_page = landingdb.getClient().collection("page");
    const page_info = await collection_page.find({_id: ObjectId(page_id)}).toArray()

    res.send({ success: true, data: page_info[0]});
    landingdb.close();
  } catch (e) {
    console.log(e)
    res.send({ success: false, data:[] });
  }
}

module.exports = {
	getFAQDetail,
	updateFAQDetail,
	getFAQList,
	removeFAQ,
	addNewFAQ,
  cloneFAQ,
  getLandingList,
  getPageList,
  updatePageInfo,
  getListLayout,
  getLayoutJson,
  updateLayoutJson,
  getLayotEditHistory,
  revertLayout,
  getPageInfo
}