const config = require("../../config");
const constant = require("../../common/constant");
const RedisConnector = require("../../Services/Connector/RedisConnector.js");
const Logging = require("../../Services/Logging");
const util = require("../../common/util");
const orcdb = require("../../Services/Connector/OracleDB.js");
const initQuery = require("../../config/InitQuery.js");
var randomstring = require("randomstring");
const axios = require("axios");
var forEach = require("async-foreach").forEach;
const oracledb = require("oracledb");
const memory_cache = require("../../Services/MemCache.js");

const kafkaQueue = require("../../Services/Connector/Kafka.js");

const MongoConnector = require("../../Services/Connector/MongoDB.js");
const { ObjectId } = require("mongodb");

const landingdb = new MongoConnector();

const testQueue = async (req, res) => {
  const send_result = await kafkaQueue.send(
    { hello: "hoang" },
    config.kafka.kafka_topic
  );
  res.send(send_result);
};
const getListActionCode = async (req, res) => {
  try {
    const common_params = { cs1: constant.CUSOR };
    const all_store = await orcdb.execute(
      initQuery.SYS_ACTION_API_GETALL,
      common_params,
      config.oracledb.master
    );
    res.send({ success: false, data: all_store });
  } catch (e) {
    res.send({ success: false, data: null });
  }
};

const getAPIParam = async (req, res) => {
  try {
    const params = memory_cache.getParams(
      req.params.owner,
      req.params.package,
      req.params.store_code
    );
    res.send({ success: true, data: params });
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: null });
  }
};

const getAllAPIParam = async (req, res) => {
  try {
    const params = memory_cache.getAllParams();
    res.send({ success: true, data: params });
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: null });
  }
};

async function getLandingList(req, res) {
  try {
    await landingdb.createConnection("landingsdk");
    const collection = landingdb.getClient().collection("landing_list");
    const items = await collection
      .aggregate([
        {
          $lookup: {
            from: "language_group",
            localField: "language",
            foreignField: "_id",
            as: "language",
          },
        },
        {
          $lookup: {
            from: "landing_child_page",
            localField: "_id",
            foreignField: "landing",
            as: "landing_child_page",
          },
        },
      ])
      .toArray();
    landingdb.close();
    return res.send(items);
  } catch (error) {
    console.log(error);
    landingdb.close();
    return res.send(util.sendError());
  }
}

async function getListLanguageGroup(req, res) {
  try {
    await landingdb.createConnection("landingsdk");
    const collection = landingdb.getClient().collection("language_group");
    const list = await collection.find({}).toArray();
    // console.log(list)
    landingdb.close();
    return res.send(list);
  } catch (err) {
    console.log(err);
    landingdb.close();
    return res.send(util.sendError());
  }
}

async function getLanguageByGroupId(req, res) {
  try {
    await landingdb.createConnection("landingsdk");
    const { groupId } = req.params;
    const collection = landingdb.getClient().collection("language_translation");
    const list = await collection.find({ group: ObjectId(groupId) }).toArray();
    landingdb.close();
    return res.send(list);
  } catch (err) {
    console.log(err);
    landingdb.close();
    return res.send(util.sendError());
  }
}

async function insertBlockLanguage(req, res) {
  try {
    await landingdb.createConnection("landingsdk");
    const { group_name } = req.body;
    const collection = landingdb.getClient().collection("language_group");
    if (group_name) {
      const result = await collection.insert({
        group_name: group_name,
      });
      landingdb.close();
      return res.send(util.sendSuccess());
    }
  } catch (error) {
    console.log(error);
    landingdb.close();
    return res.send(util.sendError());
  }
}

async function insertLanguage(req, res) {
  try {
    await landingdb.createConnection("landingsdk");
    let { group, vi = "", en = "", translate_key = "", _id = "" } = req.body;
    let list = [];
    const collection = landingdb.getClient().collection("language_translation");
    list = await collection.find({ translate_key: translate_key }).toArray();
    console.log(list);
    if (list.length > 0 && !_id)
      return res.send(util.sendError(null, null, "Key already exist!"));
    if (_id && translate_key && group && vi) {
      // update
      const result = await collection.updateOne(
        { _id: ObjectId(_id) },
        {
          $set: {
            translate_key: translate_key,
            group: ObjectId(group),
            vi: vi.toString(),
            en: en ? en.toString() : vi.toString(),
          },
        }
      );
      // console.log(result.result);
      landingdb.close();
      return res.send(util.sendSuccess({ translate_key: translate_key }));
    }
    // insert new
    // console.log("vao day")
    if (!translate_key) {
      translate_key = randomstring.generate(8).toLowerCase();
    }
    if (group && vi) {
      const result = await collection.insert({
        group: ObjectId(group),
        translate_key: translate_key,
        vi: vi.toString(),
        en: en ? en.toString() : vi.toString(),
      });
      // console.log(result);
      landingdb.close();
      return res.send(util.sendSuccess({ translate_key: translate_key }));
    }
  } catch (error) {
    console.log(error);
    return res.send(util.sendError());
  }
}

module.exports = {
  getListActionCode,
  getAPIParam,
  getAllAPIParam,
  testQueue,
  getLandingList,
  getLanguageByGroupId,
  insertLanguage,
  insertBlockLanguage,
  getListLanguageGroup,
};
