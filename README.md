# HDI Open API (Hyper Services)

[![N|HoangPM](https://vietjetair.hdinsurance.com.vn/img/logo-hdi.svg)](https://hdinsurance.com.vn)

Author: HoangPM

Futures.

  - HDI OpenAPI
  - Files Upload
  - Video live stream
 
### Installation

##### Requirement
    - [Node.js](https://nodejs.org/) v4+ to run.
    - Orcale Client
    - Redis
    - Kafka
    - Mongodb (3.6+)

Install the dependencies and start the server.

```sh
$ cd hdi_openapi_nodejs
$ npm install
$ npm start
```
Run with PM2

```sh
$ pm2 start npm start --name "HDI Open API"
```


SETUP pm2 oracle path

```sh
$ export LD_LIBRARY_PATH=/disk2/u01/instantclient_18_5:$LD_LIBRARY_PATH
```
