class MemCache {
  constructor() {
  }
  static stores={}
  static params={}

  static addStore(key, value) {
    this.stores[key] = value
  }
  static addParams(_owner, _package, key, value) {

    if(!this.params[_owner]){
        this.params[_owner] = {}
        this.params[_owner][_package] = {}
        this.params[_owner][_package][key] = []
    }
    if(!this.params[_owner][_package]){
      this.params[_owner][_package] = {}
      this.params[_owner][_package][key] = []
    }
    if(!this.params[_owner][_package][key]){
      this.params[_owner][_package][key] = []
    }
    this.params[_owner][_package][key].push(value)
  }
  static getStores() {
    return this.stores
  }
  static getStore(api_code) {
    return this.stores[api_code]
  }
  static getParams(_owner, _package, store_code) {
    if(this.params[_owner]){
      if(this.params[_owner][_package]){
        if(this.params[_owner][_package][store_code]){
           return this.params[_owner][_package][store_code]
        }else{
          return []
        }
      }else{
        return []
      }
    }else{
      console.log("MemCache - not find owner:  ", _owner)
      return []
    }
   
  }
  static getAllParams() {
    return this.params
  }
  static removeStoreParams(store_code) {
    this.params = {}
  }
}
module.exports = MemCache;