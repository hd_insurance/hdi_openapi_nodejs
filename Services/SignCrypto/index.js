var crypto = require('crypto');
var path = require("path");
var fs = require('fs');
var cert_folder = require("../../resources/cert");


this.sign = (str_data, ptn, algor)=>{
	return new Promise((resolved, rejected)=>{
		try{
			// const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
			// 	// The standard secure default length for RSA keys is 2048 bits
			// 	modulusLength: 2048,
			// 	publicKeyEncoding: {
			//       type: 'spki',
			//       format: 'pem'   
			//     },
			//     privateKeyEncoding: {
			//       type: 'pkcs8',
			//       format: 'pem',
			//   	} 
			// })

			// fs.writeFile('public.pem', publicKey, function (err) {
			//   if (err) return console.log(err);
			//   console.log('publicKey > public.pem');
			// });

			// fs.writeFile('private.pem', privateKey, function (err) {
			//   if (err) return console.log(err);
			//   console.log('privateKey > private.pem');
			// });

			// console.log(privateKey)
			// console.log(publicKey)

			const filepath = path.resolve(`${cert_folder.path}/${ptn}/private.pem`)
			if (!fs.existsSync(filepath)) {
			    return resolved(null)
			}
			const privateKey = fs.readFileSync(filepath);
			const signature = crypto.sign(algor, Buffer.from(str_data), {
				key: privateKey,
				padding: crypto.constants.RSA_PKCS1_PSS_PADDING,
			})
			return resolved(signature);
		}catch(e){
			console.log(e)
			return resolved(null)
		}
	})
}

this.verify = (str_data, signature, ptn, algor)=>{
	return new Promise(async (resolved, rejected)=>{
		try{
			const filepath = path.resolve(`${cert_folder.path}/${ptn}/public.pem`)
			if (!fs.existsSync(filepath)) {
			    return resolved(false)
			}
			const publicKey = fs.readFileSync(filepath);
			const isVerified = crypto.verify(
				algor,
				Buffer.from(str_data),
				{
					key: publicKey,
					padding: crypto.constants.RSA_PKCS1_PSS_PADDING,
				},
				signature
			)
			return resolved(isVerified)
		}catch(e){
			return resolved(false)
		}
		
	})
}

this.createTextSignature = (password, ptn_code, data)=>{
	const str_data = JSON.stringify(data)
	const str_md5 = crypto.createHash('md5').update(str_data).digest("hex");
	const str = `${ptn_code}.${str_md5}`
	const sha256 = crypto.createHmac('sha256', password).update(str, password).digest("base64");
	return sha256
}


module.exports = {
  sign: this.sign,
  verify: this.verify,
  createTextSignature: this.createTextSignature,
}
