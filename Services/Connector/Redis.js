const asyncRedis = require("async-redis");
const redis = require("redis");
const config = require('../../config');
var client;



createConnection = (callback)=>{
		try{
			if(client){
				if(client.connected){
					client.quit()
				}
			}
			const normalclient = redis.createClient({host: config.redis.host, port: config.redis.port, password: config.redis.password, db: config.redis.db});
			normalclient.on("error", function(error) {
				console.log("Connect Redis Error ", {host: config.redis.host, port: config.redis.port, db: config.redis.db})
				return callback(false)
			});
			normalclient.on("connect", function(error) {
			  console.log("Connect Redis Success ", {host: config.redis.host, port: config.redis.port, db: config.redis.db})
			  client = asyncRedis.decorate(normalclient)
			  callback(true)
			});

			// normalclient.monitor(function(err, res) {
			//   console.log("Entering monitoring mode.");
			// });
			// normalclient.on("monitor", function(time, args, rawReply) {
			//   console.log(time + ": " + args); // 1458910076.446514:['set', 'foo', 'bar']
			// });

		}catch(e){
			callback(false)
		}
}


getClient = ()=>{

	return client
}

module.exports = {
  createConnection,
  getClient
}