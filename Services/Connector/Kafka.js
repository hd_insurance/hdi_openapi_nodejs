const config = require('../../config');
const { Kafka } = require('kafkajs')

var _ = require('lodash');


const kafka = new Kafka({
  clientId: 'node-localapp',
  brokers: [config.kafka.kafkaHost]
})

this.producer = kafka.producer()

this.init = async ()=>{
	console.log('INIT kafka ', config.kafka.kafkaHost)
	try {
	  await this.producer.connect()
	}
	catch(e) {
	  console.log(e);
	}
}

this.send = (data, topic)=>{
	return new Promise(async (resolved, rejected)=>{
		try{
			const send_result = await this.producer.send({
			  topic: topic,
			  messages: [
			    { value: JSON.stringify(data) },
			  ],
			})

			// await this.producer.disconnect()

			return resolved(send_result)
		}catch(e){
			return resolved({success: false})
		}
		
	})
	


}


module.exports = {
 init: this.init,
 send: this.send
}