const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");

const config = require("../../config");

class MongoConnector {
  constructor() {
    this.createConnection();
    this.getClient = this.getClient.bind(this);
    this.close = this.close.bind(this);
    this.quit = this.quit.bind(this);
  }

  async createConnection(schema = config.logsystem.scheme) {
    const url = `mongodb://${config.logsystem.host}:${config.logsystem.port}/`;
    const client = await MongoClient.connect(url, { useNewUrlParser: true });
    this.clientdb = client.db(schema);
    this.client = client;
  }
  getClient() {
    return this.clientdb;
  }
  close() {
    if (this.client) {
      this.client.close();
    }
  }
  quit() {
    this.clientdb.close();
  }
}
module.exports = MongoConnector;
