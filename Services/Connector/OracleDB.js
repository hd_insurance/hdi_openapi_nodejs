const oracledb = require('oracledb');
oracledb.fetchAsString = [ oracledb.DATE, oracledb.CLOB ];
const config = require('../../config');
var _ = require('lodash');
var forEach = require('async-foreach').forEach;

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
numRows = 100;

this.createConnection = (conn_str = config.oracledb.master)=>{
	return new Promise(async (resolved, rejected)=>{
		try{
			const conn_obj = this.parseConnectionString(conn_str)
			const connection = await oracledb.getConnection( {
					user: conn_obj.user,
					password: conn_obj.pwd,
					connectString : conn_obj.connectString
			    });
			return resolved(connection)
		}catch(e){
			console.log(e)
			return rejected("Create connection failed! ", e)
		}
		
	})
}
this.execute = async (query, in_params={}, conn_str = config.oracledb.master)=>{
	return new Promise(async (resolved, rejected)=>{
		let connection = null
		let array_out = []
		try{
			connection = await this.createConnection(conn_str);

			// console.log("conn_str>> ", conn_str)
			// console.log("query>> ", query)
			// console.log("in_params>> ", in_params)


		   	const result = await connection.execute(query, in_params, { autoCommit: true });
		   	const cs_list = _.toArray(result.outBinds)
		   	forEach(cs_list, async function(item, index){
		   		let array_cusor = []
		   		var done = this.async()
		   		let rows;
		   		do {
			      rows = await item.getRows(numRows);
			      if (rows.length > 0) {
			        array_cusor = _.concat(array_cusor, rows)
			      }
			    } while (rows.length === numRows);
			   		await item.close();
			   		array_out.push(array_cusor)
		   		done()
		   	}, ()=>{
			   	connection.close()
			   	return resolved(array_out)
		   })
    		
		}catch(e){
			console.log("Connection ERROR: ",e)
			if(connection){
				connection.close()
			}
			return rejected(e)
		}finally {
		    if (connection) {
		      try {

		      } catch (err) {
		        console.error('ORC-ERROR ',err);
		      }
		    }
		}
	})
}

this.getFuncParams = (store_name)=>{
	return new Promise(async (resolved, rejected)=>{
		const connection = await this.createConnection(config.oracledb.master);
		const result = await connection.execute(`SELECT * FROM SYS.ALL_ARGUMENTS where object_name  = UPPER('${store_name}') ORDER BY POSITION ASC`);
		connection.close()
		return resolved(result)
	})
}

this.parseConnectionString = (str)=>{
	str = str.replace('Data Source=', '');
	str = str.split(';')
	const str_parse = {
		connectString: str[0],
		user: str[1].replace('User Id=',''),
		pwd: str[2].replace('Password=','')
	}
	return str_parse
}

this.query_string = (package, store_procedure, params_mapped)=>{
	
	const arr_param = _.keys(params_mapped)
	let str_param = ''
	if(arr_param.length > 0){
		str_param = ':'+arr_param.join(',:')
	}
	const prequeryCursor = this.preQueryCursor(params_mapped)
	return `BEGIN ${package?(package.toUpperCase()+"."):""}${store_procedure.toUpperCase()}(${str_param}); ${prequeryCursor} END;`
}

this.preQueryCursor = (params)=>{
	var strout = ""
	Object.keys(params).map((param_key, index)=>{
		const param = params[param_key]
		if(param.type == 2021){
			strout+=`if(:${param_key} IS NULL) then OPEN :${param_key} FOR SELECT * FROM dual WHERE 1 = 0; end if;`
		}
	})
	return strout;
}

module.exports = {
  createConnection:this.createConnection,
  execute:this.execute,
  getFuncParams: this.getFuncParams,
  query_string: this.query_string,
  parseConnectionString: this.parseConnectionString
}