const asyncRedis = require("async-redis");
const config = require('../../config');

class RedisConnector {
	constructor(redis_db) {
		if(!redis_db){
			redis_db = config.redis.db
		}
		this.client = asyncRedis.createClient({host: config.redis.host, port: config.redis.port, db: redis_db});
		this.client.on("error", function(error) {
			console.log("Connect Redis Error 2 ", {host: config.redis.host, port: config.redis.port, db: redis_db})
		});
		this.client.on("connect", function(error) {
		  	//console.log("Connect Redis Success 2 ", {host: config.redis.host, port: config.redis.port, db: config.redis.db})
		});
		this.getClient = this.getClient.bind(this)
		this.close = this.close.bind(this)
		this.quit = this.quit.bind(this)
		this.onConnected = this.onConnected.bind(this)
	}
	onConnected(){

	}
	getClient(){
		return this.client;
	}
	close(){
		this.client.quit()
	}
	quit(){
		this.client.quit()
	}
}
module.exports = RedisConnector