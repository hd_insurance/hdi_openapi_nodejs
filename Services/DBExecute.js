const RedisConnector = require('./Connector/Redis.js');
const oracledb = require('oracledb');
const orcdb = require('./Connector/OracleDB.js');
const constant = require('../common/constant');
var forEach = require('async-foreach').forEach;
const config = require('../config');
const Logging = require('./Logging');
var _ = require('lodash');




this.executeStore = (data, api_code)=>{
	return new Promise(async (resolved, rejected)=>{
		try{
			const client = RedisConnector.getClient() 
			// console.log("action code", data.Action.ActionCode)
			const req_params = Object.entries(data)
			// console.log(req_params, store_params)
			
			const PROCEDURE_TEMP = await client.get(`STR_${data.Action.ActionCode.toUpperCase()}`)

			if(PROCEDURE_TEMP){
				const store = await this.getStoreRedis(api_code)
				const database_config = await this.getApiDB(api_code, "LIVE")
				const data_parse = orcdb.parseConnectionString(database_config.ORACLE.DATA)
				const owner = data_parse.user

				let PACKAGES = database_config.ORACLE.PACKAGES
				let PROCEDURE = PROCEDURE_TEMP
				if(PROCEDURE_TEMP.split(".").length > 1){
					PACKAGES = PROCEDURE_TEMP.split(".")[0]
					PROCEDURE = PROCEDURE_TEMP.split(".")[1]
				}

				const rd_store_params = await client.get(`PARAM_${owner}-${PACKAGES.toUpperCase()}-${data.Action.ActionCode.toUpperCase()}`)
				if(!rd_store_params){
					console.log("GET REDIS STORE PARAMS ERROR ", `PARAM_${owner}-${PACKAGES.toUpperCase()}-${data.Action.ActionCode.toUpperCase()}`)
					try{
						Logging.push(null, "error", "request", {message:'Khong tim thay thong tin param trong redis', redis_key: `PARAM_${owner}-${PACKAGES.toUpperCase()}-${data.Action.ActionCode.toUpperCase()}`, source_at: "DBExecute.executeStore"}, `DBExecute.executeStore / Not found params redis`)
					}catch(e){}

					return rejected(constant.errorcode.MISSING_PARAMETER)
				}
				const store_params = JSON.parse(rd_store_params)
				if(store_params){
					if(store_params.length > 0){
						if(data){
							if(typeof data != 'object'){
								return rejected(constant.errorcode.INVALID_DATA)
							}
						}else{
							return rejected(constant.errorcode.INVALID_DATA)
						}
					}
				}
				if(!this.validParameters(req_params, store_params)){
					
					console.log("-----------------------------")
					console.log("Get param info ", owner, PACKAGES.toUpperCase(), api_code.toUpperCase())
					console.log("Prameter is invalid: ")
					console.log("Input: ",req_params)
					console.log("Required: ", store_params)
					console.log("-----------------------------")
					try{
						Logging.push(null, "error", "request", {message:'Khong tim thay thong tin param trong redis', source_at: "DBExecute.executeStore", req_params:req_params, store_params: store_params, package: PACKAGES.toUpperCase(), owner: owner}, `DBExecute.executeStore / valid parameters`)
					}catch(e){}
					const error_message = {
						message: constant.errorcode.MISSING_PARAMETER,
						input: req_params,
						required: store_params
					}
					return rejected(error_message) //Parameter Missing or Invalid
					
				}
				const store_params_mapped = await this.paramMapping(store_params, req_params)
				const query_string = orcdb.query_string(PACKAGES, PROCEDURE, store_params_mapped)
				// console.log(query_string)
				// console.log(database_config.ORACLE.DATA)
				// console.log(store_params_mapped)
				const result = await orcdb.execute(query_string, store_params_mapped, database_config.ORACLE.DATA)
				return resolved(result)
			}else{
				console.log("PROCEDURE_TEMP ", PROCEDURE_TEMP)

				return rejected(constant.errorcode.ACCESS_DENIED)
			}
		}catch(error){
			console.log(error)
			if(error.message){
				let xxx = error.toString().match(/(@)(.*)@/g);
				if(xxx){
					let error_code =  xxx[0].toString().match(/@(.*)([0-9]:)/g);
					if(error_code[0]){
						return rejected({error: error_code[0].replace(":","").replace("@",""), message: xxx[0].replace(error_code,"").replace("@","")})
					}else{
						return rejected({error: "DB_"+error.errorNum, message: error.message})
					}
				}else{
					return rejected({error: "DB_"+error.errorNum, message: error.message})
				}
			}else{
				return rejected({error: "DB_"+error.errorNum, message: error.message})
			}
			
			
		}
		
	})
}


this.executeFunction = (req)=>{
	return new Promise(async (resolved, rejected)=>{
		try{
			const data = req.body
			const api_code = data.Action.ActionCode.toUpperCase()
			const client = RedisConnector.getClient()
			const req_params = Object.entries(data.Data)
			// console.log(req_params, store_params)
			
			const PROCEDURE_TEMP = await client.get(`STR_${data.Action.ActionCode.toUpperCase()}`)
			if(PROCEDURE_TEMP){
				const store = await this.getStoreRedis(api_code)
				const database_config = await this.getApiDB(api_code, req.environment)
				const data_parse = orcdb.parseConnectionString(database_config.ORACLE.DATA)
				const owner = data_parse.user

				let PACKAGES = database_config.ORACLE.PACKAGES
				let PROCEDURE = PROCEDURE_TEMP
				if(PROCEDURE_TEMP.split(".").length > 1){
					PACKAGES = PROCEDURE_TEMP.split(".")[0]
					PROCEDURE = PROCEDURE_TEMP.split(".")[1]
				}

				const rd_store_params = await client.get(`PARAM_${owner}-${PACKAGES.toUpperCase()}-${data.Action.ActionCode.toUpperCase()}`)
				if(!rd_store_params){
					console.log("GET REDIS STORE PARAMS ERROR ", `PARAM_${owner}-${PACKAGES.toUpperCase()}-${data.Action.ActionCode.toUpperCase()}`)
					try{
						Logging.push(null, "error", "request", {message:'Khong tim thay thong tin param trong redis', source_at: "DBExecute.executeFunction", redis_key: `PARAM_${owner}-${PACKAGES.toUpperCase()}-${data.Action.ActionCode.toUpperCase()}`}, `DBExecute.executeFunction / valid parameters`)
					}catch(e){}

					return rejected(constant.errorcode.MISSING_PARAMETER)
				}
				const store_params = JSON.parse(rd_store_params)
				if(store_params){
					if(store_params.length > 0){
						if(data.Data){
							if(typeof data.Data != 'object'){
								return rejected(constant.errorcode.INVALID_DATA)
							}
						}else{
							return rejected(constant.errorcode.INVALID_DATA)
						}
					}
				}
				if(!this.validParameters(req_params, store_params)){

					console.log("-----------------------------")
					console.log("Get param info ", owner, PACKAGES.toUpperCase(), data.Action.ActionCode.toUpperCase())
					console.log("Prameter is invalid: ")
					console.log("Input: ",req_params)
					console.log("Required: ", store_params)
					console.log("-----------------------------")
					try{
						Logging.push(req.id, "error", "request", {message:'Khong tim thay thong tin param trong redis', source_at: "DBExecute.executeFunction", req_params:req_params, store_params: store_params, package: PACKAGES.toUpperCase(), owner: owner}, `DBExecute.executeFunction / valid parameters`)
					}catch(e){}

					return rejected(constant.errorcode.MISSING_PARAMETER) //Parameter Missing or Invalid
					
				}
				const store_params_mapped = await this.paramMapping(store_params, req_params)
				const query_string = orcdb.query_string(PACKAGES, PROCEDURE, store_params_mapped)
				// console.log(query_string)
				// console.log(database_config.ORACLE.DATA)
				// console.log(store_params_mapped)
				const result = await orcdb.execute(query_string, store_params_mapped, database_config.ORACLE.DATA)
				return resolved(result)
			}else{
				console.log("PROCEDURE_TEMP ", PROCEDURE_TEMP)

				return rejected(constant.errorcode.ACCESS_DENIED)
			}
		}catch(error){
			console.log(error)
			Logging.push(null, "error", "system", {source_at: "DBExecute.executeFunction", error: error.toString()}, `DBExecute.executeFunction / Error catch`)
			if(error.message){
				let xxx = error.toString().match(/(@)(.*)@/g);
				if(xxx){
					let error_code =  xxx[0].toString().match(/@(.*)([0-9]:)/g);
					if(error_code[0]){
						return rejected({error: error_code[0].replace(":","").replace("@",""), message: xxx[0].replace(error_code,"").replace("@","")})
					}else{
						return rejected({error: "DB_"+error.errorNum, message: error.message})
					}
				}else{
					return rejected({error: "DB_"+error.errorNum, message: error.message})
				}
			}else{
				return rejected({error: "DB_"+error.errorNum, message: error.message})
			}
			
			
		}
		
	})
}

this.paramMapping = (store_params, req_params)=>{
	return new Promise((resolved, rejected)=>{
		let obj_params = {}
		if(req_params==null){
			return resolved(null)
		}
		store_params.forEach((item, index)=>{
			if(item.type == 'REF CURSOR'){
				obj_params[item.name] = { type: oracledb.CURSOR, dir: oracledb.BIND_OUT }
			}else{
				const value = req_params[index][1]
				if(value == null){
				obj_params[item.name] = ''
				}else{
					obj_params[item.name] = req_params[index][1]
				}
			}
		})
		return resolved(obj_params)
	})
}

this.validParameters = (req_data, store_params)=>{
	const real_store_param = _.filter(store_params, function(o) { return o.type!='REF CURSOR'; });
	let isValid = (req_data.length == real_store_param.length)
	if(!isValid){
		console.log("Parameter is not valid!")
		console.log(real_store_param, req_data)
	}

	return isValid
}

this.getStoreRedis = (api_code)=>{
	return new Promise(async (resolved, rejected)=>{
		const client = RedisConnector.getClient()
		const store = await client.get(api_code);
		return resolved(store);
	})
}

this.getApiDB = (api_code, env)=>{
	let db_config = {}
	return new Promise(async (resolved, rejected)=>{
		const keyname = `${config.app.prefix}API_DB:${api_code}:${env}:*`
		const client = RedisConnector.getClient()
		const db = await client.keys(keyname);
		forEach(db, async function(item, index){
		  	var done = this.async()
		  	let mode = item.split(':')
		  	mode = mode[3]
		  	const db_item = await client.get(item);
		  	db_config[mode.toUpperCase()] = JSON.parse(db_item)
			done()
		  }, function(){
		  	return resolved(db_config)
		})
	})
}


module.exports = {
 	executeFunction: this.executeFunction,
 	paramMapping: this.paramMapping,
 	executeStore: this.executeStore
}