const config = require('../../config');
const kafkaQueue = require('../Connector/Kafka.js');
const MongoConnector = require('../Connector/MongoDB.js');
const kafka_topic = 'example';
const {ObjectId} = require('mongodb');
var randomstring = require("randomstring");
const logdb = new MongoConnector()
logdb.createConnection()


const reqlog = async (type, req = {}, name="")=>{
		try{

			let source = null
	 		const e = new Error();
		  	const regex = /\((.*):(\d+):(\d+)\)$/
		  	const match = regex.exec(e.stack.split("\n")[2]);
		  	if(match){
		  		if(match.input){
			  		source = match.input.toString()
			  	}
		  	}
		  	
			const collection = logdb.getClient().collection('req_logs');
		    const log_item = { 
		      request_id: req.request_id,
		      time: new Date(),
		      app: ObjectId("615daa12242c430775e52b60"),
		      title: name,
		      source: source,
		      evt_type: "request",
		      log_type: type,
		      content: {url: req.url, request: req.request, response: req.response}
		    }
		    const insert_result = await collection.insertOne(log_item)
		}catch(e){
			console.log(e)
		}
}


const log = async (type, req = {}, name="")=>{
		try{

			let source = null
	 		const e = new Error();
		  	const regex = /\((.*):(\d+):(\d+)\)$/
		  	const match = regex.exec(e.stack.split("\n")[2]);
		  	if(match){
			  	if(match.input){
			  		source = match.input.toString()
			  	}
		  	}
			const collection = logdb.getClient().collection('req_logs');
		    const log_item = { 
		      request_id: req.id,
		      time: new Date(),
		      app: ObjectId("615daa12242c430775e52b60"),
		      title: name,
		      source: source,
		      evt_type: "request",
		      log_type: type,
		      content: {url: "/open-api", request:req.request.body, response: req.request.response}
		    }
		    const insert_result = await collection.insertOne(log_item)
		}catch(e){
			console.log(e)
		}
}

const push = async (group_id, severity, type, content = {}, name="")=>{
		try{

			let source = null
	 		const e = new Error();
		  	const regex = /\((.*):(\d+):(\d+)\)$/
		  	const match = regex.exec(e.stack.split("\n")[2]);
		  	if(match){
			  	if(match.input){
			  		source = match.input.toString()
			  	}
		  	}
			const collection = logdb.getClient().collection('event_logs');
		    const log_item = { 
		      group_id: group_id?group_id:randomstring.generate({length: 12}),
		      time: new Date(),
		      source: source||"none",
		      app: ObjectId("615daa12242c430775e52b60"),
		      title: name,
		      severity: severity,
		      type: type,
		      content: content
		    }
		    const insert_result = await collection.insertOne(log_item)
		}catch(e){
			console.log(e)
		}
}

const handle = async (req_id, type, data, name="")=>{
		try{
			let source = null
	 		const e = new Error();
		  	const regex = /\((.*):(\d+):(\d+)\)$/
		  	const match = regex.exec(e.stack.split("\n")[2]);
		  	if(match){
		  		if(match.input){
		  			source = match.input.toString()
		  		}
		  	}
			const collection = logdb.getClient().collection('req_logs');
		    const log_item = { 
		      request_id: req_id?req_id:randomstring.generate({length: 12}),
		      time: new Date(),
		      app: ObjectId("615daa12242c430775e52b60"),
		      title: name,
		      source: source,
		      evt_type: "info",
		      log_type: type,
		      content: data
		    }
		    const insert_result = await collection.insertOne(log_item)
		}catch(e){
			console.log(e)
		}
}






module.exports = {
  log: log,
  push: push,
  reqlog: reqlog,
  handle: handle
}