var cache = require('memory-cache');
const RedisConnector = require('../Services/Connector/Redis.js');
const intercept = require('express-mung')
const config = require('../config');
var newCache = new cache.Cache();
this.init = async (req, res, next)=>{
	try{
		const req_data = req.body
		const partner_config = req.partner_config
		if(!req.body.Action){
			return next()
		}
		if(req.body.Action.ActionCode){
			const action_code = req.body.Action.ActionCode
			const api_info = await this.getAPIConfig(action_code)
			if(api_info.AUTOCACHE == 1){
				this.storeCache(res)
			}
		}
		return next()
	}catch(err){
		return next()
	}
}

this.storeCache = (res)=>{
	  var oldWrite = res.write,
	      oldEnd = res.end;
	  var chunks = [];
	  res.write = function (chunk) {
	    chunks.push(new Buffer(chunk));
	    oldWrite.apply(res, arguments);
	  };
	  res.end = function (chunk) {
	    if (chunk)
	      chunks.push(new Buffer(chunk));
	    var body = Buffer.concat(chunks).toString('utf8');
	    oldEnd.apply(res, arguments);
	  };
}
this.getAPIConfig = (api_code)=>{
	return new Promise(async (resolved, rejected)=>{
    	const client = RedisConnector.getClient()
		const api_info = await client.get(`${config.app.prefix}${api_code}`);
		if(api_info){
		  	return resolved(JSON.parse(api_info))
		}else{
		  	return resolved({})
		} 
	})
}

module.exports = {
  init: this.init
}

