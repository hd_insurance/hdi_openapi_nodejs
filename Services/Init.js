const orcdb = require('./Connector/OracleDB.js');
const colors = require('colors');
const RedisConnector = require('./Connector/Redis.js');
const initQuery = require('../config/InitQuery.js');
const constant = require('../common/constant.js');
var randomstring = require("randomstring");
const config = require('../config');
var _ = require('lodash');
const urlparser = require('urlparser');
var forEach = require('async-foreach').forEach;

init = async ()=>{
	try{
		const isDisableInit = false;
		if(isDisableInit){ //config true to disable init cache when start application
			return;
		}
		const client = RedisConnector.getClient()
		await client.set("NODE_TRACKING", new Date().getTime().toString())
		const common_params = {cs1: constant.CUSOR}
		//console.log(`${`[1]`.grey.bgCyan} QUERY >> PARTNER SYS_API_DATABASE_GETALL`)
		const all_database = await orcdb.execute(initQuery.SYS_API_DATABASE_GETALL, common_params, config.oracledb.master)
		//console.log(`${`[2]`.grey.bgCyan} QUERY >> PARTNER SYS_ACTION_API_GETALL`)
		const all_store = await orcdb.execute(initQuery.SYS_ACTION_API_GETALL, common_params, config.oracledb.master)
		//console.log(`${`[3]`.grey.bgCyan} QUERY >> PARTNER SYS_PARTNER_CONFIG_GETALL`)
		const all_ptn_config = await orcdb.execute(initQuery.SYS_PARTNER_CONFIG_GETALL, common_params, config.oracledb.master)
		//console.log(`${`[4]`.grey.bgCyan} QUERY >> PARTNER SYS_PARTNER_GROUP_API_GETALL`)
		const all_ptn_api_group = await orcdb.execute(initQuery.SYS_PARTNER_GROUP_API_GETALL, common_params, config.oracledb.master)
		//console.log(`${`[5]`.grey.bgCyan} QUERY >> PARTNER SYS_DOMAIN_GETALL`)
		const all_domain = await orcdb.execute(initQuery.SYS_DOMAIN_GETALL, common_params, config.oracledb.master)
		//console.log(`${`[6]`.grey.bgCyan} QUERY >> PARTNER SYS_USER_GETALL`)
		const all_user = await orcdb.execute(initQuery.SYS_USER_GETALL, common_params, config.oracledb.master)
		//console.log(`${`[7]`.grey.bgCyan} QUERY >> PARTNER SYS_BEHAVIOS_CACHE_GETALL`)
		const all_behavior_cache = await orcdb.execute(initQuery.SYS_BEHAVIOS_CACHE_GETALL, common_params, config.oracledb.master)
		//console.log('==================================================')
		// Cache Partner config
		if(all_ptn_config[0]){
			console.log('[1] CACHE >> PARTNER CONFIG')
			await this.cachePartnerConfig(all_ptn_config[0])
		}
		// Cache Partner API
		if(all_ptn_api_group[0]){
			console.log('[2] CACHE >> API GROUP')
			this.cachePartnerAPIGroup(all_ptn_api_group[0])
		}
		// Cache API database
		if(all_database[0]){
			console.log('[3] CACHE >> DATABASES CONFIG')
			await this.cacheAPIDatabase(all_database[0])
		}
		//Cache System Domain
		if(all_domain[0]){
			console.log('[4] CACHE >> DOMAINS CONFIG')
			this.cacheDomain(all_domain[0])
		}
		// Cache user info
		if(all_user[0]){
			console.log('[5] CACHE >> USERS')
			this.cacheUser(all_user[0])
		}
		// Cache Store: Mapping API code to store
		if(all_store[0]){
			console.log('[6] CACHE >> STORE CONFIG')
			await this.cacheStore(all_store[0])
		}
		console.log('[6] CACHE >> BEHAVIOR CONFIG')
		if(all_behavior_cache[0]){
			await this.cacheBehaviorCacheSetting(all_behavior_cache[0])
		}
		console.log('INIT CACHE DONE'.black.bgGreen)
	}catch(e){
		console.log("  Init Error",e)
	}
}

this.cachePartnerConfig  = (all_ptn_config)=>{
	return new Promise(async (resolved, rejected)=>{
		const client = RedisConnector.getClient()
		forEach(all_ptn_config, async function (item, index){
			const keyname = `${config.app.prefix}${item.PARTNER_CODE}_${item.ENVIROMENT_CODE}`

			Object.keys(item).forEach(function(key) {
			    if(item[key] === null) {
			        item[key] = '';
			    }
			})

			if(item.ENVIROMENT_CODE!=null){
				item.ENVIROMENT_CODE=""
			}
			await client.hmset(keyname, item)
		}, ()=>{
			return resolved(true)
		})
	})
}

this.cachePartnerAPIGroup = async (all_ptn_api_group)=>{
	const client = RedisConnector.getClient()
	forEach(all_ptn_api_group, async function (item, index){
		var done = this.async()
		const keyname = `${config.app.prefix}API_GR_${item.PARTNER_CODE}_${item.API_CODE}`
		await client.set(keyname, item.APIGROUP_CODE);
		done()
	}, ()=>{
		
	})
}

this.cacheDomain = async (all_domain)=>{
	const client = RedisConnector.getClient()
	const dbset = await client.keys(`${config.app.prefix}API_DOMAIN_*`);
	if(dbset.length > 0){
		// console.log("DEL dbset 1", dbset)
		await client.del(dbset)
	}
	forEach(all_domain, async function (item, index){
		var done = this.async()
		const u = urlparser.parse(item.DOMAIN)
		const keyname = `${config.app.prefix}API_DOMAIN_${u.host.hostname}:${u.host.port}`
		await client.set(keyname, item.ENVIROMENT_CODE)
		done()
	}, ()=>{
	})
}

this.cacheAPIDatabase = async (all_database)=>{
	const client = RedisConnector.getClient()
	const dbset = await client.keys(`${config.app.prefix}API_DB:*`);
	if(dbset.length > 0){
		// console.log("DEL dbset 1", dbset)
		await client.del(dbset)
	} 
	forEach(all_database, async function (item, index){
		var done = this.async()
		const keyname = `${config.app.prefix}API_DB:${item.API_CODE}:${item.ENVIROMENT_CODE}:${item.DB_MODE}`
		await client.set(keyname, JSON.stringify(item))
		done()
	}, ()=>{
		
	})
}

this.cacheBehaviorCacheSetting = async (all_bhvcache)=>{
	const client = RedisConnector.getClient()
	const dbset = await client.keys(`${config.app.prefix}BHVCACHE:*`);
	if(dbset.length > 0){
		// console.log("DEL dbset 1", dbset)
		await client.del(dbset)
	} 

	forEach(all_bhvcache, async function (item, index){
		var done = this.async()
		const keyname = `${config.app.prefix}BHVCACHE:${item.API_CODE}`
		await client.set(keyname, JSON.stringify(item))
		done()
	}, ()=>{
	})
}
this.cacheUser = async (all_user)=>{
	const client = RedisConnector.getClient()
	forEach(all_user, async function (item, index){
		var done = this.async()
		const keyname = `${config.app.prefix}_USR_${item.USERNAME}_${item.ENVIROMENT_CODE}`
		if(item.PARTNER_CODE){
			let isHaveNullProperties = Object.values(item).every(o => o === null);
			if(!isHaveNullProperties){
				if(!item.CLIENT_ID){
					item.CLIENT_ID = ""
				}
				if(!item.ISSIGNATURE_RESPONSE){
					item.ISSIGNATURE_RESPONSE = ""
				}
				if(!item.SECRET){
					item.SECRET = ""
				}
				if(!item.ENVIROMENT_CODE){
					item.ENVIROMENT_CODE = ""
				}
				await client.hmset(keyname, item)
			}
		}
		done()
	}, ()=>{ 
	})
}

this.cacheStore = (all_store)=>{
	return new Promise((resolved, rejected)=>{
		try{

			forEach(all_store, async function(item, index){
					cacheStoreRedis(item)
					// var done = this.async(item)
						const client = RedisConnector.getClient()
						await client.set(`STR_${item.API_CODE}`, item.PRO_CODE)
						await cacheParams(item.API_CODE, item.PRO_CODE)
					// done()
				}, function(){
					return resolved()
			})
		}catch(e){
		console.log("INIT ERROR ", e)
	}
	})
}

cacheStoreRedis = async (store)=>{
	const client = RedisConnector.getClient()
	if(store.ACTION_TYPE == "QUEUE"){
		await client.set(`${config.app.prefix}${store.API_CODE}`, JSON.stringify(store))
	}else{
		await client.set(`${config.app.prefix}${store.API_CODE}`, JSON.stringify(store))
	}
}

cacheParams = async (api_code, store)=>{

	let store_name = store
	if(store.split(".").length > 1){
		store_name = store.split(".")[1]
	}
	return new Promise(async (resolved, rejected)=>{
		
		try{

				const store_params = await orcdb.getFuncParams(store_name)

				
				const client = RedisConnector.getClient()

				let pr_list = {}

				if(store_params.rows){

					forEach(store_params.rows, (item, index)=>{
						// params.push({name: item.ARGUMENT_NAME, type:item.DATA_TYPE})
						const k = `PARAM_${item.OWNER}-${item.PACKAGE_NAME}-${api_code.toUpperCase()}`

						if(pr_list[k]){
							pr_list[k].push({name: item.ARGUMENT_NAME, type:item.DATA_TYPE})
						}else{
							pr_list[k] = []
							pr_list[k].push({name: item.ARGUMENT_NAME, type:item.DATA_TYPE})
						}
					}, async function(){
						for (const [key, value] of Object.entries(pr_list)) {
						  setRedisParam(client, key, JSON.stringify(value))
						}

						return resolved(api_code)
					})
				}
		}catch(e){
			console.log("Cache-Params is error ", e)
		}
	
	})
}

const setRedisParam = async (client, k,v)=>{
	await client.set(k, v)
}


module.exports = {
  init:init,
  cachePartnerConfig: this.cachePartnerConfig,
  cacheDomain: this.cacheDomain,
  cacheUser: this.cacheUser
}